<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>{{ trans('message.quotation.sales_orders') }}</title>
</head>
<style>

 body{ font-family:"DeJaVu Sans",Helvetica, sans-serif; color:#121212; line-height:22px;}

 table, tr, td{
    border-bottom: 1px solid #d1d1d1;
    padding: 6px 0px;
}
tr{ height:40px;}
</style>

<body>
  <div style="width:900px; margin:15px auto;">
    <div style="width:450px; float:left; margin-top:20px;height:50px;">
   <div style="font-size:30px; font-weight:bold; color:#383838;">{{ trans('message.quotation.quotation') }}</div>
  </div>
  <div style="width:450px; float:right;height:50px;">
    <div style="text-align:right; font-size:14px; color:#383838;"><strong>{{ trans('message.quotation.quotation_no') }} # {{$saleData->reference}}</strong></div>
    <div style="text-align:right; font-size:14px; color:#383838;"><strong>{{ trans('message.quotation.quotation_date') }} : {{formatDate($saleData->ord_date)}}</strong></div>
  </div>
  <div style="clear:both;"></div>

  <div style="margin-top:40px;height:125px;">
    <div style="width:400px; float:left; font-size:15px; color:#383838; font-weight:400;">
      <div><strong>{{ $settings['company']['company_name'] }}</strong></div>
    <div><strong>GSTIN :</strong>&nbsp;{{ !empty($settings['company']['company_gstin']) ? $settings['company']['company_gstin'] : '-'}}</div>
    <div>{{ $settings['company']['company_street']}}</div>
    <div>{{ $settings['company']['company_city'] }}, {{ $settings['company']['company_state'] }}</div>
    <div>{{ $settings['company']['company_country_id'] }}, {{ $settings['company']['company_zipCode'] }}</div>
    </div>
    <div style="width:300px; float:left;font-size:15px; color:#383838; font-weight:400;">
      <div><strong>{{ trans('message.quotation.bill_to') }}</strong></div>
      <div><strong>GSTIN :</strong>&nbsp;{{ !empty($customerInfo->gstin) ? $customerInfo->gstin : '-'}}</div>
      <div>{{ !empty($customerInfo->name) ? $customerInfo->name : ''}}</div>
      <div>{{ !empty($customerInfo->billing_street) ? $customerInfo->billing_street : ''}}</div>
      <div>{{ !empty($customerInfo->billing_city) ? $customerInfo->billing_city : ''}}{{ !empty($customerInfo->billing_state) ? ', '.$customerInfo->billing_state : ''}}</div>
      <div>{{ !empty($customerInfo->billing_country_id) ? $customerInfo->billing_country_id : ''}}{{ !empty($customerInfo->billing_zip_code) ? ' ,'.$customerInfo->billing_zip_code : ''}}</div>
    </div>
  </div>
  <div style="clear:both"></div>

  <div style="margin-top:30px;">
   <table style="width:100%; border-radius:2px; border:2px solid #d1d1d1; border-collapse: collapse;">
      <tr style="background-color:#f0f0f0; border-bottom:1px solid #d1d1d1; text-align:center; font-size:13px; font-weight:bold;">
      <td>{{ trans('message.quotation.s_n') }}</td>
      <td>{{ trans('message.quotation.item_description') }}</td>
      <td>HSN/SAC</td>
      <td>{{ trans('message.quotation.quantity') }}</td>
      <td>{{ trans('message.quotation.price') }}({{Session::get('currency_symbol')}})</td>
      <!--<td>{{ trans('message.quotation.tax') }}(%)</td>-->
      @if($customerInfo->billing_country_id=='IN' && ($customerInfo->billing_state==$settings['company']['company_state'] && $settings['company']['company_country_id']=='India'))      <!--<td>{{ trans('message.quotation.tax') }}(%)</td>-->
       <!--<th width="10%" class="text-center">{{ trans('message.table.tax') }}(%)</th>-->
       <td>CGST(%)</td>
       <td>SGST(%)</td>
       @else
       <td>IGST(%)</td>
       @endif
      <td>{{ trans('message.quotation.discount') }}(%)</td>
      <td style="padding-right:10px;text-align:right">{{ trans('message.quotation.amount') }}({{Session::get('currency_symbol')}})</td>
    </tr>

   <?php
    $taxAmount      = 0;
    $taxTotal = 0;
    $subTotalAmount = 0;
    $qtyTotal       = 0;
    $priceAmount    = 0;
    $discount       = 0;
    $discountPriceAmount = 0;  
    $sum = 0;
    $i=0;
  ?>
  @foreach ($invoiceData as $item)
   <?php
    $price = ($item['quantity']*$item['unit_price']);
    $discount =  ($item['discount_percent']*$price)/100;
    $discountPriceAmount = ($price-$discount);
    $qtyTotal +=$item['quantity']; 
    $subTotalAmount += $discountPriceAmount; 
    $taxAmount = (($discountPriceAmount*$item['tax_rate'])/100);
    $taxTotal += $taxAmount;

   ?> 
      
    <tr style="background-color:#fff; text-align:center; font-size:13px; font-weight:normal;">
      <td>{{++$i}}</td>
      <td>{{$item['description']}}</td>
      <td>{{$item['hsn']?$item['hsn']:'-'}}</td>
      <td>{{$item['quantity']}}</td>
      <td>{{number_format(($item['unit_price']),2,'.',',')}}</td>
      <!--<td>{{number_format($item['tax_rate'],2,'.',',')}}</td>-->
      @if($customerInfo->billing_country_id=='IN' && ($customerInfo->billing_state==$settings['company']['company_state'] && $settings['company']['company_country_id']=='India'))      <!--<td>{{ trans('message.quotation.tax') }}(%)</td>-->
      <td>{{number_format($item['tax_rate']/2,2,'.',',')}}</td>
      <td>{{number_format($item['tax_rate']/2,2,'.',',')}}</td>
      @else
      <td>{{number_format($item['tax_rate'],2,'.',',')}}</td>
      @endif
      <td>{{number_format($item['discount_percent'],2,'.',',')}}</td>
      <td style="padding-right:10px;text-align:right">{{number_format($discountPriceAmount,2,'.',',')}}</td>
    </tr>
  <?php
    $sum = $item['quantity']+$sum;
  ?>
  @endforeach   

    <tr style="background-color:#fff; text-align:right; font-size:13px; font-weight:normal; height:100px;">
      <td colspan="<?= (($customerInfo->billing_country_id=='IN') && ($customerInfo->billing_state==$settings['company']['company_state'] && $settings['company']['company_country_id']=='India'))?8:7;?>" style="border-bottom:none">
         {{ trans('message.quotation.total_quantity') }}<br />
       <strong>{{ trans('message.quotation.subtotal') }}</strong><br/>
        </td>   

      <td style="text-align:right; padding-right:10px;border-bottom:none">
        {{$sum}}<br />
       {{Session::get('currency_symbol').number_format(($subTotalAmount),2,'.',',')}}<br/>
      </td>
    </tr>

    <tr style="background-color:#fff; text-align:right; font-size:13px; font-weight:normal; height:100px;">
      <td colspan="<?= (($customerInfo->billing_country_id=='IN') && ($customerInfo->billing_state==$settings['company']['company_state'] && $settings['company']['company_country_id']=='India'))?8:7;?>" style="border-bottom:none">
         {{ trans('message.quotation.tax') }}
        </td>
      <td style="text-align:right; padding-right:10px; border-bottom:none">
       {{$taxTotal}}
      </td>
    </tr>     

    <tr style="background-color:#f0f0f0; text-align:right; font-size:13px; font-weight:normal;">
      <td colspan="<?= (($customerInfo->billing_country_id=='IN') && ($customerInfo->billing_state==$settings['company']['company_state'] && $settings['company']['company_country_id']=='India'))?8:7;?>" style="text-align:right;"><strong>{{ trans('message.quotation.grand_total') }}</strong></td>
      <td style="text-align:right; padding-right:10px"><strong>{{Session::get('currency_symbol').number_format(($subTotalAmount+$taxTotal),2,'.',',')}}</strong></td>
    </tr>
   </table> 
    </div>
    
  </div>
  <script type="text/javascript">
      window.onload = function() { window.print(); }
 </script> 
</body>
</html>
