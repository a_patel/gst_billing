@extends('layouts.app')
@section('content')
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          @include('layouts.includes.sub_menu')
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="box box-default">
            <div class="box-body">
              <div class="row">
                <div class="col-md-9">
                 <div class="top-bar-title padding-bottom">Events</div>
                </div> 
                <div class="col-md-3">
                  @if(!Session::has('user_expired'))
                    <a href="javascript:void(0)" data-toggle="modal" data-target="#add-event" class="btn btn-block btn-default btn-flat btn-border-orange"><span class="fa fa-plus"> &nbsp;</span>New Event</a>
                  @endif
                </div>
              </div>
            </div>
          </div>

          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Start Date</th>
                  <th>End Date</th>
                  <th width="5%">{{ trans('message.table.action') }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($eventData as $data)
                <tr>
                  <td>{{ $data->name }}</td>
                  <td>{{ $data->start_time }}</td>
                  <td>{{ $data->end_time }}</td>
                  <td>
              
              @if(!Session::has('user_expired'))
                      <a title="{{ trans('message.form.edit') }}" href="javascript:void(0)"  class="btn btn-xs btn-primary edit_event" id="{{$data->id}}" ><span class="fa fa-edit"></span></a> &nbsp;
              @endif
              
              @if(!Session::has('user_expired'))
              @if(!in_array($data->id,[1]))   
                      <form method="POST" action="{{ url("delete-event/$data->id") }}" accept-charset="UTF-8" style="display:inline">
                          {!! csrf_field() !!}
                          <button title="{{ trans('message.form.Delete') }}" class="btn btn-xs btn-danger" type="button" data-toggle="modal" data-target="#confirmDelete" data-title="{{ trans('message.table.delete_unit_header') }}" data-message="Are you sure you want to delete this Event ?">
                              <i class="glyphicon glyphicon-trash"></i> 
                          </button>
                      </form>
              @endif
              @endif
                  </td>
                </tr>
               @endforeach
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
<div id="add-event" class="modal fade" role="dialog" style="display: none;">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content"> 
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title">{{ trans('message.table.add_new') }}</h4>
      </div>
      <div class="modal-body">
        <form action="{{ url('save-event') }}" method="post" id="addUnit" class="form-horizontal">
            {!! csrf_field() !!}
          
          <div class="form-group">
            <label class="col-sm-3 control-label require" for="inputEmail3">Name</label>

            <div class="col-sm-6">
              <input type="text" placeholder="Name" class="form-control" id="name" name="name">
              <span id="val_name" style="color: red"></span>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label require" for="inputEmail3">Start Date</label>

            <div class="col-sm-6">
              <input class="form-control datepicker" id="datepicker" type="text" name="start_time">
              <span id="val_ab" style="color: red"></span>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label require" for="inputEmail3">End Date</label>

            <div class="col-sm-6">
              <input class="form-control datepicker" type="text" name="end_time">
              <span id="val_ab" style="color: red"></span>
            </div>
          </div>

          
          <div class="form-group">
            <label for="btn_save" class="col-sm-3 control-label"></label>
            <div class="col-sm-6">
              <button type="button" class="btn btn-info btn-flat" data-dismiss="modal">{{ trans('message.form.close') }}</button>
              <button type="submit" class="btn btn-primary btn-flat">{{ trans('message.form.submit') }}</button>
            </div>
          </div>
        </form>
      </div>
    </div>

  </div>
</div>

<div id="edit-event" class="modal fade" role="dialog" style="display: none;">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title">Update Event</h4>
      </div>
      <div class="modal-body">
        <form action="{{ url('update-event') }}" method="post" id="editUnit" class="form-horizontal">
            {!! csrf_field() !!}

          <div class="form-group">
            <label class="col-sm-3 control-label require" for="inputEmail3">Name</label>

            <div class="col-sm-6">
              <input type="text" placeholder="Name" class="form-control" id="event_name" name="name">
              <span id="val_name" style="color: red"></span>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label require" for="inputEmail3">Start Date</label>

            <div class="col-sm-6">
              <input class="form-control datepicker" id="event_start_time" type="text" name="start_time">
              <span id="val_ab" style="color: red"></span>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label require" for="inputEmail3">End Date</label>

            <div class="col-sm-6">
              <input class="form-control datepicker" type="text" name="end_time" id="event_end_time">
              <span id="val_ab" style="color: red"></span>
            </div>
          </div>
          <input type="hidden" name="id" id="event_id">

          <div class="form-group">
            <label for="btn_save" class="col-sm-3 control-label"></label>
            <div class="col-sm-6">
              <button type="button" class="btn btn-info btn-flat" data-dismiss="modal">{{ trans('message.form.close') }}</button>
              <button type="submit" class="btn btn-primary btn-flat">{{ trans('message.form.submit') }}</button>
            </div>
          </div>

        </form>
      </div>
    </div>

  </div>
</div>
    @include('layouts.includes.message_boxes')
@endsection

@section('js')
    <script type="text/javascript">
      $(function () {
        $("#example1").DataTable({
          "columnDefs": [ {
            "targets": 2,
            "orderable": false
            } ],
            
            "language": '{{Session::get('dflt_lang')}}',
            "pageLength": '{{Session::get('row_per_page')}}'
        });
        
      });

      //Date picker
      $('.datepicker').datepicker({
        autoclose: true,
        startDate:new Date(),
        todayHighlight: true,
        format: '{{Session::get('date_format_type')}}'
      });
      $('.datepicker').datepicker('update', new Date());

      $('#addUnit').on('submit',function(e) {
        $(this).find('button[type="submit"]').attr('disabled', 'disabled');
      });
    $('#addUnit').validate({
        rules: {
            name: {
                required: true
            },
            start_time: {
                required: true
            },
            end_time: {
                required: true
            }
        },
      invalidHandler: function() {
        $(this).find('button[type="submit"]').attr('disabled', false);
      }
    });

    $('.edit_event').on('click', function() {
       var id = $(this).attr("id");

        $.ajax({
            url: '{{ URL::to('edit-event')}}',
            data:{  // data that will be sent
                id:id
            },
            type: 'POST',
            dataType: 'JSON',
            success: function (data) {
              
                $('#event_name').val(data.name);
                $('#event_start_time').val(data.start_time);
                $('#event_end_time').val(data.end_time);
                $('#event_id').val(data.id);

                $('#edit-event').modal();
            }
        });

    });

    $('#editUnit').validate({
        rules: {
            name: {
                required: true
            },
            abbr: {
                required: true
            }                     
        }
    });
    </script>
@endsection