@extends('layouts.app')
@section('content')
    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">
          @include('layouts.includes.company_menu')
        </div>
        <!-- /.col -->
        <div class="col-md-9">

          <div class="box box-default">
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                 <div class="top-bar-title padding-bottom">{{ trans('message.table.company_setting') }}</div>
                </div> 
              </div>
            </div>
          </div>

          <div class="box">
          <div class="box-body">
            <!-- /.box-header -->
            <form action="{{ url('company/setting/save') }}" method="post" id="settingForm" class="form-horizontal" enctype="multipart/form-data">
            {!! csrf_field() !!}
              <div class="box-body">
                <div class="form-group">
                  <label class="col-sm-3 control-label require" for="inputEmail3">{{ trans('message.table.name') }}</label>

                  <div class="col-sm-6">
                    <input type="text" class="form-control" name="company_name" id="company_name" value="{{isset($companyData['company']['company_name']) ? $companyData['company']['company_name'] : ''}}" >
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-3 control-label" for="inputEmail3">{{trans('message.form.site_short_name')}}</label>

                  <div class="col-sm-6">
                    <input type="text" name="site_short_name" value="{{isset($companyData['company']['site_short_name']) ? $companyData['company']['site_short_name'] : ''}}" class="form-control">
                  </div>
                </div>
                
                <div class="form-group">
                  <label class="col-sm-3 control-label require" for="inputEmail3">{{ trans('message.table.email') }}</label>

                  <div class="col-sm-6">
                    <input type="email" class="form-control" name="company_email" id="company_email" value="{{isset($companyData['company']['company_email']) ? $companyData['company']['company_email'] : ''}}" >
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label require" for="inputEmail3">{{ trans('message.table.phone') }}</label>

                  <div class="col-sm-6">
                    <input type="text" class="form-control" name="company_phone" id="company_phone" value="{{isset($companyData['company']['company_phone']) ? $companyData['company']['company_phone'] : ''}}" >
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="inputEmail3">GSTIN</label>
                  <div class="col-sm-6">
                    <input type="text" value="{{isset($companyData['company']['company_gstin']) ? $companyData['company']['company_gstin'] : ''}}" class="form-control" name="company_gstin">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label require" for="inputEmail3">{{ trans('message.invoice.country') }}</label>

                  <div class="col-sm-6">
                    <select class="form-control select2" name="company_country_id" id="company_country_id" >
                    @foreach ($countries as $data)
                      <option value="{{$data->country}}" <?=isset($companyData['company']['company_country_id']) && $companyData['company']['company_country_id'] == $data->country ? 'selected':""?> >{{$data->country}}</option>
                    @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group" id="select_div" style="<?= isset($companyData['company']['company_country_id']) && $companyData['company']['company_country_id']=='India'?'display: block':'display:none'?>">
                      <label class="col-sm-3 control-label require" for="inputEmail3">{{ trans('message.invoice.state') }}</label>
                      <div class="col-sm-6">
                        <select class="form-control select2" name="company_state_select" id="company_state_select">
                           <option value="">{{ trans('message.form.select_one') }}</option>
                           @foreach ($all_state as $data)
                                <option value="{{$data->name}}" <?= (isset($companyData['company']['company_country_id']) && $companyData['company']['company_state'] == $data->name) ? 'selected' : '' ?>>{{$data->name}}</option>
                           @endforeach
                        </select>
                      </div>
                </div>
                <div class="form-group" id="text_div" style="<?= isset($companyData['company']['company_country_id']) && $companyData['company']['company_country_id']!='India'?'display: block':'display:none';?>">
                  <label class="col-sm-3 control-label require" for="inputEmail3">{{ trans('message.invoice.state') }}</label>

                  <div class="col-sm-6">
                    <input type="text" class="form-control" name="company_state" id="company_state" value="{{isset($companyData['company']['company_state'])?$companyData['company']['company_state']:''}}">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label require" for="inputEmail3">{{ trans('message.invoice.city') }}</label>

                  <div class="col-sm-6">
                    <input type="text" class="form-control" name="company_city" id="company_city" value="{{isset($companyData['company']['company_city']) ? $companyData['company']['company_city'] : ''}}" >
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label require" for="inputEmail3">{{ trans('message.invoice.street') }}</label>

                  <div class="col-sm-6">
                    <input type="text" class="form-control" name="company_street" id="company_street" value="{{isset($companyData['company']['company_street']) ? $companyData['company']['company_street'] : ''}}" >
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-3 control-label" for="inputEmail3">{{ trans('message.invoice.zipcode') }}</label>

                  <div class="col-sm-6">
                    <input type="text" class="form-control" name="company_zipCode" id="company_zipCode" value="{{isset($companyData['company']['company_zipCode']) ? $companyData['company']['company_zipCode'] : ''}}" >
                  </div>
                </div>



                <div class="form-group">
                  <label class="col-sm-3 control-label" for="inputEmail3">{{ trans('message.table.default_language') }}</label>

                                   <div class="col-sm-6">
                    <select name="dflt_lang" class="form-control" >
                        <option value="ar" <?=isset($companyData['company']['dflt_lang']) && $companyData['company']['dflt_lang'] == 'ar' ? 'selected':""?> >Arabic</option>
                        
                        <option value="ch" <?=isset($companyData['company']['dflt_lang']) && $companyData['company']['dflt_lang'] == 'ch' ? 'selected':""?>>Chinese</option>              
                        <option value="en" <?=isset($companyData['company']['dflt_lang']) && $companyData['company']['dflt_lang'] == 'en' ? 'selected':""?>>English</option>
                        
                        <option value="fr" <?=isset($companyData['company']['dflt_lang']) && $companyData['company']['dflt_lang'] == 'fr' ? 'selected':""?>>French</option>
                        
                        <option value="po" <?=isset($companyData['company']['dflt_lang']) && $companyData['company']['dflt_lang'] == 'po' ? 'selected':""?>>Portuguese</option>
                        
                        <option value="rs" <?=isset($companyData['company']['dflt_lang']) && $companyData['company']['dflt_lang'] == 'rs' ? 'selected':""?>>Russain</option>
                        
                        <option value="sp" <?=isset($companyData['company']['dflt_lang']) && $companyData['company']['dflt_lang'] == 'sp' ? 'selected':""?>>Spanish</option>

                        <option value="tu" <?=isset($companyData['company']['dflt_lang']) && $companyData['company']['dflt_lang'] == 'tu' ? 'selected':""?>>Turkish</option>

                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-3 control-label" for="inputEmail3">{{ trans('message.table.default_currency') }}</label>

                  <div class="col-sm-6">
                    <select class="form-control" name="dflt_currency_id" >
                      <option value="">{{ trans('message.form.select_one') }}</option>
                    @foreach ($currencyData as $data)
                      <option value="{{$data->id}}" <?=isset($companyData['company']['dflt_currency_id']) && $companyData['company']['dflt_currency_id'] == $data->id ? 'selected':""?> >{{$data->name}}</option>
                    @endforeach
                    </select>
                  </div>
                </div>
                
			         <div class="form-group">
                <label class="col-sm-3 control-label" for="inputEmail3">Logo</label>

                <div class="col-sm-6">
                  <input name="company_logo" class="form-control input-file-field" type="file">
                 @if (!empty($companyData['company']['company_logo']))
                <input name="company_logo" class="form-control" type="hidden" value="{{$companyData['company']['company_logo']}}"><br>
                 <?php $image_url = $companyData['company']['company_logo'];?>
                    <img alt="Company Logo" src='{{url("public/uploads/company/$image_url")}}' class="img-responsive asa" height="80" width="80">
                  @endif
                </div>
                
          
              </div>
				

                <div class="form-group" style="display:none">
                  <label class="col-sm-3 control-label" for="inputEmail3">{{ trans('message.table.price_type') }}</label>

                  <div class="col-sm-6">
                    <select class="form-control" name="sales_type_id" >
                      <option value="">{{ trans('message.form.select_one') }}</option>

                        @foreach ($saleTypes as $saleType)
                          <option value="{{$saleType->id}}" <?=isset($companyData['company']['sales_type_id']) && $companyData['company']['sales_type_id'] == $saleType->id ? 'selected':""?> >{{$saleType->sales_type}}</option>
                        @endforeach
              
                    </select>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              @if(Helpers::has_permission(Auth::user()->id, 'manage_company_setting') && !Session::has('user_expired'))
              <div class="box-footer">
                <button class="btn btn-primary pull-right btn-flat" type="submit" id="btnSubmits">{{ trans('message.form.submit') }}</button>
              </div>
              @endif
              <!-- /.box-footer -->
            </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>


    @include('layouts.includes.message_boxes')
@endsection
@section('js')
    <script type="text/javascript">
     $('.select2').select2();
     $("#btnSubmit").on('click', function(){
      alert("This option is not available on demo version!");
      return false;
     });

    $('#settingForm').validate({
        rules: {
            company_name: {
                required: true
            },
            company_email: {
                required: true,
              myEmail:true
            },
            company_phone:{
              required : true,
              number:true
            }, 
            company_street: {
                required: true
            },
            company_city :{
              required : true,
              lettersonly: true
            },
            company_state :{
              required : true
            } ,
            company_zipCode :{
              required : true
            } ,
            company_country_id :{
              required : true
            }                                 
        }
    });

    </script>

    <script type="text/javascript">
      $(window).on('load',function (){
          var bill_country_code = $('#company_country_id').val();
          $('span.select2-container--default').css("width", "");
          if(bill_country_code=="India"){
            $('#text_div').css('display','none');
            $('#select_div').css('display','block');
            $('#company_state').val('');
          }else{
            $('#text_div').css('display','block');
            $('#select_div').css('display','none');
            $('#company_state_select').val('');
            
          }
    });
      
    </script>

     <script type="text/javascript">
      $(document).on('change','#company_country_id',function (){
      var bill_country_code = $(this).val();
      $('span.select2-container--default').css("width", "");
      if(bill_country_code=="India"){
        $('#text_div').css('display','none');
        $('#select_div').css('display','block');
        $('#company_state').val('');
        $.ajax({
          url : SITE_URL+"/customer/get_state_for_country",
          type : "get",
          async : false,
          data: { 'bill_country_code' : bill_country_code},
          dataType : 'json',
          success: function(reply)
          {
            if (reply.success == 1) 
            {
              var html   = '';
              var states = eval(reply.data);
              html += '<option value="">'+'Select State'+'</option>';
              $.each(states, function(i, st){
                html += '<option value="'+st.name+'">'+st.name+'</option>';
              });
              $('#company_state_select option').each(function() {
                      $(this).remove();
              });
              $('#company_state_select').append(html);
            } 
            else 
            {
              alert(reply.message);
              return false;
            }
          }
        });
      }else{
        $('#text_div').css('display','block');
        $('#select_div').css('display','none');
        $('#company_state_select').val('');

      }
    });
      
    </script>
@endsection