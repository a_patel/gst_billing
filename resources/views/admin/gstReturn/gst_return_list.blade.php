@extends('layouts.app')
@section('content')
    <!-- Main content -->
    <section class="content">

    <div class="box box-default">
      <div class="box-body">
        <div class="row">
          <div class="col-md-10">
           <div class="top-bar-title padding-bottom">{{ trans('message.extra_text.invoices') }}</div>
          </div> 
          <div class="col-md-2">
           @if(Helpers::has_permission(Auth::user()->id, 'add_invoice') && !Session::has('user_expired'))
              <a href="{{ url('sales/add') }}" class="btn btn-block btn-default btn-flat btn-border-orange"><span class="fa fa-plus"> &nbsp;</span>{{ trans('message.extra_text.new_sales_invoice') }}</a>
           @endif
          </div>
        </div>
      </div>
    </div>
     

     

      <div class="box">
        <div class="box-body">
                <ul class="nav nav-tabs cus" role="tablist">
                    
                    <li class="active">
                      <a href='{{url("gst-return/list")}}' >{{ trans('message.extra_text.all') }}</a>
                    </li>
                    
                    <li>
                      <a href="{{url("gst-return/filtering")}}" >{{ trans('message.extra_text.filter') }}</a>
                    </li>
                  

               </ul>
              

          
        </div>

       
      </div><!--Filtering Box End-->

       <?php
            $to_taxable_amount   = 0;
            $total_amount        = 0;
            $total_sgst          = 0;
            $total_cgst          = 0;
            $total_igst          = 0;
            $total_tax           = 0;
          ?>
          <?php for($i=0;$i<count($gstReturn);$i++) {
            
            $to_taxable_amount += $gstReturn[$i][0]->taxable_amount;
            $total_amount += $gstReturn[$i][0]->total;
            if($gstReturn[$i][0]->billing_country_id=='IN' && ($gstReturn[$i][0]->billing_state==$companyInfo['company']['company_state'] && $companyInfo['company']['company_country_id']=='India')){
              $total_sgst +=(($gstReturn[$i][0]->total-$gstReturn[$i][0]->taxable_amount)/2);
              $total_cgst +=(($gstReturn[$i][0]->total-$gstReturn[$i][0]->taxable_amount)/2);

            }else{
              $total_igst +=($gstReturn[$i][0]->total-$gstReturn[$i][0]->taxable_amount);
            }
            $total_tax +=($gstReturn[$i][0]->total-$gstReturn[$i][0]->taxable_amount);
             
            } ?> 

      <div class="box">
        <div class="box-body">
          <div class="col-md-1 col-xs-6 border-right text-center">
              <h3 class="bold">{{$i}}</h3>
              <span class="text-info">{{ trans('message.report.no_of_orders') }}</span>
          </div>
           <div class="col-md-2 col-xs-6 border-right text-center">
              <h3 class="bold">{{ Session::get('currency_symbol').number_format($total_amount ,2,'.',',')}}</h3>
              <span class="text-info">{{trans('message.gst_return.total_amount')}} </span>
          </div>
          <div class="col-md-2 col-xs-6 border-right text-center">
              <h3 class="bold">{{Session::get('currency_symbol').number_format($to_taxable_amount,2,'.',',')}}</h3>
              <span class="text-info">{{trans('message.gst_return.taxable_value')}}</span>
          </div>
          <div class="col-md-2 col-xs-6 border-right text-center">
              <h3 class="bold">{{Session::get('currency_symbol').number_format($total_tax,2,'.',',')}}</h3>
              <span class="text-info">{{trans('message.gst_return.total_tax')}}</span>
          </div>
           <div class="col-md-1 col-xs-6 border-right text-center">
              <h3 class="bold">{{Session::get('currency_symbol').number_format($total_igst,2,'.',',')}}</h3>
              <span class="text-info">{{trans('message.gst_return.total_igst')}}</span>
          </div>
         
          <div class="col-md-2 col-xs-6 border-right text-center">
              <h3 class="bold">{{Session::get('currency_symbol').number_format($total_sgst,2,'.',',')}}</h3>
              <span class="text-info">{{trans('message.gst_return.total_sgst')}}</span>
          </div>
           <div class="col-md-2 col-xs-6 border-right text-center">
              <h3 class="bold">{{Session::get('currency_symbol').number_format($total_cgst,2,'.',',')}}</h3>
              <span class="text-info">{{trans('message.gst_return.total_cgst')}}</span>
          </div>
          
          
           
        </div>
      </div><!--Top Box End-->
      
      <!-- Default box -->
      <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>{{trans('message.gst_return.date')}}</th>
                    <th>{{trans('message.gst_return.id')}}</th>
                    <th>{{trans('message.gst_return.customer')}}</th>
                    <th>{{trans('message.gst_return.gst_in')}}</th>
                    <th>{{trans('message.gst_return.total_amt')}}({{Session::get('currency_symbol')}})</th>
                    <th>{{trans('message.gst_return.taxable_amt')}}({{ Session::get('currency_symbol')}})</th>
                    <th>{{trans('message.gst_return.total_tax')}}({{ Session::get('currency_symbol')}})</th>
                    <th>{{trans('message.gst_return.status')}}</th>
                  </tr>
                  </thead>
                  <tbody>
                
                 @for($i=0;$i<count($gstReturn);$i++)
                 <tr>
                    <td>{{formatDate($gstReturn[$i][0]->ord_date)}}</td>

                    <td><a href="{{URL::to('/')}}/invoice/view-detail-invoice/{{$gstReturn[$i][0]->order_reference_id.'/'.$gstReturn[$i][0]->order_no}}" target="_blank">{{$gstReturn[$i][0]->reference }}</a></td>
                    <td>
                    @if(Helpers::has_permission(Auth::user()->id, 'edit_customer'))
                    <a href="{{url("customer/edit")}}/{{$gstReturn[$i][0]->debtor_no}}" target="_blank">{{$gstReturn[$i][0]->name }}</a>
                    @else
                    {{ $gstReturn[$i][0]->name }}
                    @endif
                    </td>
                    <td>{{ $gstReturn[$i][0]->gstin }}</td>
                    <td>{{ Session::get('currency_symbol').number_format($gstReturn[$i][0]->total,2,'.',',') }}</td>

                    <td>{{ Session::get('currency_symbol').number_format($gstReturn[$i][0]->taxable_amount,2,'.',',') }}</td>
                    <td>{{ Session::get('currency_symbol').number_format(($gstReturn[$i][0]->total-$gstReturn[$i][0]->taxable_amount),2,'.',',') }}</td>
  
                    @if($gstReturn[$i][0]->paid_amount == 0)
                      <td><span class="label label-danger">{{ trans('message.invoice.unpaid')}}</span></td>
                    @elseif($gstReturn[$i][0]->paid_amount > 0 && $gstReturn[$i][0]->total > $gstReturn[$i][0]->paid_amount )
                      <td><span class="label label-warning">{{ trans('message.invoice.partially_paid')}}</span></td>
                    @elseif($gstReturn[$i][0]->paid_amount<=$gstReturn[$i][0]->paid_amount)
                      <td><span class="label label-success">{{ trans('message.invoice.paid')}}</span></td>
                    @endif

                    
                  </tr>
                 @endfor
                 
                </table>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
      <!-- /.box -->

    </section>
<style>
  .box-body h3{
    font-size: 20px;
    word-break: break-all;
  }
  .box-body div{
    padding: 0px;
  }
</style>
@include('layouts.includes.message_boxes')
@endsection
@section('js')
    <script type="text/javascript">

  $(function () {
    $("#example1").DataTable({
      "order": [],
      "columnDefs": [ {
        "targets": 6,
        "orderable": false
        } ],

        "language": '{{Session::get('dflt_lang')}}',
        "pageLength": {{Session::get('row_per_page')}}
    });
    
  });

    </script>
@endsection