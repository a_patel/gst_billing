@extends('layouts.app')
@section('content')
    <!-- Main content -->
    <section class="content">

    <div class="box box-default">
      <div class="box-body">
        <div class="row">
          <div class="col-md-10">
           <div class="top-bar-title padding-bottom">{{ trans('message.extra_text.invoices') }}</div>
          </div> 
          <div class="col-md-2">
           @if(Helpers::has_permission(Auth::user()->id, 'add_invoice') && !Session::has('user_expired'))
              <a href="{{ url('sales/add') }}" class="btn btn-block btn-default btn-flat btn-border-orange"><span class="fa fa-plus"> &nbsp;</span>{{ trans('message.extra_text.new_sales_invoice') }}</a>
           @endif
          </div>
        </div>
      </div>
    </div>

      <div class="box">
        <div class="box-body">
                <ul class="nav nav-tabs cus" role="tablist">
                    
                    <li>
                      <a href='{{url("gst-return/list")}}' >{{ trans('message.extra_text.all') }}</a>
                    </li>
                    
                    <li class="active">
                      <a href="{{url("gst-return/filtering")}}" >{{ trans('message.extra_text.filter') }}</a>
                    </li>
                  
                  
               </ul>
               <form class="form-horizontal" action="{{ url('gst-return/filtering') }}" method="GET" id='orderListFilter'>
          <div class="col-md-12">
            <div class="row">

               <div class="col-md-2">
               <div class="form-group" style="margin-right: 5px">
                  <label for="exampleInputEmail1">{{ trans('message.report.from') }}</label>
                  <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input class="form-control" id="from" type="text" name="from" value="{{$from}}" required>
                  </div>
                </div>
               </div> 

               <div class="col-md-2">
               <div class="form-group" style="margin-right: 5px">
                  <label for="exampleInputEmail1">{{ trans('message.report.to') }}</label>
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input class="form-control" id="to" type="text" name="to" value="{{$to}}" required>
                  </div>
                  </div>
               </div> 
              

              

            </div>
           
             <div class="row">
             <div class="col-md-1">
             <div class="form-group">
                <div class="input-group">
                <button type="submit" name="btn" class="btn btn-primary btn-flat">{{ trans('message.extra_text.filter') }}</button>
                </div>
              </div>
              </div>
               <div class="col-md-1">
                <div class="form-group">
                  <div class="input-group">
                     <a href="{{URL::to('/')}}/gst-return-csv" title="CSV" class="btn btn-primary btn-flat" id="csv">{{trans('message.gst_return.download_gstr')}} </a>
                  </div>
                </div>
                </div>
             </div>


          </div>
          </form>
              

          
        </div>
         

       
      </div><!--Filtering Box End-->
      
      <!-- Default box -->
      <?php
            $to_taxable_amount   = 0;
            $total_amount        = 0;
            $total_sgst          = 0;
            $total_cgst          = 0;
            $total_igst          = 0;
            $total_tax           = 0;
          ?>
          <?php for($i=0;$i<count($gstReturn);$i++) {
            
            $to_taxable_amount += $gstReturn[$i][0]->taxable_amount;
            $total_amount += $gstReturn[$i][0]->total;
            if($gstReturn[$i][0]->billing_country_id=='IN' && ($gstReturn[$i][0]->billing_state==$companyInfo['company']['company_state'] && $companyInfo['company']['company_country_id']=='India')){
              $total_sgst +=(($gstReturn[$i][0]->total-$gstReturn[$i][0]->taxable_amount)/2);
              $total_cgst +=(($gstReturn[$i][0]->total-$gstReturn[$i][0]->taxable_amount)/2);

            }else{
              $total_igst +=($gstReturn[$i][0]->total-$gstReturn[$i][0]->taxable_amount);
            }
            $total_tax +=($gstReturn[$i][0]->total-$gstReturn[$i][0]->taxable_amount);
             
            } ?> 

     <div class="box">
        <div class="box-body">
          <div class="col-md-2 col-xs-6 border-right text-center">
              <h3 class="bold">{{$i}}</h3>
              <span class="text-info">{{ trans('message.report.no_of_orders') }}</span>
          </div>
           <div class="col-md-2 col-xs-6 border-right text-center">
              <h3 class="bold">{{ Session::get('currency_symbol').number_format($total_amount ,2,'.',',')}}</h3>
              <span class="text-info">{{trans('message.gst_return.total_amount')}} </span>
          </div>
          <div class="col-md-2 col-xs-6 border-right text-center">
              <h3 class="bold">{{Session::get('currency_symbol').number_format($to_taxable_amount,2,'.',',')}}</h3>
              <span class="text-info">{{trans('message.gst_return.taxable_value')}}</span>
          </div>
          <div class="col-md-2 col-xs-6 border-right text-center">
              <h3 class="bold">{{Session::get('currency_symbol').number_format($total_tax,2,'.',',')}}</h3>
              <span class="text-info">{{trans('message.gst_return.total_tax')}}</span>
          </div>
           <div class="col-md-2 col-xs-6 border-right text-center">
              <h3 class="bold">{{Session::get('currency_symbol').number_format($total_igst,2,'.',',')}}</h3>
              <span class="text-info">{{trans('message.gst_return.total_igst')}}</span>
          </div>
         
          <div class="col-md-1 col-xs-6 border-right text-center">
              <h3 class="bold">{{Session::get('currency_symbol').number_format($total_sgst,2,'.',',')}}</h3>
              <span class="text-info">{{trans('message.gst_return.total_sgst')}}</span>
          </div>
           <div class="col-md-1 col-xs-6 border-right text-center">
              <h3 class="bold">{{Session::get('currency_symbol').number_format($total_cgst,2,'.',',')}}</h3>
              <span class="text-info">{{trans('message.gst_return.total_cgst')}}</span>
          </div>
          
          
           
        </div>
      </div><!--Top Box End-->
      
      <!-- Default box -->
      <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                 <tr>
                    <th>{{trans('message.gst_return.date')}}</th>
                    <th>{{trans('message.gst_return.id')}}</th>
                    <th>{{trans('message.gst_return.customer')}}</th>
                    <th>{{trans('message.gst_return.gst_in')}}</th>
                    <th>{{trans('message.gst_return.total_amt')}}({{Session::get('currency_symbol')}})</th>
                    <th>{{trans('message.gst_return.taxable_amt')}}({{ Session::get('currency_symbol')}})</th>
                    <th>{{trans('message.gst_return.total_tax')}}({{ Session::get('currency_symbol')}})</th>
                    <th>{{trans('message.gst_return.status')}}</th>
                  </tr>
                  </thead>
                  <tbody>
               
                 @for($i=0;$i<count($gstReturn);$i++)
                 <tr>
                    <td>{{formatDate($gstReturn[$i][0]->ord_date)}}</td>

                    <td><a href="{{URL::to('/')}}/invoice/view-detail-invoice/{{$gstReturn[$i][0]->order_reference_id.'/'.$gstReturn[$i][0]->order_no}}" target="_blank">{{$gstReturn[$i][0]->reference }}</a></td>
                    <td>
                    @if(Helpers::has_permission(Auth::user()->id, 'edit_customer'))
                    <a href="{{url("customer/edit")}}/{{$gstReturn[$i][0]->debtor_no}}" target="_blank">{{$gstReturn[$i][0]->name }}</a>
                    @else
                    {{ $gstReturn[$i][0]->name }}
                    @endif
                    </td>
                    <td>{{ $gstReturn[$i][0]->gstin }}</td>
                    <td>{{ Session::get('currency_symbol').number_format($gstReturn[$i][0]->total,2,'.',',') }}</td>

                    <td>{{ Session::get('currency_symbol').number_format($gstReturn[$i][0]->taxable_amount,2,'.',',') }}</td>
                    <td>{{ Session::get('currency_symbol').number_format(($gstReturn[$i][0]->total-$gstReturn[$i][0]->taxable_amount),2,'.',',') }}</td>
  
                    @if($gstReturn[$i][0]->paid_amount == 0)
                      <td><span class="label label-danger">{{ trans('message.invoice.unpaid')}}</span></td>
                    @elseif($gstReturn[$i][0]->paid_amount > 0 && $gstReturn[$i][0]->total > $gstReturn[$i][0]->paid_amount )
                      <td><span class="label label-warning">{{ trans('message.invoice.partially_paid')}}</span></td>
                    @elseif($gstReturn[$i][0]->paid_amount<=$gstReturn[$i][0]->paid_amount)
                      <td><span class="label label-success">{{ trans('message.invoice.paid')}}</span></td>
                    @endif

                    
                  </tr>
                 @endfor
                
                 
                </table>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
      <!-- /.box -->

    </section>

@include('layouts.includes.message_boxes')
@endsection
@section('js')
    <script type="text/javascript">

  $(function () {
     $('#from').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: '{{Session::get('date_format_type')}}'
    });

    $('#to').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: '{{Session::get('date_format_type')}}'
    });
    $('#csv').on('click', function(event){
     event.preventDefault();

      var to = $('#to').val();
      var from = $('#from').val();
     
      window.location = SITE_URL+"/gst-return-csv?to="+to+"&from="+from;
     

    });
    $("#example1").DataTable({
      "order": [],
      "columnDefs": [ {
        "targets": 6,
        "orderable": false
        } ],

        "language": '{{Session::get('dflt_lang')}}',
        "pageLength": '{{Session::get('row_per_page')}}'
    });
    
  });

    </script>
@endsection