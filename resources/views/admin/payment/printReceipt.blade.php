<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>{{ trans('message.payment.payment') }}</title>
</head>
<style>
body{ font-family:"DeJaVu Sans",Helvetica, sans-serif; color:#121212; line-height:22px;}
 table, tr, td{
    border-bottom: 1px solid #d1d1d1;
    padding: 6px 0px;
}
tr{ height:40px;}
</style>
<body>
  <div style="width:900px; margin:15px auto; padding-bottom:40px;">
   <div style="margin-bottom: 10px;">
     @if(!empty($companyInfo['company']['company_logo']))
      <img src='{{ url("public/uploads/company/".$companyInfo['company']['company_logo']) }}' alt="Logo" height="50" weight="50"/>
      @else
      <img src='{{ url("public/uploads/company/logo.png") }}' alt="Logo" height="50" weight="50"/>
      @endif
    </div>
    <div style="font-weight:bold;font-size:30px;">{{ trans('message.payment.payment') }}</div>
    <div style="width:300px; float:left;">
      <div style="margin-top:20px;">
        <div style="font-size:16px; color:#000000; font-weight:bold;">{{ Session::get('company_name') }}</div>
        <div style="font-size:16px;">{{ Session::get('company_street') }}</div>
        <div style="font-size:16px;">{{ Session::get('company_city') }}, {{ Session::get('company_state') }}</div>
        <div style="font-size:16px;">{{ Session::get('company_country_id') }}, {{ Session::get('company_zipCode') }}</div>
      </div>
    </div>

    <div style="width:300px; float:left;">
      <div style="margin-top:20px;">
        <div style="font-size:16px;"><strong>{{ !empty($paymentInfo->name) ? $paymentInfo->name : '' }}</strong></div>
        <div style="font-size:16px;">{{ !empty($paymentInfo->billing_street) ? $paymentInfo->billing_street : '' }}</div>
        <div style="font-size:16px;">{{ !empty($paymentInfo->billing_city) ? $paymentInfo->billing_city : '' }}{{ !empty($paymentInfo->billing_state) ? ', '.$paymentInfo->billing_state : '' }}</div>
        <div style="font-size:16px;">{{ !empty($paymentInfo->billing_country_id) ? $paymentInfo->billing_country_id : '' }}{{ !empty($paymentInfo->billing_zip_code) ? ', '.$paymentInfo->billing_zip_code: '' }}</div>
      </div>
      <br/>
    </div>
  
    <div style="width:300px; float:left;">
      <div style="margin-top:20px;">
      
       <div style="font-size:16px;">{{ trans('message.payment.payment_no').' # '.sprintf("%04d", $paymentInfo->id) }}</div>
      </div>
      <br/>
    </div>

  <div style="clear:both"></div>
    <h3 style="text-align:center;margin:20px 0px;">{{ trans('message.payment.payment_receipt') }}</h3>
    <div>{{ trans('message.payment.payment_date') }} : {{ formatDate($paymentInfo->payment_date) }}</div>
    <div>{{ trans('message.payment.payment_method') }} : {{ $paymentInfo->payment_method }}</div>
    <br>
    <div style="height:100px;width:300px;background-color:#f0f0f0;color:#000;text-align:center;padding-top:30px">
      <strong>{{ trans('message.payment.total_amount') }}</strong><br>
      <strong>{{ Session::get('currency_symbol').number_format($paymentInfo->amount,2,'.',',') }}</strong>
    </div>
    <div style="clear:both"></div>
    <br>
    
   <table style="width:100%; border-radius:2px; border:2px solid #d1d1d1; border-collapse: collapse;">
      <tr style="background-color:#f0f0f0; border-bottom:1px solid #d1d1d1; text-align:center; font-size:13px; font-weight:bold;">
      <td>{{ trans('message.quotation.quotation') }}</td>
      <td>{{ trans('message.invoice_pdf.invoice_no') }}</td>
      <td>{{ trans('message.invoice_pdf.invoice_date') }}</td>
      <td>{{ trans('message.invoice_pdf.invoice_amount') }}</td>
      <td>{{ trans('message.invoice_pdf.paid_amount') }}</td>
    </tr>
    <tr style="background-color:#fff; text-align:center; font-size:13px; font-weight:normal;">
      <td>{{ $paymentInfo->order_reference }}</td>
      <td>{{$paymentInfo->invoice_reference}}</td>
      <td>{{formatDate($paymentInfo->invoice_date)}}</td>
      <td>{{ Session::get('currency_symbol').number_format($paymentInfo->invoice_amount,2,'.',',') }}</td>
      <td>{{ Session::get('currency_symbol').number_format($paymentInfo->amount,2,'.',',') }}</td>
    </tr>
  </table>
  </div>
  <script type="text/javascript">
      window.onload = function() { window.print(); }
 </script>
  
</body>
</html>