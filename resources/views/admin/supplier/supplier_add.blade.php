@extends('layouts.app')
@section('content')
    <!-- Main content -->
    <section class="content">
    <div class="box box-default">
      <div class="box-body">
        <div class="row">
          <div class="col-md-12">
           <div class="top-bar-title padding-bottom">{{ trans('message.extra_text.supplier') }}</div>
          </div> 
        </div>
      </div>
    </div>

        <div class="box">
<div class="box-body">
              <h4 class="text-info text-center">{{ trans('message.table.create_suppiler') }}</h4>
                <!-- form start -->
              <form action="{{ url('save-supplier') }}" method="post" id="supplier" class="form-horizontal">
              <input type="hidden" value="{{csrf_token()}}" name="_token" id="token">
              <div class="box-body">
              <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-sm-3 control-label require" for="inputEmail3">{{ trans('message.table.supp_name') }}</label>
                  <div class="col-sm-6">
                    <input type="text" placeholder="{{ trans('message.form.full_name') }}" class="form-control" id="supp_name" name="supp_name">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label require" for="inputEmail3">{{ trans('message.form.email') }}</label>
                  <div class="col-sm-6">
                    <input type="text" placeholder="{{ trans('message.form.email') }}" class="form-control" id="email" name="email">
                    
                  </div>
                </div>
                
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="inputEmail3">{{ trans('message.table.phone') }}</label>
                  <div class="col-sm-6">
                    <input type="text" placeholder="{{ trans('message.table.phone') }}" class="form-control" id="contact" name="contact">
                   
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="inputEmail3">GSTIN</label>

                  <div class="col-sm-6">
                    <input type="text" value="{{old('gstin')}}" class="form-control" name="gstin">
                  </div>
                </div>
                <div class="form-group">
                      <label class="col-sm-3 control-label require" for="inputEmail3">{{ trans('message.extra_text.country') }}</label>
                      <div class="col-sm-6">
                            <select class="form-control select2" name="country" id="country">
                            <option value="">{{ trans('message.form.select_one') }}</option>
                            @foreach ($countries as $data)
                              <option value="{{$data->code}}" >{{$data->country}}</option>
                            @endforeach
                            </select>
                      </div>
                  </div>
                   <div class="form-group" id="text_div">
                      <label class="col-sm-3 control-label" for="inputEmail3">{{ trans('message.invoice.state') }}</label>

                      <div class="col-sm-6">
                        <input type="text" class="form-control" name="state" value="{{old('bill_state')}}" id="bill_state">
                      </div>
                    </div>
                    <div class="form-group" id="select_div">
                          <label class="col-sm-3 control-label require" for="inputEmail3">{{ trans('message.invoice.state') }}</label>
                          <div class="col-sm-6">
                            <select class="form-control select2" name="selected_state" id="bill_state_select">
                               <option value="">{{ trans('message.form.select_one') }}</option>
                            </select>
                          </div>
                    </div>

                <div class="form-group">
                        <label class="col-sm-3 control-label require" for="inputEmail3">{{ trans('message.invoice.city') }}</label>

                        <div class="col-sm-6">
                          <input type="text" class="form-control" name="city" value="{{old('bill_city')}}" id="bill_city">
                        </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label require" for="inputEmail3">{{ trans('message.extra_text.street') }}</label>
                  <div class="col-sm-6">
                    <input type="text" placeholder="{{ trans('message.extra_text.street') }}" class="form-control" id="street" name="street">
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-3 control-label" for="inputEmail3">{{ trans('message.extra_text.zipcode') }}</label>
                  <div class="col-sm-6">
                    <input type="text" placeholder="{{ trans('message.extra_text.zipcode') }}" class="form-control" id="zipcode" name="zipcode">
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-3 control-label" for="inputEmail3"></label>
                  <div class="col-sm-6">

                   <div style="margin-top:10px;"> 
                      <br><br>
                      <a href="{{ url('supplier') }}" class="btn btn-info btn-flat">{{ trans('message.form.cancel') }}</a>
                      <button class="btn btn-primary btn-flat pull-right" type="submit">{{ trans('message.form.submit') }}</button>                     
                   </div>     
                  </div>
                </div>                                                                

              </div>
              </div>
              </div>
            </form>
          </div>
        </div>
        
        <!-- /.box-footer-->
      
      <!-- /.box -->

    </section>
@endsection

@section('js')
    <script type="text/javascript">
     $(".select2").select2();

     $('#supplier').on('submit',function(e) {
       $(this).find('button[type="submit"]').attr('disabled', 'disabled');
     });
    $('#supplier').validate({
        rules: {
            supp_name: {
                required: true,
              lettersonly: true
            },
            email: {
                required: true,
              myEmail:true
            },
            street:{
              required : true
            }, 
            city: {
                required: true,
              lettersonly: true
            },
          contact: {
            number:true
            },
            country :{
              required : true
            }
        },
      invalidHandler: function() {
        $(this).find('button[type="submit"]').attr('disabled', false);
      }
    });

    </script>
    <script type="text/javascript">
    $(document).on('change', '#country', function (){
      var bill_country_code = $(this).val();
      if(bill_country_code=="IN"){
        $('#text_div').css('display','none');
        $('#select_div').css('display','block');
        $.ajax({
          url : SITE_URL+"/customer/get_state_for_country",
          type : "get",
          async : false,
          data: { 'bill_country_code' : bill_country_code},
          dataType : 'json',
          success: function(reply)
          {
            if (reply.success == 1) 
            {
              var html   = '';
              var states = eval(reply.data);
              html += '<option value="">'+'Select State'+'</option>';
              $.each(states, function(i, st){
                html += '<option value="'+st.name+'">'+st.name+'</option>';
              });
              $('#bill_state_select option').each(function() {
                      $(this).remove();
              });
              $('#bill_state_select').append(html);
            } 
            else 
            {
              alert(reply.message);
              return false;
            }
          }
        });
      }else{
        $('#text_div').css('display','block');
        $('#select_div').css('display','none');
      }
    });
    $(window).on('load',function(){
       $('#text_div').css('display','block');
       $('#select_div').css('display','none');
    });
    </script>
@endsection