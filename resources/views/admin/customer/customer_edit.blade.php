@extends('layouts.app')
@section('content')
    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
        <div class="box">
           <div class="panel-body">
                <ul class="nav nav-tabs cus" role="tablist">
                    <li class="active">
                      <a href='{{url("customer/edit/$customerData->debtor_no")}}' >{{ trans('message.sidebar.profile') }}</a>
                    </li>
                    <li>
                      <a href="{{url("customer/order/$customerData->debtor_no")}}" >{{ trans('message.accounting.quotations') }}</a>
                    </li>
                    <li>
                      <a href="{{url("customer/invoice/$customerData->debtor_no")}}" >{{ trans('message.extra_text.invoices') }}</a>
                    </li>
                    <li>
                      <a href="{{url("customer/payment/$customerData->debtor_no")}}" >{{ trans('message.extra_text.payments') }}</a>
                    </li>
               </ul>
              <div class="clearfix"></div>
           </div>
        </div>

        <h3>{{$customerData->name}}</h3>
        <div class="box">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs" id="tabs" style="font-size:12px">
              <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="false">{{ trans('message.table.general_settings') }}</a></li>
              @if(!empty($customerData->password))
              <li><a href="#tab_3" data-toggle="tab" aria-expanded="false">{{ trans('message.form.update_password') }}</a></li>
              @else
              <li><a href="#tab_3" data-toggle="tab" aria-expanded="false">{{ trans('message.form.set_password') }}</a></li>
              @endif
            </ul>
            <div class="tab-content">
              <div class="tab-pane fade in active" id="tab_1">
                  <form action='{{ url("update-customer/$customerData->debtor_no") }}' method="post" class="form-horizontal" id="customerAdd">

                <div class="row">
                <div class="col-md-6">      
                  <input type="hidden" value="{{csrf_token()}}" name="_token" id="token">
                        <div class="box-body">
                          
                          <div class="form-group">
                            <label class="col-sm-3 control-label require" for="inputEmail3">{{ trans('message.form.name') }}</label>
                            <div class="col-sm-7">
                              <input type="text" placeholder="{{ trans('message.form.full_name') }}" class="form-control valdation_check" id="name" name="name" value="{{$customerData->name}}">
                            </div>
                          </div>
                          
                          <div class="form-group">
                            <label class="col-sm-3 control-label" for="inputEmail3">{{ trans('message.form.email') }}</label>
                            <div class="col-sm-7">
                              <input type="text" placeholder="{{ trans('message.table.email') }}" class="form-control valdation_check" id="email" name="email" value="{{$customerData->email}}" readonly>
                            </div>
                          </div>
                          
                          <div class="form-group">
                            <label class="col-sm-3 control-label require" for="inputEmail3">{{ trans('message.table.phone') }}</label>
                            <div class="col-sm-7">
                              <input type="text" placeholder="{{ trans('message.table.phone') }}" class="form-control" id="phone" name="phone" value="{{$customerData->phone}}">
                            </div>
                          </div>
                           <div class="form-group">
                              <label class="col-sm-3 control-label" for="inputEmail3">GSTIN</label>
                              <div class="col-sm-7">
                                <input type="text" value="{{$customerData->gstin}}" class="form-control" name="gstin">
                              </div>
                            </div>
                          <div class="form-group">
                            <label class="col-sm-3 control-label require" for="inputEmail3">{{ trans('message.invoice.country') }}</label>
                            <div class="col-sm-7">
                              <select class="form-control select2" name="billing_country_id" id="billing_country_id">
                              <option value="">{{ trans('message.form.select_one') }}</option>
                              @foreach ($countries as $data)
                                <option value="{{$data->code}}" <?= ($cusBranchData->billing_country_id == $data->code) ? 'selected' : '' ?>>{{$data->country}}</option>
                              @endforeach
                              </select>
                            </div>
                          </div>
                          <div class="form-group" style="{{$cusBranchData->billing_country_id=='IN'?'display: block':'display:none'}}" id="select_div_first">
                            <label class="col-sm-3 control-label require" for="inputEmail3">{{ trans('message.invoice.state') }}</label>
                            <div class="col-sm-7">
                              <select class="form-control select2" name="billing_state" id="billing_state">
                                    <option value="">{{ trans('message.form.select_one') }}</option>
                                    @if(!empty($all_state))
                                      @foreach ($all_state as $data)
                                        <option value="{{$data->name}}" <?= ($cusBranchData->billing_state == $data->name) ? 'selected' : '' ?>>{{$data->name}}</option>
                                      @endforeach
                                    @endif
                              </select>
                            </div>
                          </div>
                          <div class="form-group" style="{{$cusBranchData->billing_country_id!='IN'?'display: block':'display:none'}}" id="text_div">
                            <label class="col-sm-3 control-label" for="inputEmail3">{{ trans('message.invoice.state') }}</label>
                            <div class="col-sm-7">
                              <input name="bill_state" id="bill_state" type="text" class="form-control" value="{{$cusBranchData->billing_country_id=='IN'?'':$cusBranchData->billing_state}}">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-3 control-label require" for="inputEmail3">{{ trans('message.invoice.city') }}</label>
                            <div class="col-sm-7">
                              <input name="bill_city" id="bill_city" type="text" class="form-control" value="{{$cusBranchData->billing_city}}">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-3 control-label require" for="inputEmail3">{{ trans('message.invoice.street') }}</label>
                            <div class="col-sm-7">
                              <input name="bill_street" id="bill_street" type="text" class="form-control" value="{{$cusBranchData->billing_street}}">
                            </div>
                          </div>
                        
                          <div class="form-group">
                            <label class="col-sm-3 control-label" for="inputEmail3">{{ trans('message.invoice.zipcode') }}</label>
                            <div class="col-sm-7">
                              <input name="bill_zipCode" id="bill_zipCode" type="text" class="form-control" name="bill_zipCode" value="{{$cusBranchData->billing_zip_code}}">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-3 control-label" for="inputEmail3">{{ trans('message.form.status') }}</label>
                            <div class="col-sm-7">
                              <select class="form-control" name="status" id="status">
                                <option value="0" <?=isset($customerData->inactive) && $customerData->inactive ==  0? 'selected':""?> >Active</option>
                                <option value="1"  <?=isset($customerData->inactive) && $customerData->inactive == 1 ? 'selected':""?> >Inactive</option>
                              </select>
                            </div>
                          </div>
                        </div>
                  </div>
                  
                  <div class="col-md-6">
                          <h4 class="text-info text-center">{{ trans('message.invoice.shipping_address') }}<button id="copy" class="btn btn-default btn-xs" type="button">{{ trans('message.table.copy_address') }}</button></h4>
                          
                          <div class="form-group">
                            <label class="col-sm-3 control-label" for="inputEmail3">{{ trans('message.invoice.street') }}</label>
                            <div class="col-sm-7">
                              <input name="ship_street" id="ship_street" type="text" class="form-control" value="{{$cusBranchData->shipping_street}}">
                            </div>
                          </div>
                          
                          <div class="form-group">
                            <label class="col-sm-3 control-label" for="inputEmail3">{{ trans('message.invoice.city') }}</label>
                            <div class="col-sm-7">
                              <input name="ship_city" id="ship_city" type="text" class="form-control" value="{{$cusBranchData->shipping_city}}">
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="col-sm-3 control-label" for="inputEmail3">{{ trans('message.invoice.state') }}</label>
                            <div class="col-sm-7">
                              <input name="ship_state" id="ship_state" type="text" class="form-control" value="{{$cusBranchData->shipping_state}}">
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="col-sm-3 control-label" for="inputEmail3">{{ trans('message.invoice.zipcode') }}</label>
                            <div class="col-sm-7">
                              <input name="ship_zipCode" id="ship_zipCode" type="text" class="form-control" value="{{$cusBranchData->shipping_zip_code}}">
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="col-sm-3 control-label" for="inputEmail3">{{ trans('message.invoice.country') }}</label>
                            <div class="col-sm-7">
                              <select class="form-control select2" name="shipping_country_id" id="shipping_country_id">
                              <option value="">{{ trans('message.form.select_one') }}</option>
                              @foreach ($countries as $data)
                                <option value="{{$data->code}}" <?= ($cusBranchData->shipping_country_id == $data->code) ? 'selected' : '' ?>>{{$data->country}}</option>
                              @endforeach
                              </select>
                            </div>
                          </div>

                  </div>

              </div>
              <div class="row">
                <div class="col-md-12">
                 
                    <div style="margin-top:10px">
                      <a href="{{ url('customer/list') }}" class="btn btn-info btn-flat">{{ trans('message.form.cancel') }}</a>
                      @if(!Session::has('user_expired'))
                      <button class="btn btn-primary pull-right btn-flat" type="submit">{{ trans('message.form.submit') }}</button>
                        @endif
                    </div>
                  
                </div>
              </div>
              </form>
            </div>

        
              <div class="tab-pane" id="tab_3">
                    <div class="row">
                      <div class="col-md-6">
                          <form action='{{url("customer/update-password")}}' class="form-horizontal" id="password-form" method="POST">
                            <input type="hidden" value="{{csrf_token()}}" name="_token" id="token">
                            <input type="hidden" value="{{$customerData->debtor_no}}" name="customer_id">
                            <div class="form-group">
                              <label class="col-sm-4 control-label require" for="inputEmail3">{{ trans('message.form.password') }}</label>

                              <div class="col-sm-8">
                              <input type="password" class="form-control" name="password" id="password">
                              </div>
                            </div>

                            <div class="form-group">
                              <label class="col-sm-4 control-label require" for="inputEmail3">{{ trans('message.form.confirm_password') }}</label>

                              <div class="col-sm-8">
                              <input type="password" class="form-control" name="password_confirmation" id="password_confirmation">
                              <br>
                             @if(Helpers::has_permission(Auth::user()->id, 'edit_customer') &&  !Session::has('user_expired'))
                              <button class="btn btn-primary pull-right btn-flat" type="submit">{{ trans('message.form.submit') }}</button>
                              @endif
                              </div>
                            </div>
                          </form>
                      </div>
              </div>
            </div>
            <!-- /.tab-content -->
          </div>
        </div>

    @include('layouts.includes.message_boxes')
    </section>
@endsection


@section('js')
    <script type="text/javascript">
    $('.select2').select2();
    $(document).find('.select2').attr('disabled', true);
    $(document).find('#bill_state').attr('disabled', true);
    $(function () {
      $("#example1").DataTable({
        "order": [],
        "columnDefs": [{
          "targets": 3,
          "orderable": false
          } ],

          "language": '{{Session::get('language')}}'
      });
        var type = window.location.hash.substr(1);
    });

    $('#customerAdd').validate({
        rules: {
            name: {
                required: true,
              lettersonly: true
            },
            email:{
                required: true,
              myEmail:true
            },
          phone:{
            required: true,
            number:true
          },

            bill_street: {
                required: true
            },
            bill_city:{
                required: true,
              lettersonly: true
            },
            billing_country_id:{
               required: true
            },
        }
    });

    $('#password-form').validate({
        rules: {
            password: {
                required: true,
                minlength: 5
            },
            password_confirmation: {
                required: true,
                minlength: 5,
                equalTo: "#password"
            }
        }
    });




    $('#copy').on('click', function() { 
        var state        = $('#bill_state').val()?$('#bill_state').val():$('#billing_state').val();
        $('#ship_street').val($('#bill_street').val());
        $('#ship_city').val($('#bill_city').val());
        $('#ship_state').val(state);
        $('#ship_zipCode').val($('#bill_zipCode').val());
        var billing_country_id = $('#billing_country_id').val();
        $("#shipping_country_id").val(billing_country_id).change();
    })

jQuery(function($) {
    var index = 'qpsstats-active-tab';
    //  Define friendly data store name
    var dataStore = window.sessionStorage;
    var oldIndex = 0;
    //  Start magic!
    try {
        // getter: Fetch previous value
        oldIndex = dataStore.getItem(index);
    } catch(e) {}
 
    $( "#tabs" ).tabs({active: oldIndex,
        activate: function(event, ui) {
            //  Get future value
            var newIndex = ui.newTab.parent().children().index(ui.newTab);
            //  Set future value
            try {
                dataStore.setItem( index, newIndex );
            } catch(e) {}
        }
    });
});

  /*$(document).on('change', '#billing_country_id', function (){
      var bill_country_code = $(this).val();
      if(bill_country_code=="IN"){
        $('#text_div').css('display','none');
        $('#select_div_first').css('display','block');
        $.ajax({
          url : SITE_URL+"/customer/get_state_for_country",
          type : "get",
          async : false,
          data: { 'bill_country_code' : bill_country_code},
          dataType : 'json',
          success: function(reply)
          {
            if (reply.success == 1) 
            {
              var html   = '';
              var states = eval(reply.data);
              html += '<option value="">'+'Select State'+'</option>';
              $.each(states, function(i, st){
                html += '<option value="'+st.name+'">'+st.name+'</option>';
              });
              $('#billing_state option').each(function() {
                      $(this).remove();
              });
              $('#billing_state').append(html);
            } 
            else 
            {
              alert(reply.message);
              return false;
            }
          }
        });
      }else{
        $('#text_div').css('display','block');
        $('#select_div_first').css('display','none');
      }
    });*/

    $(window).on('change load',function(){
        var billing_country = $('#billing_country_id').val();
        $('span.select2-container--default').css("width", "");
        if(billing_country=='IN'){
          $('#select_div_first').css('display','block');
          $('#text_div').css('display','none');
          $('#bill_state').val('');
        }else{
          $('#select_div_first').css('display','none');
          $('#text_div').css('display','block');
        }
    });
    </script>
@endsection