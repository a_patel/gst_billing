<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{ Session::get('company_name') }} | {{ucfirst(trans("message.sidebar.$menu"))}}</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta name="_token" content="{!! csrf_token() !!}"/>
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{ asset('public/bootstrap/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('public/bootstrap/css/font-awesome.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('public/bootstrap/css/ionicons.min.css') }}">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('public/plugins/datatables/dataTables.bootstrap.css') }}">
  
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('public/plugins/select2/select2.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('public/dist/css/AdminLTE.min.css') }}">

  <link rel="stylesheet" href="{{ asset('public/dist/css/skins/_all-skins.min.css') }}">
  <!-- Date Picker -->
  <link rel="stylesheet" href="{{ asset('public/plugins/datepicker/datepicker3.css') }}">
  <link rel="stylesheet" href="{{ asset('public/dist/css/jquery-ui.min.css') }}" type="text/css" /> 
  <link rel="stylesheet" href="{{ asset('public/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ asset('public/dist/css/custom.css') }}">
  <link rel="stylesheet" href="{{ asset('public/dist/css/bootstrap-confirm-delete.css') }}">
  <!-- jQuery 2.2.3 -->
  <script src="{{ asset('public/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
<style>
  /* Button style  */
  .button {
    font             : bold 16px Helvetica, Arial, sans-serif;
    color            : #FFFFFF;
    padding          : 1em 2em;
    background       : linear-gradient(top, rgba(0, 0, 0, .15), rgba(0, 0, 0, .3));
    background-color : crimson;
    border-radius    : 5px;
    box-shadow       : 1px 1px 3px rgba(0, 0, 0, .3), inset 0 -1px 2px -1px rgba(0, 0, 0, .5), inset 0 1px 2px 1px rgba(255, 255, 255, .3);
    text-shadow      : 0 -1px 0 rgba(0, 0, 0, .3), 0 1px 0 rgba(255, 255, 255, .3);
    text-decoration  : none;
    transition       : transform .150s, background .01s;
  }
  #add_new {
    position: fixed;
    top: 110px;
    right: 0px;
    z-index: 100;
  }
  .sidebar-menu li.active .pull-right-container i{
    transform: rotate(-90deg);
  }
</style>
  <script type="text/javascript">
    var SITE_URL = "{{URL ::to('/')}}";
  </script>
  <link rel='shortcut icon' href="{{URL::to('/')}}/favicon.ico" type='image/x-icon'/ >
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  @include('layouts.includes.header')
  <!-- Left side column. contains the logo and sidebar -->

  @include('layouts.includes.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    @include('layouts.includes.notifications')
    
   {{-- @include('layouts.includes.breadcrumb') --}}
    @if(Session::has('user_expired'))
    <div class="button" id="add_new">You're plan is expired.</div>
    @endif
    @yield('content')
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @include('layouts.includes.footer')

  <!-- Control Sidebar -->
  @include('layouts.includes.sidebar_right')
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
@include('layouts.includes.script')
@yield('js')
</body>
</html>