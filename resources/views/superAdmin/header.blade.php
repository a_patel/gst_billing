<header class="main-header">
    <!-- Logo -->
    <a href="{{url('dashboard')}}" class="logo" style="background: #4B5158">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>EB</b></span>
      {{--<span class="logo-mini"><b>{{ Session::get('site_short_name') }}</b></span>--}}
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg" style="color: white">Easy Billing</span>
      <!--<span class="logo-lg">{{ Session::get('company_name') }}</span>-->
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top header_controls" style="background: #4B5158">
      <!-- Sidebar toggle button-->

      <?php
        if(!empty(Auth::user()->picture)) {
          $v = Auth::user()->picture; 
        }

        $flag = Session::get('dflt_lang').'.png';
      ?>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">

          <!-- User Account: style can be found in dropdown.less -->
          <li class="user user-menu">
                  <a href="{{ url('/logout') }}" style="color: white">{{ trans('message.form.sign_out') }}</a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
