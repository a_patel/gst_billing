@extends('superAdmin.app')
@section('content')
    <!-- Main content -->
    <section class="content">
       <!-- /.col -->
        <div class="col-md-12">
          <div class="box box-default">
            <div class="box-body">
              <div class="row">
                <div class="col-md-9">
                 <div class="top-bar-title padding-bottom">User Lists</div>
                </div>
              </div>
            </div>
          </div>

          <form action="{{ url('admin/update-trial-days') }}" method="post" id="itemAddForm" class="form-horizontal" enctype="multipart/form-data">
            <input type="hidden" value="{{csrf_token()}}" name="_token" id="token">
            <div class="box-body">
              <div class="form-group">
                <label class="col-sm-2 control-label require" for="inputEmail3">Trial days</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" name="trial_days" value="{{$super_data->trial_days}}">
                </div>
                <div class="col-sm-2">
                  <button class="btn btn-primary pull-right btn-flat" type="submit">{{ trans('message.form.submit') }}</button>
                </div>
              </div>
            </div>
          </form>
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table id="userList" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>{{ trans('message.form.name') }}</th>
                  <th>{{ trans('message.table.email') }}</th>
                  <th>{{ trans('message.table.phone') }}</th>
                  <th>Registration Date</th>
                  <th>Expire Date</th>
                  <th>Left Day</th>
                  <th width="15%">{{ trans('message.table.action') }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($userData as $data)
                <tr>
                  <td>{{ $data['id'] }}</td>
                  <td><a class="userDetail" href="javascript:void(0)" data-id="{{$data['id']}}">{{ $data['name'] }}</a></td>
                  <td>{{ $data['email'] }}</td>
                  <td>{{ $data['phone'] }}</td>
                  <td>{{ $data['created_at'] }}</td>
                  <td class="expire_date">{{ $data['expire_date'] }}</td>
                  <td><input type="text" class="form-control left_day" value="{{$data['left_day']}}" data-id="{{$data['id']}}"></td>
                  <td><input class="toggle-event" type="checkbox" {{($data['under_trial_period'] == 1) ? 'checked' : ''}} data-toggle="toggle" data-id="{{$data['id']}}"></td>
                </tr>
               @endforeach
                </tbody>
              </table>
            </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      </div>

       <!-- Modal -->
      <div class="modal fade" id="userDetail" role="dialog" style="display: none;">
        <div class="modal-dialog">

          <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">User Details</h4>
              </div>
              <div class="modal-body">
                <div class="form-group clearfix">
                  <label class="col-sm-3 control-label text-right" for="inputEmail3">User Name</label>
                  <div class="col-sm-6 username"></div>
                </div>
                <div class="form-group clearfix">
                  <label class="col-sm-3 control-label text-right" for="inputEmail3">Email</label>
                  <div class="col-sm-6 user_email"></div>
                </div>
                <div class="form-group clearfix">
                  <label class="col-sm-3 control-label text-right" for="inputEmail3">Phone</label>
                  <div class="col-sm-6 user_phone"></div>
                </div>
                <div class="form-group clearfix">
                  <label class="col-sm-3 control-label text-right" for="inputEmail3">Expire Date</label>
                  <div class="col-sm-6 user_expire_date"></div>
                </div>
              </div>
            </div>
        </div>
      </div>
    </section>
@endsection

@section('js')
  <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function () {
      $("#userList").DataTable({
        "order"     : [],
        "columnDefs": [
          {
            "targets"  : 3,
            "orderable": false
          }
        ],

        "language": '{{Session::get('dflt_lang')}}',

      });
      $(document).on('blur', '.left_day', function () {
        var $this = $(this);
        $.ajax({
          url: '{{ URL::to('admin/update-user-trial-days')}}',
          data:{
            left_days: $(this).val(),
            id: $(this).data('id')
          },
          type: 'GET',
          dataType: 'JSON',
          success: function (data) {
            if(data.data) {
              $this.closest('tr').find('.expire_date').html(data.data.expire_date);
            }
          }
        });
      });

      $('.toggle-event').change(function() {
        console.log('Toggle: ' + $(this).prop('checked'),$(this).data('id'));
        var $this = $(this);
        $.ajax({
          url: '{{ URL::to('admin/update-user-trial-status')}}',
          data:{
            id: $(this).data('id'),
            under_trial_period: $(this).prop('checked'),
          },
          type: 'GET',
          dataType: 'JSON',
          success: function (data) {
            if(data.data) {
              $this.closest('tr').find('.expire_date').html(data.data.expire_date);
            }
          }
        });
      })
    });
    $(document).on('click', '.userDetail', function(){
      $('#userDetail').modal();
      $.ajax({
        type: 'GET',
        url: '{{ URL::to('admin/user-detail')}}',
        data: {
          id: $(this).data('id')
        },
        success: function(data)
        {
            var userData = data.user;
            $('.username').html(userData.real_name);
            $('.user_email').html(userData.email);
            $('.user_phone').html(userData.phone);
            $('.user_expire_date').html(userData.expire_date);
            $('#userDetail').modal();
        }
      });
    });
  </script>
@endsection