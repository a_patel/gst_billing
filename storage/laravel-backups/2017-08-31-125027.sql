

CREATE TABLE IF NOT EXISTS `backup` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO backup VALUES("1","2017-07-29-083948.sql","2017-07-29 08:39:48","");
INSERT INTO backup VALUES("2","2017-08-05-145335.sql","2017-08-05 14:53:35","");
INSERT INTO backup VALUES("3","2017-08-23-145311.sql","2017-08-23 14:53:11","");
INSERT INTO backup VALUES("4","2017-08-28-124855.sql","2017-08-28 12:48:55","");
INSERT INTO backup VALUES("5","2017-08-28-124909.sql","2017-08-28 12:49:09","");
INSERT INTO backup VALUES("6","2017-08-30-111156.sql","2017-08-30 11:11:56","");
INSERT INTO backup VALUES("7","2017-08-30-111213.sql","2017-08-30 11:12:13","");





CREATE TABLE IF NOT EXISTS `bank_account_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO bank_account_type VALUES("1","Savings Account");
INSERT INTO bank_account_type VALUES("2","Chequing Account");
INSERT INTO bank_account_type VALUES("3","Credit Account");
INSERT INTO bank_account_type VALUES("4","Cash Account");





CREATE TABLE IF NOT EXISTS `bank_accounts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `account_type_id` tinyint(4) NOT NULL,
  `account_name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `account_no` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `bank_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `bank_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `default_account` tinyint(4) NOT NULL,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO bank_accounts VALUES("1","1","Test  Account","ac-154200","DBBL Bank","Dhaka","0","0");
INSERT INTO bank_accounts VALUES("2","4","Cash","0","Cash","","0","0");
INSERT INTO bank_accounts VALUES("3","1","RSSS","123456","SBI","Mumbai","0","0");
INSERT INTO bank_accounts VALUES("4","4","Ben Venture Pvt Ltd","902346756566","RBL","Malad Mumbai West","1","0");





CREATE TABLE IF NOT EXISTS `bank_trans` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `amount` double NOT NULL,
  `trans_type` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `account_no` int(11) NOT NULL,
  `trans_date` date NOT NULL,
  `person_id` int(11) NOT NULL,
  `reference` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `payment_method` int(11) NOT NULL,
  `attachment` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO bank_trans VALUES("1","0","cash-in","1","2017-07-24","1","opening balance","opening balance","1","1","","0000-00-00 00:00:00");
INSERT INTO bank_trans VALUES("2","23","cash-in-by-sale","1","2017-07-24","1","","Payment for INV-0001","1","2","","2017-07-24 12:32:23");
INSERT INTO bank_trans VALUES("3","23","cash-in-by-sale","1","2017-07-24","1","","Payment for INV-0001","1","2","","2017-07-24 12:59:23");
INSERT INTO bank_trans VALUES("4","23","cash-in-by-sale","1","2017-07-24","1","","Payment for INV-0002","1","2","","2017-07-24 13:01:15");
INSERT INTO bank_trans VALUES("5","50.75","cash-in-by-sale","1","2017-07-26","1","","Payment for INV-0001","1","1","","2017-07-26 10:13:59");
INSERT INTO bank_trans VALUES("6","-12","expense","1","2017-07-27","1","test","snacks for office","4","1","","2017-07-27 09:59:45");
INSERT INTO bank_trans VALUES("7","100","cash-in-by-sale","1","2017-07-27","1","","Payment for INV-0002","1","1","","2017-07-27 10:05:27");
INSERT INTO bank_trans VALUES("8","138768","cash-in-by-sale","1","2017-07-27","1","","Payment for INV-0008","1","2","","2017-07-27 11:57:20");
INSERT INTO bank_trans VALUES("9","12744","cash-in-by-sale","1","2017-07-27","1","","Payment for INV-0007","1","2","","2017-07-27 12:28:41");
INSERT INTO bank_trans VALUES("10","16124.7","cash-in-by-sale","1","2017-07-28","1","","Payment for INV-0006","1","2","","2017-07-28 04:22:29");
INSERT INTO bank_trans VALUES("11","-40","expense","1","2017-08-02","1","","making","3","2","","2017-08-02 07:50:32");
INSERT INTO bank_trans VALUES("12","10900","cash-in-by-sale","1","2017-08-05","1","vinod","Payment for INV-0016","5","2","","2017-08-05 15:03:37");
INSERT INTO bank_trans VALUES("13","6948.95","cash-in-by-sale","1","2017-08-11","1","","Payment for INV-0019","1","2","","2017-08-11 18:10:10");
INSERT INTO bank_trans VALUES("14","0","cash-in","2","2017-08-18","1","opening balance","opening balance","1","1","","0000-00-00 00:00:00");
INSERT INTO bank_trans VALUES("15","590","cash-in-by-sale","2","2017-08-21","1","","Payment for INV-0022","1","2","","2017-08-21 13:20:20");
INSERT INTO bank_trans VALUES("16","-100","expense","1","2017-08-23","1","","weqeeedw","4","1","","2017-08-22 19:59:06");
INSERT INTO bank_trans VALUES("17","118","cash-in-by-sale","1","2017-08-23","1","","Payment for INV-0006","1","2","","2017-08-23 11:39:44");
INSERT INTO bank_trans VALUES("18","10.8","cash-in-by-sale","1","2017-08-24","1","","Payment for INV-0026","1","2","","2017-08-24 07:15:25");
INSERT INTO bank_trans VALUES("19","-2000","expense","1","2017-08-25","1","","P3","4","2","1503604139.jpg","2017-08-24 19:48:28");
INSERT INTO bank_trans VALUES("20","8000","deposit","1","2017-08-25","1","","jj","1","1","","2017-08-25 08:32:40");
INSERT INTO bank_trans VALUES("21","1.944","cash-in-by-sale","2","2017-08-25","1","","Payment for INV-0026","1","2","","2017-08-25 09:58:48");
INSERT INTO bank_trans VALUES("22","8.8","cash-in-by-sale","2","2017-08-25","1","","Payment for INV-0029","2","2","","2017-08-25 17:01:56");
INSERT INTO bank_trans VALUES("23","3","cash-in-by-sale","2","2017-08-25","1","","Payment for INV-0029","2","2","","2017-08-25 17:02:37");
INSERT INTO bank_trans VALUES("24","3","cash-in-by-sale","2","2017-08-25","1","","Payment for INV-0029","2","2","","2017-08-25 17:02:38");
INSERT INTO bank_trans VALUES("25","2.9","cash-in-by-sale","1","2017-08-25","1","","Payment for INV-0029","1","2","","2017-08-25 17:04:16");
INSERT INTO bank_trans VALUES("26","10000","cash-in-by-sale","2","2017-08-27","1","","Payment for INV-0032","1","2","","2017-08-27 05:09:38");
INSERT INTO bank_trans VALUES("27","5000","cash-in-by-sale","2","2017-08-27","1","","Payment for INV-0032","1","2","","2017-08-27 05:10:54");
INSERT INTO bank_trans VALUES("28","1200","cash-in","3","2017-08-28","1","opening balance","opening balance","1","1","","0000-00-00 00:00:00");
INSERT INTO bank_trans VALUES("29","500","deposit","3","2017-08-28","1","","nmnm","1","2","","2017-08-28 07:01:48");
INSERT INTO bank_trans VALUES("30","-600","cash-out-by-transfer","3","2017-08-28","1","","hhjj","0","2","","0000-00-00 00:00:00");
INSERT INTO bank_trans VALUES("31","600","cash-in-by-transfer","3","2017-08-28","1","","hhjj","0","2","","0000-00-00 00:00:00");
INSERT INTO bank_trans VALUES("32","-400","expense","3","2017-08-28","1","","staff salary dalywise","6","2","","2017-08-28 07:29:50");
INSERT INTO bank_trans VALUES("33","1200","deposit","1","2017-08-29","1","1234","test description ","1","2","","2017-08-29 15:57:57");
INSERT INTO bank_trans VALUES("34","-1200","cash-out-by-transfer","1","2017-08-29","1","121212","description ","0","2","","0000-00-00 00:00:00");
INSERT INTO bank_trans VALUES("35","1200","cash-in-by-transfer","1","2017-08-29","1","121212","description ","0","2","","0000-00-00 00:00:00");
INSERT INTO bank_trans VALUES("36","110","cash-in-by-sale","2","2017-08-29","1","","Payment for INV-0042","1","1","","2017-08-29 20:29:04");
INSERT INTO bank_trans VALUES("37","5000","cash-in-by-sale","2","2017-08-30","1","","Payment for INV-0043","1","2","","2017-08-30 00:02:43");
INSERT INTO bank_trans VALUES("38","-100","expense","2","2017-08-30","1","wha","hjhj","3","1","","2017-08-30 10:17:51");
INSERT INTO bank_trans VALUES("39","-100","expense","2","2017-08-30","1","212","des","4","2","","2017-08-30 11:43:55");
INSERT INTO bank_trans VALUES("40","4484","cash-in-by-sale","2","2017-08-30","1","","Payment for INV-0044","1","2","","2017-08-30 17:37:40");
INSERT INTO bank_trans VALUES("41","27500","cash-in-by-sale","1","2017-08-31","1","","Payment for INV-0045","1","2","","2017-08-31 01:06:53");
INSERT INTO bank_trans VALUES("42","100000","cash-in","4","2017-08-31","4","opening balance","opening balance","1","1","","0000-00-00 00:00:00");
INSERT INTO bank_trans VALUES("43","50000","deposit","4","2017-08-31","4","","Deposit against service","5","2","","2017-08-31 12:22:25");
INSERT INTO bank_trans VALUES("44","-20000","cash-out-by-transfer","4","2017-08-31","4","","Transfer of amount against goods purchase","0","2","","0000-00-00 00:00:00");
INSERT INTO bank_trans VALUES("45","20000","cash-in-by-transfer","3","2017-08-31","4","","Transfer of amount against goods purchase","0","2","","0000-00-00 00:00:00");
INSERT INTO bank_trans VALUES("46","108300","cash-in-by-sale","4","2017-08-31","4","","Payment for INV-0047","1","2","","2017-08-31 12:43:00");
INSERT INTO bank_trans VALUES("47","-120000","expense","4","2017-08-31","4","","salary paid","6","2","","2017-08-31 12:45:53");





CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `country` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=243 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO countries VALUES("1","United States","US");
INSERT INTO countries VALUES("2","Canada","CA");
INSERT INTO countries VALUES("3","Afghanistan","AF");
INSERT INTO countries VALUES("4","Albania","AL");
INSERT INTO countries VALUES("5","Algeria","DZ");
INSERT INTO countries VALUES("6","American Samoa","AS");
INSERT INTO countries VALUES("7","Andorra","AD");
INSERT INTO countries VALUES("8","Angola","AO");
INSERT INTO countries VALUES("9","Anguilla","AI");
INSERT INTO countries VALUES("10","Antarctica","AQ");
INSERT INTO countries VALUES("11","Antigua and/or Barbuda","AG");
INSERT INTO countries VALUES("12","Argentina","AR");
INSERT INTO countries VALUES("13","Armenia","AM");
INSERT INTO countries VALUES("14","Aruba","AW");
INSERT INTO countries VALUES("15","Australia","AU");
INSERT INTO countries VALUES("16","Austria","AT");
INSERT INTO countries VALUES("17","Azerbaijan","AZ");
INSERT INTO countries VALUES("18","Bahamas","BS");
INSERT INTO countries VALUES("19","Bahrain","BH");
INSERT INTO countries VALUES("20","Bangladesh","BD");
INSERT INTO countries VALUES("21","Barbados","BB");
INSERT INTO countries VALUES("22","Belarus","BY");
INSERT INTO countries VALUES("23","Belgium","BE");
INSERT INTO countries VALUES("24","Belize","BZ");
INSERT INTO countries VALUES("25","Benin","BJ");
INSERT INTO countries VALUES("26","Bermuda","BM");
INSERT INTO countries VALUES("27","Bhutan","BT");
INSERT INTO countries VALUES("28","Bolivia","BO");
INSERT INTO countries VALUES("29","Bosnia and Herzegovina","BA");
INSERT INTO countries VALUES("30","Botswana","BW");
INSERT INTO countries VALUES("31","Bouvet Island","BV");
INSERT INTO countries VALUES("32","Brazil","BR");
INSERT INTO countries VALUES("33","British lndian Ocean Territory","IO");
INSERT INTO countries VALUES("34","Brunei Darussalam","BN");
INSERT INTO countries VALUES("35","Bulgaria","BG");
INSERT INTO countries VALUES("36","Burkina Faso","BF");
INSERT INTO countries VALUES("37","Burundi","BI");
INSERT INTO countries VALUES("38","Cambodia","KH");
INSERT INTO countries VALUES("39","Cameroon","CM");
INSERT INTO countries VALUES("40","Cape Verde","CV");
INSERT INTO countries VALUES("41","Cayman Islands","KY");
INSERT INTO countries VALUES("42","Central African Republic","CF");
INSERT INTO countries VALUES("43","Chad","TD");
INSERT INTO countries VALUES("44","Chile","CL");
INSERT INTO countries VALUES("45","China","CN");
INSERT INTO countries VALUES("46","Christmas Island","CX");
INSERT INTO countries VALUES("47","Cocos (Keeling) Islands","CC");
INSERT INTO countries VALUES("48","Colombia","CO");
INSERT INTO countries VALUES("49","Comoros","KM");
INSERT INTO countries VALUES("50","Congo","CG");
INSERT INTO countries VALUES("51","Cook Islands","CK");
INSERT INTO countries VALUES("52","Costa Rica","CR");
INSERT INTO countries VALUES("53","Croatia (Hrvatska)","HR");
INSERT INTO countries VALUES("54","Cuba","CU");
INSERT INTO countries VALUES("55","Cyprus","CY");
INSERT INTO countries VALUES("56","Czech Republic","CZ");
INSERT INTO countries VALUES("57","Democratic Republic of Congo","CD");
INSERT INTO countries VALUES("58","Denmark","DK");
INSERT INTO countries VALUES("59","Djibouti","DJ");
INSERT INTO countries VALUES("60","Dominica","DM");
INSERT INTO countries VALUES("61","Dominican Republic","DO");
INSERT INTO countries VALUES("62","East Timor","TP");
INSERT INTO countries VALUES("63","Ecudaor","EC");
INSERT INTO countries VALUES("64","Egypt","EG");
INSERT INTO countries VALUES("65","El Salvador","SV");
INSERT INTO countries VALUES("66","Equatorial Guinea","GQ");
INSERT INTO countries VALUES("67","Eritrea","ER");
INSERT INTO countries VALUES("68","Estonia","EE");
INSERT INTO countries VALUES("69","Ethiopia","ET");
INSERT INTO countries VALUES("70","Falkland Islands (Malvinas)","FK");
INSERT INTO countries VALUES("71","Faroe Islands","FO");
INSERT INTO countries VALUES("72","Fiji","FJ");
INSERT INTO countries VALUES("73","Finland","FI");
INSERT INTO countries VALUES("74","France","FR");
INSERT INTO countries VALUES("75","France, Metropolitan","FX");
INSERT INTO countries VALUES("76","French Guiana","GF");
INSERT INTO countries VALUES("77","French Polynesia","PF");
INSERT INTO countries VALUES("78","French Southern Territories","TF");
INSERT INTO countries VALUES("79","Gabon","GA");
INSERT INTO countries VALUES("80","Gambia","GM");
INSERT INTO countries VALUES("81","Georgia","GE");
INSERT INTO countries VALUES("82","Germany","DE");
INSERT INTO countries VALUES("83","Ghana","GH");
INSERT INTO countries VALUES("84","Gibraltar","GI");
INSERT INTO countries VALUES("85","Greece","GR");
INSERT INTO countries VALUES("86","Greenland","GL");
INSERT INTO countries VALUES("87","Grenada","GD");
INSERT INTO countries VALUES("88","Guadeloupe","GP");
INSERT INTO countries VALUES("89","Guam","GU");
INSERT INTO countries VALUES("90","Guatemala","GT");
INSERT INTO countries VALUES("91","Guinea","GN");
INSERT INTO countries VALUES("92","Guinea-Bissau","GW");
INSERT INTO countries VALUES("93","Guyana","GY");
INSERT INTO countries VALUES("94","Haiti","HT");
INSERT INTO countries VALUES("95","Heard and Mc Donald Islands","HM");
INSERT INTO countries VALUES("96","Honduras","HN");
INSERT INTO countries VALUES("97","Hong Kong","HK");
INSERT INTO countries VALUES("98","Hungary","HU");
INSERT INTO countries VALUES("99","Iceland","IS");
INSERT INTO countries VALUES("100","India","IN");
INSERT INTO countries VALUES("101","Indonesia","ID");
INSERT INTO countries VALUES("102","Iran (Islamic Republic of)","IR");
INSERT INTO countries VALUES("103","Iraq","IQ");
INSERT INTO countries VALUES("104","Ireland","IE");
INSERT INTO countries VALUES("105","Israel","IL");
INSERT INTO countries VALUES("106","Italy","IT");
INSERT INTO countries VALUES("107","Ivory Coast","CI");
INSERT INTO countries VALUES("108","Jamaica","JM");
INSERT INTO countries VALUES("109","Japan","JP");
INSERT INTO countries VALUES("110","Jordan","JO");
INSERT INTO countries VALUES("111","Kazakhstan","KZ");
INSERT INTO countries VALUES("112","Kenya","KE");
INSERT INTO countries VALUES("113","Kiribati","KI");
INSERT INTO countries VALUES("114","Korea, Democratic People\'s Republic of","KP");
INSERT INTO countries VALUES("115","Korea, Republic of","KR");
INSERT INTO countries VALUES("116","Kuwait","KW");
INSERT INTO countries VALUES("117","Kyrgyzstan","KG");
INSERT INTO countries VALUES("118","Lao People\'s Democratic Republic","LA");
INSERT INTO countries VALUES("119","Latvia","LV");
INSERT INTO countries VALUES("120","Lebanon","LB");
INSERT INTO countries VALUES("121","Lesotho","LS");
INSERT INTO countries VALUES("122","Liberia","LR");
INSERT INTO countries VALUES("123","Libyan Arab Jamahiriya","LY");
INSERT INTO countries VALUES("124","Liechtenstein","LI");
INSERT INTO countries VALUES("125","Lithuania","LT");
INSERT INTO countries VALUES("126","Luxembourg","LU");
INSERT INTO countries VALUES("127","Macau","MO");
INSERT INTO countries VALUES("128","Macedonia","MK");
INSERT INTO countries VALUES("129","Madagascar","MG");
INSERT INTO countries VALUES("130","Malawi","MW");
INSERT INTO countries VALUES("131","Malaysia","MY");
INSERT INTO countries VALUES("132","Maldives","MV");
INSERT INTO countries VALUES("133","Mali","ML");
INSERT INTO countries VALUES("134","Malta","MT");
INSERT INTO countries VALUES("135","Marshall Islands","MH");
INSERT INTO countries VALUES("136","Martinique","MQ");
INSERT INTO countries VALUES("137","Mauritania","MR");
INSERT INTO countries VALUES("138","Mauritius","MU");
INSERT INTO countries VALUES("139","Mayotte","TY");
INSERT INTO countries VALUES("140","Mexico","MX");
INSERT INTO countries VALUES("141","Micronesia, Federated States of","FM");
INSERT INTO countries VALUES("142","Moldova, Republic of","MD");
INSERT INTO countries VALUES("143","Monaco","MC");
INSERT INTO countries VALUES("144","Mongolia","MN");
INSERT INTO countries VALUES("145","Montserrat","MS");
INSERT INTO countries VALUES("146","Morocco","MA");
INSERT INTO countries VALUES("147","Mozambique","MZ");
INSERT INTO countries VALUES("148","Myanmar","MM");
INSERT INTO countries VALUES("149","Namibia","NA");
INSERT INTO countries VALUES("150","Nauru","NR");
INSERT INTO countries VALUES("151","Nepal","NP");
INSERT INTO countries VALUES("152","Netherlands","NL");
INSERT INTO countries VALUES("153","Netherlands Antilles","AN");
INSERT INTO countries VALUES("154","New Caledonia","NC");
INSERT INTO countries VALUES("155","New Zealand","NZ");
INSERT INTO countries VALUES("156","Nicaragua","NI");
INSERT INTO countries VALUES("157","Niger","NE");
INSERT INTO countries VALUES("158","Nigeria","NG");
INSERT INTO countries VALUES("159","Niue","NU");
INSERT INTO countries VALUES("160","Norfork Island","NF");
INSERT INTO countries VALUES("161","Northern Mariana Islands","MP");
INSERT INTO countries VALUES("162","Norway","NO");
INSERT INTO countries VALUES("163","Oman","OM");
INSERT INTO countries VALUES("164","Pakistan","PK");
INSERT INTO countries VALUES("165","Palau","PW");
INSERT INTO countries VALUES("166","Panama","PA");
INSERT INTO countries VALUES("167","Papua New Guinea","PG");
INSERT INTO countries VALUES("168","Paraguay","PY");
INSERT INTO countries VALUES("169","Peru","PE");
INSERT INTO countries VALUES("170","Philippines","PH");
INSERT INTO countries VALUES("171","Pitcairn","PN");
INSERT INTO countries VALUES("172","Poland","PL");
INSERT INTO countries VALUES("173","Portugal","PT");
INSERT INTO countries VALUES("174","Puerto Rico","PR");
INSERT INTO countries VALUES("175","Qatar","QA");
INSERT INTO countries VALUES("176","Republic of South Sudan","SS");
INSERT INTO countries VALUES("177","Reunion","RE");
INSERT INTO countries VALUES("178","Romania","RO");
INSERT INTO countries VALUES("179","Russian Federation","RU");
INSERT INTO countries VALUES("180","Rwanda","RW");
INSERT INTO countries VALUES("181","Saint Kitts and Nevis","KN");
INSERT INTO countries VALUES("182","Saint Lucia","LC");
INSERT INTO countries VALUES("183","Saint Vincent and the Grenadines","VC");
INSERT INTO countries VALUES("184","Samoa","WS");
INSERT INTO countries VALUES("185","San Marino","SM");
INSERT INTO countries VALUES("186","Sao Tome and Principe","ST");
INSERT INTO countries VALUES("187","Saudi Arabia","SA");
INSERT INTO countries VALUES("188","Senegal","SN");
INSERT INTO countries VALUES("189","Serbia","RS");
INSERT INTO countries VALUES("190","Seychelles","SC");
INSERT INTO countries VALUES("191","Sierra Leone","SL");
INSERT INTO countries VALUES("192","Singapore","SG");
INSERT INTO countries VALUES("193","Slovakia","SK");
INSERT INTO countries VALUES("194","Slovenia","SI");
INSERT INTO countries VALUES("195","Solomon Islands","SB");
INSERT INTO countries VALUES("196","Somalia","SO");
INSERT INTO countries VALUES("197","South Africa","ZA");
INSERT INTO countries VALUES("198","South Georgia South Sandwich Islands","GS");
INSERT INTO countries VALUES("199","Spain","ES");
INSERT INTO countries VALUES("200","Sri Lanka","LK");
INSERT INTO countries VALUES("201","St. Helena","SH");
INSERT INTO countries VALUES("202","St. Pierre and Miquelon","PM");
INSERT INTO countries VALUES("203","Sudan","SD");
INSERT INTO countries VALUES("204","Suriname","SR");
INSERT INTO countries VALUES("205","Svalbarn and Jan Mayen Islands","SJ");
INSERT INTO countries VALUES("206","Swaziland","SZ");
INSERT INTO countries VALUES("207","Sweden","SE");
INSERT INTO countries VALUES("208","Switzerland","CH");
INSERT INTO countries VALUES("209","Syrian Arab Republic","SY");
INSERT INTO countries VALUES("210","Taiwan","TW");
INSERT INTO countries VALUES("211","Tajikistan","TJ");
INSERT INTO countries VALUES("212","Tanzania, United Republic of","TZ");
INSERT INTO countries VALUES("213","Thailand","TH");
INSERT INTO countries VALUES("214","Togo","TG");
INSERT INTO countries VALUES("215","Tokelau","TK");
INSERT INTO countries VALUES("216","Tonga","TO");
INSERT INTO countries VALUES("217","Trinidad and Tobago","TT");
INSERT INTO countries VALUES("218","Tunisia","TN");
INSERT INTO countries VALUES("219","Turkey","TR");
INSERT INTO countries VALUES("220","Turkmenistan","TM");
INSERT INTO countries VALUES("221","Turks and Caicos Islands","TC");
INSERT INTO countries VALUES("222","Tuvalu","TV");
INSERT INTO countries VALUES("223","Uganda","UG");
INSERT INTO countries VALUES("224","Ukraine","UA");
INSERT INTO countries VALUES("225","United Arab Emirates","AE");
INSERT INTO countries VALUES("226","United Kingdom","GB");
INSERT INTO countries VALUES("227","United States minor outlying islands","UM");
INSERT INTO countries VALUES("228","Uruguay","UY");
INSERT INTO countries VALUES("229","Uzbekistan","UZ");
INSERT INTO countries VALUES("230","Vanuatu","VU");
INSERT INTO countries VALUES("231","Vatican City State","VA");
INSERT INTO countries VALUES("232","Venezuela","VE");
INSERT INTO countries VALUES("233","Vietnam","VN");
INSERT INTO countries VALUES("234","Virgin Islands (British)","VG");
INSERT INTO countries VALUES("235","Virgin Islands (U.S.)","VI");
INSERT INTO countries VALUES("236","Wallis and Futuna Islands","WF");
INSERT INTO countries VALUES("237","Western Sahara","EH");
INSERT INTO countries VALUES("238","Yemen","YE");
INSERT INTO countries VALUES("239","Yugoslavia","YU");
INSERT INTO countries VALUES("240","Zaire","ZR");
INSERT INTO countries VALUES("241","Zambia","ZM");
INSERT INTO countries VALUES("242","Zimbabwe","ZW");





CREATE TABLE IF NOT EXISTS `currency` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `symbol` char(5) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO currency VALUES("1","USD","$");
INSERT INTO currency VALUES("3","INR","?");





CREATE TABLE IF NOT EXISTS `cust_branch` (
  `branch_code` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `debtor_no` int(11) NOT NULL,
  `br_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `br_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `br_contact` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `billing_street` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `billing_city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `billing_state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `billing_zip_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `billing_country_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `shipping_street` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `shipping_city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `shipping_state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `shipping_zip_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `shipping_country_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`branch_code`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO cust_branch VALUES("1","1","Aminul Islam","","","steaa","States","Assam","6511","IN","steaa","States","tee","6511","HU");
INSERT INTO cust_branch VALUES("2","2","Test Customer","","","dsafdsafdsa","ddfd","Maharashtra","dasfdsafdsa","US","dsafdsafdsa","ddfd","Maharashtra","dasfdsafdsa","US");
INSERT INTO cust_branch VALUES("3","3","Test Customer 2","","","Test Street","Test City","Assam","587455","IN","Test Street","Test City","Assam","587455","IN");
INSERT INTO cust_branch VALUES("4","4","Test Customer 3","","","Dhaka","Dhaka","Assam","78444","IN","Dhaka","Dhaka","Haryana","78444","IN");
INSERT INTO cust_branch VALUES("5","5","Al Mahmud Reza","","","Dhaka","Dhaka","","78444","US","Dhaka","Dhaka","New York","78444","US");
INSERT INTO cust_branch VALUES("6","6","Ravi Teja","","","123","jabalpur","Madhya Pradesh","78444","IN","123","jabalpur","Madhya Pradesh","78444","IN");
INSERT INTO cust_branch VALUES("7","1","Iris Watson","","","Fusce Rd","Fusce Rd","Assam","65111","IN","Fusce Rd","Fusce Rd","Assam","65111","IN");
INSERT INTO cust_branch VALUES("8","2","John Smith","","","Washington","Washington","","87444","US","Washington","Washington","United Kingdom","87444","US");
INSERT INTO cust_branch VALUES("9","3","rajesh","","","24 street","Patna","Bihar","687872","IN","24 street","Patna","Bihar","687872","IN");
INSERT INTO cust_branch VALUES("10","4","Reza","","","dhaka","dhaka","Assam","8024","IN","dhaka","dhaka","asam","8024","IN");
INSERT INTO cust_branch VALUES("11","5","Reza","","","dhaka","dhaka","","1212","CA","dhaka","dhaka","dhaka","1212","CA");
INSERT INTO cust_branch VALUES("12","6","DFAH TEST COMPANY","","","Sector-4","Rewari","Haryana","123401","IN","Sector-4","Rewari","Haryana","123401","IN");
INSERT INTO cust_branch VALUES("13","7","Cust1","","","Dadar","Maharashtra","Maharashtra","","IN","Dadar","Maharashtra","Maharashtra","","IN");
INSERT INTO cust_branch VALUES("14","8","gffg","","","hgfh","fghfgh","","fghhfghf","AF","ghfggh","trerter","tghrtr","hgfghgf","AF");
INSERT INTO cust_branch VALUES("15","9","psrinuhp","","","Main Road Bhilai","Bhilai","Jharkhand","560068","IN","Main Road Raipur","Raipur","Jharkhand","560068","IN");
INSERT INTO cust_branch VALUES("16","10","sahji","","","dfeijpknlm","assam","Assam","","IN","","","","","");
INSERT INTO cust_branch VALUES("17","11","tester","","","tester street","London","","sw19 5ex","GB","tester street","London","","sw19 5ex","GB");
INSERT INTO cust_branch VALUES("18","12","Mahipal Yadav","","","Sector-4","Rewari","Haryana","123401","IN","Sector-4","Rewari","Haryana","123401","IN");
INSERT INTO cust_branch VALUES("19","13","kiki","","","vlp","coimbatore","","Y87777","IN","vlp","coimbatore","tamil nadu","Y87777","IN");
INSERT INTO cust_branch VALUES("20","14","kiruthika","","","vlp","coimbatore","Tamil Nadu","Y87777","IN","vlp","coimbatore","tamil nadu","Y87777","IN");
INSERT INTO cust_branch VALUES("21","15","You Sun","","","Bhilai main road","Bhilai","Jharkhand","12345","IN","Main Raipur","Raipur","jharkhand","56567","IN");





CREATE TABLE IF NOT EXISTS `custom_item_orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_no` int(11) NOT NULL,
  `tax_type_id` tinyint(4) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` double NOT NULL,
  `unit_price` double NOT NULL,
  `discount_percent` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;






CREATE TABLE IF NOT EXISTS `debtors_master` (
  `debtor_no` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `sales_type` int(11) NOT NULL,
  `gstin` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `inactive` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`debtor_no`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO debtors_master VALUES("1","Iris Watson","iris@yahoo.com","","","(372) 587-2335","0","gst2335","","0","2017-07-26 09:27:04","");
INSERT INTO debtors_master VALUES("2","John Smith","customer@techvill.net","","","(372) 587-2335","0","","","0","2017-07-26 09:28:33","");
INSERT INTO debtors_master VALUES("3","rajesh","apatel.en@gmail.com","","","9898487501","0","345ghatgrsv156","","0","2017-07-27 11:22:44","");
INSERT INTO debtors_master VALUES("4","Reza2","reza@gmail.com","","","01920677225","0","4565","","0","2017-07-30 05:37:46","");
INSERT INTO debtors_master VALUES("5","Reza","reza.techvill@gmail.com","","","01920677225","0","258","","0","2017-07-30 05:37:54","");
INSERT INTO debtors_master VALUES("6","DFAH TEST COMPANY","dfah@dfah.com","","","1234556764","0","23432345432","","0","2017-08-05 14:49:42","");
INSERT INTO debtors_master VALUES("7","Cust1","cust@cust.com","","","9898098980","0","98998","","0","2017-08-21 13:18:08","");
INSERT INTO debtors_master VALUES("8","gffg","fghghdf@gfh.hgjgf","","","gfhfgh","0","fghfghgf","","0","2017-08-24 07:32:12","");
INSERT INTO debtors_master VALUES("9","psrinuhp","psrinuhp@gmail.com","","","9916120006","0","","","0","2017-08-24 13:20:26","");
INSERT INTO debtors_master VALUES("10","sahji","sahil@gmail.com","","","9696969587","0","dfihlnjsdlkhvnsdkn","","0","2017-08-27 06:29:53","");
INSERT INTO debtors_master VALUES("11","tester","info@tester.co.uk","","","09854 454 454","0","","","0","2017-08-28 14:19:59","");
INSERT INTO debtors_master VALUES("12","Mahipal Yadav","mahipalyadav007@gmail.com","","","9812073666","0","22AAAAA0000A1Z1","","0","2017-08-28 17:55:44","");
INSERT INTO debtors_master VALUES("13","kiki","k@gmail.com","","","7766767676","0","3","","0","2017-08-30 09:42:04","");
INSERT INTO debtors_master VALUES("14","kiruthika","kk@gmail.com","","","7766767676","0","3","","0","2017-08-30 09:43:38","");
INSERT INTO debtors_master VALUES("15","You Sun","yousun@yahoo.co.in","","","5678567890","0","","","0","2017-08-31 12:11:55","");





CREATE TABLE IF NOT EXISTS `email_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email_protocol` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_encryption` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `smtp_host` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `smtp_port` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `smtp_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `smtp_username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `smtp_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `from_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `from_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO email_config VALUES("1","smtp","tls","mail.techvill.net","587","stockpile@techvill.net","stockpile@techvill.net","nT4HD2XEdRUX","stockpile@techvill.net","stockpile@techvill.net");





CREATE TABLE IF NOT EXISTS `email_temp_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `temp_id` tinyint(4) NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `lang` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `lang_id` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO email_temp_details VALUES("1","2","Your Quotation # {order_reference_no} from {company_name} has been shipped","Hi {customer_name},<br><br>Thank you for your Quotation. Here�s a brief overview of your shipment:<br>Quotation # {order_reference_no} was packed on {packed_date} and shipped on {delivery_date}.<br> <br><b>Shipping address   </b><br><br>{shipping_street}<br>{shipping_city}<br>{shipping_state}<br>{shipping_zip_code}<br>{shipping_country}<br><br><b>Item Summery</b><br>{item_information}<br> <br>If you have any questions, please feel free to reply to this email.<br><br>Regards<br>{company_name}<br><br><br>","en","1");
INSERT INTO email_temp_details VALUES("2","2","Subject","Body","ar","2");
INSERT INTO email_temp_details VALUES("3","2","Subject","Body","ch","3");
INSERT INTO email_temp_details VALUES("4","2","Subject","Body","fr","4");
INSERT INTO email_temp_details VALUES("5","2","Subject","Body","po","5");
INSERT INTO email_temp_details VALUES("6","2","Subject","Body","rs","6");
INSERT INTO email_temp_details VALUES("7","2","Subject","Body","sp","7");
INSERT INTO email_temp_details VALUES("8","2","Subject","Body","tu","8");
INSERT INTO email_temp_details VALUES("9","1","Payment information for Quotation#{order_reference_no} and Invoice#{invoice_reference_no}.","<p>Hi {customer_name},</p><p>Thank you for purchase our product and pay for this.</p><p>We just want to confirm a few details about payment information:</p><p><b>Customer Information</b></p><p>{billing_street}</p><p>{billing_city}</p><p>{billing_state}</p><p>{billing_zip_code}</p><p>{billing_country}<br></p><p><b>Payment Summary<br></b></p><p><b></b><i>Payment No : {payment_id}</i></p><p><i>Payment Date : {payment_date}&nbsp;</i></p><p><i>Payment Method : {payment_method} <br></i></p><p><i><b>Total Amount : {total_amount}</b></i></p><p><i>Quotation No : {order_reference_no}</i><br><i></i></p><p><i>Invoice No : {invoice_reference_no}</i><br></p><p><br></p><p>Regards,</p><p>{company_name}<br></p><br><br><br><br><br><br>","en","1");
INSERT INTO email_temp_details VALUES("10","1","Subject","Body","ar","2");
INSERT INTO email_temp_details VALUES("11","1","Subject","Body","ch","3");
INSERT INTO email_temp_details VALUES("12","1","Subject","Body","fr","4");
INSERT INTO email_temp_details VALUES("13","1","Subject","Body","po","5");
INSERT INTO email_temp_details VALUES("14","1","Subject","Body","rs","6");
INSERT INTO email_temp_details VALUES("15","1","Subject","Body","sp","7");
INSERT INTO email_temp_details VALUES("16","1","Subject","Body","tu","8");
INSERT INTO email_temp_details VALUES("17","3","Payment information for Quotation#{order_reference_no} and Invoice#{invoice_reference_no}.","<p>Hi {customer_name},</p><p>Thank you for purchase our product and pay for this.</p><p>We just want to confirm a few details about payment information:</p><p><b>Customer Information</b></p><p>{billing_street}</p><p>{billing_city}</p><p>{billing_state}</p><p>{billing_zip_code}<br></p><p>{billing_country}<br>&nbsp; &nbsp; &nbsp; &nbsp; <br></p><p><b>Payment Summary<br></b></p><p><b></b><i>Payment No : {payment_id}</i></p><p><i>Payment Date : {payment_date}&nbsp;</i></p><p><i>Payment Method : {payment_method} <br></i></p><p><i><b>Total Amount : {total_amount}</b><br>Quotation No : {order_reference_no}<br>&nbsp;</i><i>Invoice No : {invoice_reference_no}<br>&nbsp;</i>Regards,</p><p>{company_name} <br></p><br>","en","1");
INSERT INTO email_temp_details VALUES("18","3","Subject","Body","ar","2");
INSERT INTO email_temp_details VALUES("19","3","Subject","Body","ch","3");
INSERT INTO email_temp_details VALUES("20","3","Subject","Body","fr","4");
INSERT INTO email_temp_details VALUES("21","3","Subject","Body","po","5");
INSERT INTO email_temp_details VALUES("22","3","Subject","Body","rs","6");
INSERT INTO email_temp_details VALUES("23","3","Subject","Body","sp","7");
INSERT INTO email_temp_details VALUES("24","3","Subject","Body","tu","8");
INSERT INTO email_temp_details VALUES("25","4","Your Invoice # {invoice_reference_no} for Quotation #{order_reference_no} from {company_name} has been created.","<p>Hi {customer_name},</p><p>Thank you for your order. Here�s a brief overview of your invoice: Invoice #{invoice_reference_no} is for Quotation #{order_reference_no}. The invoice total is {currency}{total_amount}, please pay before {due_date}.</p><p>If you have any questions, please feel free to reply to this email. </p><p><b>Billing address</b></p><p>&nbsp;{billing_street}</p><p>&nbsp;{billing_city}</p><p>&nbsp;{billing_state}</p><p>&nbsp;{billing_zip_code}</p><p>&nbsp;{billing_country}<br></p><p><br></p><p><b>Quotation summary<br></b></p><p><b></b>{invoice_summery}<br></p><p>Regards,</p><p>{company_name}<br></p><br><br>","en","1");
INSERT INTO email_temp_details VALUES("26","4","Subject","Body","ar","2");
INSERT INTO email_temp_details VALUES("27","4","Subject","Body","ch","3");
INSERT INTO email_temp_details VALUES("28","4","Subject","Body","fr","4");
INSERT INTO email_temp_details VALUES("29","4","Subject","Body","po","5");
INSERT INTO email_temp_details VALUES("30","4","Subject","Body","rs","6");
INSERT INTO email_temp_details VALUES("31","4","Subject","Body","sp","7");
INSERT INTO email_temp_details VALUES("32","4","Subject","Body","tu","8");
INSERT INTO email_temp_details VALUES("33","5","Your Quotation # {order_reference_no} from {company_name} has been created.","<p>Hi {customer_name},</p><p>Thank you for your order. Here�s a brief overview of your Quotation #{order_reference_no} that was created on {order_date}. The order total is {currency}{total_amount}.</p><p>If you have any questions, please feel free to reply to this email. </p><p><b>Billing address</b></p><p>&nbsp;{billing_street}</p><p>&nbsp;{billing_city}</p><p>&nbsp;{billing_state}</p><p>&nbsp;{billing_zip_code}</p><p>&nbsp;{billing_country}<br></p><p><br></p><p><b>Quotation summary<br></b></p><p><b></b>{order_summery}<br></p><p>Regards,</p><p>{company_name}</p><br><br>","en","1");
INSERT INTO email_temp_details VALUES("34","5","Subject","Body","ar","2");
INSERT INTO email_temp_details VALUES("35","5","Subject","Body","ch","3");
INSERT INTO email_temp_details VALUES("36","5","Subject","Body","fr","4");
INSERT INTO email_temp_details VALUES("37","5","Subject","Body","po","5");
INSERT INTO email_temp_details VALUES("38","5","Subject","Body","rs","6");
INSERT INTO email_temp_details VALUES("39","5","Subject","Body","sp","7");
INSERT INTO email_temp_details VALUES("40","5","Subject","Body","tu","8");
INSERT INTO email_temp_details VALUES("41","6","Your Quotation # {order_reference_no} from {company_name} has been packed","Hi {customer_name},<br><br>Thank you for your order. Here�s a brief overview of your shipment:<br>Quotation # {order_reference_no} was packed on {packed_date}.<br> <br><b>Shipping address   </b><br><br>{shipping_street}<br>{shipping_city}<br>{shipping_state}<br>{shipping_zip_code}<br>{shipping_country}<br><br><b>Item Summery</b><br>{item_information}<br> <br>If you have any questions, please feel free to reply to this email.<br><br>Regards<br>{company_name}<br><br><br>","en","1");
INSERT INTO email_temp_details VALUES("42","6","Subject","Body","ar","2");
INSERT INTO email_temp_details VALUES("43","6","Subject","Body","ch","3");
INSERT INTO email_temp_details VALUES("44","6","Subject","Body","fr","4");
INSERT INTO email_temp_details VALUES("45","6","Subject","Body","po","5");
INSERT INTO email_temp_details VALUES("46","6","Subject","Body","rs","6");
INSERT INTO email_temp_details VALUES("47","6","Subject","Body","sp","7");
INSERT INTO email_temp_details VALUES("48","6","Subject","Body","tu","8");





CREATE TABLE IF NOT EXISTS `income_expense_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO income_expense_categories VALUES("1","Sales","income");
INSERT INTO income_expense_categories VALUES("2","Sallery","income");
INSERT INTO income_expense_categories VALUES("3","Utility Bill","expense");
INSERT INTO income_expense_categories VALUES("4","Repair & MaintEnance","expense");
INSERT INTO income_expense_categories VALUES("5","SERVICE","income");
INSERT INTO income_expense_categories VALUES("6","SERVICE EXP","expense");
INSERT INTO income_expense_categories VALUES("7","debit","income");





CREATE TABLE IF NOT EXISTS `invoice_payment_terms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `terms` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `days_before_due` tinyint(4) NOT NULL,
  `defaults` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO invoice_payment_terms VALUES("1","Cash on deleivery","0","0");
INSERT INTO invoice_payment_terms VALUES("2","NEFT","1","0");
INSERT INTO invoice_payment_terms VALUES("3","RTGS","1","0");
INSERT INTO invoice_payment_terms VALUES("4","Cheque","3","1");
INSERT INTO invoice_payment_terms VALUES("5","Cash","1","0");





CREATE TABLE IF NOT EXISTS `item_code` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `stock_id` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` tinyint(4) NOT NULL,
  `item_image` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `hsn` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `inactive` tinyint(4) NOT NULL DEFAULT '0',
  `deleted_status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO item_code VALUES("8","22222","TABLET","3","","7676766166","0","0","","");
INSERT INTO item_code VALUES("6","1001","CAMERA","2","","80011","0","1","","2017-08-20 06:46:22");
INSERT INTO item_code VALUES("7","76767","IPHONE","2","","78787772","0","0","","");
INSERT INTO item_code VALUES("4","HP","HP PRO BOOK","2","hpprobook.jpg","hsn_no","0","0","","2017-08-29 20:30:20");
INSERT INTO item_code VALUES("5","LENEVO","LED TV","1","ledtv.jpg","LEDHSN","0","0","","");
INSERT INTO item_code VALUES("9","TT1","PEN","3","","","0","0","","");
INSERT INTO item_code VALUES("10","N326","JEANS","2","","","0","0","","2017-08-02 07:43:30");
INSERT INTO item_code VALUES("11","910001","DFAH SUPPORT SERVICE","1","A023.png","99922","0","0","","2017-08-23 19:41:01");
INSERT INTO item_code VALUES("12","RP01","RP01","2","","Rp01","0","0","","2017-08-21 18:07:54");
INSERT INTO item_code VALUES("13","DF","SDFDSF","2","gallery_banner2.png","dfsd","1","0","","2017-08-22 10:57:50");
INSERT INTO item_code VALUES("14","DFGDFG","Sarojni","3","imgonline-com-ua-twotoone-rZIp12l0k7fy7inR.jpg","fgdfgdfg","0","0","","2017-08-24 07:55:28");
INSERT INTO item_code VALUES("15","HYDF","GJHDFGJ","1","","","0","0","","");
INSERT INTO item_code VALUES("16","IT100","ITEM NAME","2","facebook.png","HSN100","0","0","","");
INSERT INTO item_code VALUES("17","FVFVE","TSHIRT","2","","fvve","0","0","","2017-08-25 16:58:03");
INSERT INTO item_code VALUES("18","RS123","FURNITURE","2","","","0","0","","2017-08-28 05:07:23");
INSERT INTO item_code VALUES("19","10","WEBSIT4","2","","23433454","0","0","","2017-08-28 18:21:41");
INSERT INTO item_code VALUES("20","222","ASSESS","2","","444544","0","0","","");
INSERT INTO item_code VALUES("21","06","NANA","3","","2","0","0","","");
INSERT INTO item_code VALUES("22","8","BANANA","3","","2","0","0","","");
INSERT INTO item_code VALUES("23","KK","DELL Laptop","7","DellIco.png","15","0","0","","2017-08-31 12:31:38");
INSERT INTO item_code VALUES("24","123","ABC","1","","","0","0","","");





CREATE TABLE IF NOT EXISTS `item_tax_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `tax_rate` double(8,2) NOT NULL,
  `defaults` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO item_tax_types VALUES("1","Tax Exempt","0.00","0");
INSERT INTO item_tax_types VALUES("2","GST @10","10.00","0");
INSERT INTO item_tax_types VALUES("3","GST @15","15.00","0");
INSERT INTO item_tax_types VALUES("4","GST @5","5.00","0");
INSERT INTO item_tax_types VALUES("5","GST@18%","18.00","0");
INSERT INTO item_tax_types VALUES("6","CGST 9%","9.00","0");
INSERT INTO item_tax_types VALUES("7","SGST 9%","9.00","0");
INSERT INTO item_tax_types VALUES("8","GST@28","28.00","1");





CREATE TABLE IF NOT EXISTS `item_unit` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `abbr` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `inactive` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO item_unit VALUES("1","each","Each","0","","");
INSERT INTO item_unit VALUES("3","dozens","Dozens","0","2017-08-31 11:23:07","");





CREATE TABLE IF NOT EXISTS `location` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `loc_code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `location_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_address` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `fax` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `contact` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `inactive` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO location VALUES("1","PL","Primary Location","Primary Location","","","","Primary Location","0","2017-07-22 18:13:53","");
INSERT INTO location VALUES("2","JA","Jackson Av","125 Hayes St, San Francisco, CA 94102, USA","","","","Jackson Av","0","2017-07-22 18:13:53","");
INSERT INTO location VALUES("3","PH","Primary Hub","Mumbai","xyz@gmail.com","xxxxxx","xx","2323232323","0","2017-08-31 11:14:18","2017-08-31 11:14:57");





CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO migrations VALUES("2014_10_12_100000_create_password_resets_table","1");
INSERT INTO migrations VALUES("2015_09_26_161159_entrust_setup_tables","1");
INSERT INTO migrations VALUES("2016_08_30_100832_create_users_table","1");
INSERT INTO migrations VALUES("2016_08_30_104058_create_security_role_table","1");
INSERT INTO migrations VALUES("2016_08_30_104506_create_stock_category_table","1");
INSERT INTO migrations VALUES("2016_08_30_105339_create_location_table","1");
INSERT INTO migrations VALUES("2016_08_30_110408_create_item_code_table","1");
INSERT INTO migrations VALUES("2016_08_30_114231_create_item_unit_table","1");
INSERT INTO migrations VALUES("2016_09_02_070031_create_stock_master_table","1");
INSERT INTO migrations VALUES("2016_09_20_123717_create_stock_move_table","1");
INSERT INTO migrations VALUES("2016_10_05_113244_create_debtor_master_table","1");
INSERT INTO migrations VALUES("2016_10_05_113333_create_sales_orders_table","1");
INSERT INTO migrations VALUES("2016_10_05_113356_create_sales_order_details_table","1");
INSERT INTO migrations VALUES("2016_10_18_060431_create_supplier_table","1");
INSERT INTO migrations VALUES("2016_10_18_063931_create_purch_order_table","1");
INSERT INTO migrations VALUES("2016_10_18_064211_create_purch_order_detail_table","1");
INSERT INTO migrations VALUES("2016_11_15_121343_create_preference_table","1");
INSERT INTO migrations VALUES("2016_12_01_130110_create_shipment_table","1");
INSERT INTO migrations VALUES("2016_12_01_130443_create_shipment_details_table","1");
INSERT INTO migrations VALUES("2016_12_03_051429_create_sale_price_table","1");
INSERT INTO migrations VALUES("2016_12_03_052017_create_sales_types_table","1");
INSERT INTO migrations VALUES("2016_12_03_061206_create_purchase_price_table","1");
INSERT INTO migrations VALUES("2016_12_03_062131_create_payment_term_table","1");
INSERT INTO migrations VALUES("2016_12_03_062247_create_payment_history_table","1");
INSERT INTO migrations VALUES("2016_12_03_062932_create_item_tax_type_table","1");
INSERT INTO migrations VALUES("2016_12_03_063827_create_invoice_payment_term_table","1");
INSERT INTO migrations VALUES("2016_12_03_064157_create_email_temp_details_table","1");
INSERT INTO migrations VALUES("2016_12_03_064747_create_email_config_table","1");
INSERT INTO migrations VALUES("2016_12_03_065532_create_cust_branch_table","1");
INSERT INTO migrations VALUES("2016_12_03_065915_create_currency_table","1");
INSERT INTO migrations VALUES("2016_12_03_070030_create_country_table","1");
INSERT INTO migrations VALUES("2016_12_03_070030_create_stock_transfer_table","1");
INSERT INTO migrations VALUES("2016_12_03_071018_create_backup_table","1");
INSERT INTO migrations VALUES("2017_03_20_104506_create_bank_account_type_table","1");
INSERT INTO migrations VALUES("2017_03_20_104506_create_bank_accounts_table","1");
INSERT INTO migrations VALUES("2017_03_20_104506_create_bank_trans_table","1");
INSERT INTO migrations VALUES("2017_03_20_104506_create_custom_item_orders_table","1");
INSERT INTO migrations VALUES("2017_03_20_104506_create_income_expense_categories_table","1");
INSERT INTO migrations VALUES("2017_03_20_104506_create_month_table","1");
INSERT INTO migrations VALUES("2017_04_10_062131_create_payment_gateway_table","1");





CREATE TABLE IF NOT EXISTS `months` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO months VALUES("1","January");
INSERT INTO months VALUES("2","February");
INSERT INTO months VALUES("3","March");
INSERT INTO months VALUES("4","Appril");
INSERT INTO months VALUES("5","May");
INSERT INTO months VALUES("6","June");
INSERT INTO months VALUES("7","July");
INSERT INTO months VALUES("8","August");
INSERT INTO months VALUES("9","September");
INSERT INTO months VALUES("10","October");
INSERT INTO months VALUES("11","November");
INSERT INTO months VALUES("12","December");





CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;






CREATE TABLE IF NOT EXISTS `payment_gateway` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `site` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO payment_gateway VALUES("1","username","techvillage_business_api1.gmail.com","PayPal");
INSERT INTO payment_gateway VALUES("2","password","9DDYZX2JLA6QL668","PayPal");
INSERT INTO payment_gateway VALUES("3","signature","AFcWxV21C7fd0v3bYYYRCpSSRl31ABayz5pdk84jno7.Udj6-U8ffwbT","PayPal");
INSERT INTO payment_gateway VALUES("4","mode","sandbox","PayPal");





CREATE TABLE IF NOT EXISTS `payment_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `transaction_id` int(11) NOT NULL,
  `payment_type_id` smallint(6) NOT NULL,
  `order_reference` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `invoice_reference` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `payment_date` date NOT NULL,
  `amount` double DEFAULT '0',
  `person_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `reference` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'completed',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO payment_history VALUES("4","5","1","SO-0011","INV-0001","2017-07-26","50.75","1","1","","completed","","");
INSERT INTO payment_history VALUES("5","7","1","SO-0012","INV-0002","2017-07-27","100","1","2","","completed","","");
INSERT INTO payment_history VALUES("6","8","2","SO-0018","INV-0008","2017-07-27","138768","1","3","","completed","","");
INSERT INTO payment_history VALUES("7","9","2","SO-0017","INV-0007","2017-07-27","12744","1","3","","completed","","");
INSERT INTO payment_history VALUES("8","10","2","SO-0016","INV-0006","2017-07-28","16124.7","1","3","","completed","","");
INSERT INTO payment_history VALUES("9","12","2","SO-0029","INV-0016","2017-08-05","10900","1","6","vinod","completed","","");
INSERT INTO payment_history VALUES("10","13","2","SO-0033","INV-0019","2017-08-11","6948.95","1","1","","completed","","");
INSERT INTO payment_history VALUES("11","15","2","SO-0036","INV-0022","2017-08-21","590","1","7","","completed","","");
INSERT INTO payment_history VALUES("12","17","2","SO-0016","INV-0006","2017-08-23","118","1","3","","completed","","");
INSERT INTO payment_history VALUES("13","18","2","SO-0040","INV-0026","2017-08-24","10.8","1","1","","completed","","");
INSERT INTO payment_history VALUES("14","21","2","SO-0040","INV-0026","2017-08-25","1.944","1","1","","completed","","");
INSERT INTO payment_history VALUES("15","22","2","SO-0044","INV-0029","2017-08-25","8.8","1","1","","completed","","");
INSERT INTO payment_history VALUES("16","23","2","SO-0044","INV-0029","2017-08-25","3","1","1","","completed","","");
INSERT INTO payment_history VALUES("17","24","2","SO-0044","INV-0029","2017-08-25","3","1","1","","completed","","");
INSERT INTO payment_history VALUES("18","25","2","SO-0044","INV-0029","2017-08-25","2.9","1","1","","completed","","");
INSERT INTO payment_history VALUES("19","26","2","SO-0047","INV-0032","2017-08-27","10000","1","1","","completed","","");
INSERT INTO payment_history VALUES("20","27","2","SO-0047","INV-0032","2017-08-27","5000","1","1","","completed","","");
INSERT INTO payment_history VALUES("21","36","1","SO-0057","INV-0042","2017-08-29","110","1","2","","completed","","");
INSERT INTO payment_history VALUES("23","40","2","SO-0059","INV-0044","2017-08-30","4484","1","4","","completed","","");
INSERT INTO payment_history VALUES("24","41","2","SO-0060","INV-0045","2017-08-31","27500","1","1","","completed","","");
INSERT INTO payment_history VALUES("25","46","2","SO-0063","INV-0047","2017-08-31","108300","4","15","","completed","","");





CREATE TABLE IF NOT EXISTS `payment_terms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `defaults` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO payment_terms VALUES("1","Paypal","0");
INSERT INTO payment_terms VALUES("2","Bank","1");





CREATE TABLE IF NOT EXISTS `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO permission_role VALUES("1","1");
INSERT INTO permission_role VALUES("2","1");
INSERT INTO permission_role VALUES("3","1");
INSERT INTO permission_role VALUES("4","1");
INSERT INTO permission_role VALUES("5","1");
INSERT INTO permission_role VALUES("6","1");
INSERT INTO permission_role VALUES("7","1");
INSERT INTO permission_role VALUES("8","1");
INSERT INTO permission_role VALUES("9","1");
INSERT INTO permission_role VALUES("10","1");
INSERT INTO permission_role VALUES("11","1");
INSERT INTO permission_role VALUES("12","1");
INSERT INTO permission_role VALUES("13","1");
INSERT INTO permission_role VALUES("14","1");
INSERT INTO permission_role VALUES("15","1");
INSERT INTO permission_role VALUES("16","1");
INSERT INTO permission_role VALUES("17","1");
INSERT INTO permission_role VALUES("18","1");
INSERT INTO permission_role VALUES("19","1");
INSERT INTO permission_role VALUES("20","1");
INSERT INTO permission_role VALUES("21","1");
INSERT INTO permission_role VALUES("22","1");
INSERT INTO permission_role VALUES("23","1");
INSERT INTO permission_role VALUES("24","1");
INSERT INTO permission_role VALUES("25","1");
INSERT INTO permission_role VALUES("26","1");
INSERT INTO permission_role VALUES("27","1");
INSERT INTO permission_role VALUES("28","1");
INSERT INTO permission_role VALUES("29","1");
INSERT INTO permission_role VALUES("30","1");
INSERT INTO permission_role VALUES("31","1");
INSERT INTO permission_role VALUES("32","1");
INSERT INTO permission_role VALUES("33","1");
INSERT INTO permission_role VALUES("34","1");
INSERT INTO permission_role VALUES("35","1");
INSERT INTO permission_role VALUES("36","1");
INSERT INTO permission_role VALUES("37","1");
INSERT INTO permission_role VALUES("38","1");
INSERT INTO permission_role VALUES("39","1");
INSERT INTO permission_role VALUES("40","1");
INSERT INTO permission_role VALUES("41","1");
INSERT INTO permission_role VALUES("42","1");
INSERT INTO permission_role VALUES("43","1");
INSERT INTO permission_role VALUES("44","1");
INSERT INTO permission_role VALUES("45","1");
INSERT INTO permission_role VALUES("46","1");
INSERT INTO permission_role VALUES("47","1");
INSERT INTO permission_role VALUES("48","1");
INSERT INTO permission_role VALUES("49","1");
INSERT INTO permission_role VALUES("49","2");
INSERT INTO permission_role VALUES("50","1");
INSERT INTO permission_role VALUES("51","1");
INSERT INTO permission_role VALUES("51","2");
INSERT INTO permission_role VALUES("52","1");
INSERT INTO permission_role VALUES("52","2");
INSERT INTO permission_role VALUES("53","1");
INSERT INTO permission_role VALUES("53","2");
INSERT INTO permission_role VALUES("54","1");
INSERT INTO permission_role VALUES("54","2");
INSERT INTO permission_role VALUES("55","1");
INSERT INTO permission_role VALUES("55","2");
INSERT INTO permission_role VALUES("56","1");
INSERT INTO permission_role VALUES("56","2");
INSERT INTO permission_role VALUES("57","1");
INSERT INTO permission_role VALUES("58","1");
INSERT INTO permission_role VALUES("59","1");
INSERT INTO permission_role VALUES("60","1");
INSERT INTO permission_role VALUES("61","1");
INSERT INTO permission_role VALUES("62","1");
INSERT INTO permission_role VALUES("63","1");
INSERT INTO permission_role VALUES("64","1");
INSERT INTO permission_role VALUES("65","1");
INSERT INTO permission_role VALUES("66","1");
INSERT INTO permission_role VALUES("67","1");
INSERT INTO permission_role VALUES("68","1");
INSERT INTO permission_role VALUES("69","1");
INSERT INTO permission_role VALUES("70","1");
INSERT INTO permission_role VALUES("71","1");
INSERT INTO permission_role VALUES("72","1");
INSERT INTO permission_role VALUES("73","1");
INSERT INTO permission_role VALUES("74","1");
INSERT INTO permission_role VALUES("75","1");
INSERT INTO permission_role VALUES("76","1");
INSERT INTO permission_role VALUES("77","1");
INSERT INTO permission_role VALUES("78","1");
INSERT INTO permission_role VALUES("79","1");
INSERT INTO permission_role VALUES("80","1");
INSERT INTO permission_role VALUES("81","1");
INSERT INTO permission_role VALUES("82","1");
INSERT INTO permission_role VALUES("83","1");
INSERT INTO permission_role VALUES("84","1");
INSERT INTO permission_role VALUES("85","1");
INSERT INTO permission_role VALUES("86","1");
INSERT INTO permission_role VALUES("87","1");
INSERT INTO permission_role VALUES("88","1");
INSERT INTO permission_role VALUES("89","1");
INSERT INTO permission_role VALUES("90","1");
INSERT INTO permission_role VALUES("91","1");
INSERT INTO permission_role VALUES("92","1");
INSERT INTO permission_role VALUES("93","1");
INSERT INTO permission_role VALUES("94","1");
INSERT INTO permission_role VALUES("95","1");
INSERT INTO permission_role VALUES("96","1");
INSERT INTO permission_role VALUES("97","1");
INSERT INTO permission_role VALUES("98","1");
INSERT INTO permission_role VALUES("99","1");
INSERT INTO permission_role VALUES("100","1");
INSERT INTO permission_role VALUES("101","1");
INSERT INTO permission_role VALUES("102","1");
INSERT INTO permission_role VALUES("103","1");
INSERT INTO permission_role VALUES("104","1");
INSERT INTO permission_role VALUES("105","1");
INSERT INTO permission_role VALUES("106","1");
INSERT INTO permission_role VALUES("107","1");
INSERT INTO permission_role VALUES("108","1");
INSERT INTO permission_role VALUES("109","1");
INSERT INTO permission_role VALUES("110","1");
INSERT INTO permission_role VALUES("111","1");
INSERT INTO permission_role VALUES("112","1");
INSERT INTO permission_role VALUES("113","1");





CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=114 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO permissions VALUES("1","manage_relationship","Manage Relationship","Manage Relationship","","");
INSERT INTO permissions VALUES("2","manage_customer","Manage Customers","Manage Customers","","");
INSERT INTO permissions VALUES("3","add_customer","Add Customer","Add Customer","","");
INSERT INTO permissions VALUES("4","edit_customer","Edit Customer","Edit Customer","","");
INSERT INTO permissions VALUES("5","delete_customer","Delete Customer","Delete Customer","","");
INSERT INTO permissions VALUES("6","manage_supplier","Manage Suppliers","Manage Suppliers","","");
INSERT INTO permissions VALUES("7","add_supplier","Add Supplier","Add Supplier","","");
INSERT INTO permissions VALUES("8","edit_supplier","Edit Supplier","Edit Supplier","","");
INSERT INTO permissions VALUES("9","delete_supplier","Delete Supplier","Delete Supplier","","");
INSERT INTO permissions VALUES("10","manage_item","Manage Items","Manage Items","","");
INSERT INTO permissions VALUES("11","add_item","Add Item","Add Item","","");
INSERT INTO permissions VALUES("12","edit_item","Edit Item","Edit Item","","");
INSERT INTO permissions VALUES("13","delete_item","Delete Item","Delete Item","","");
INSERT INTO permissions VALUES("14","manage_sale","Manage Sales","Manage Sales","","");
INSERT INTO permissions VALUES("15","manage_quotation","Manage Quotations","Manage Quotations","","");
INSERT INTO permissions VALUES("16","add_quotation","Add Quotation","Add Quotation","","");
INSERT INTO permissions VALUES("17","edit_quotation","Edit Quotation","Edit Quotation","","");
INSERT INTO permissions VALUES("18","delete_quotation","Delete Quotation","Delete Quotation","","");
INSERT INTO permissions VALUES("19","manage_invoice","Manage Invoices","Manage Invoices","","");
INSERT INTO permissions VALUES("20","add_invoice","Add Invoice","Add Invoice","","");
INSERT INTO permissions VALUES("21","edit_invoice","Edit Invoice","Edit Invoice","","");
INSERT INTO permissions VALUES("22","delete_invoice","Delete Invoice","Delete Invoice","","");
INSERT INTO permissions VALUES("23","manage_payment","Manage Payment","Manage Payment","","");
INSERT INTO permissions VALUES("24","add_payment","Add Payment","Add Payment","","");
INSERT INTO permissions VALUES("25","edit_payment","Edit Payment","Edit Payment","","");
INSERT INTO permissions VALUES("26","delete_payment","Delete Payment","Delete Payment","","");
INSERT INTO permissions VALUES("27","manage_purchase","Manage Purchase","Manage Purchase","","");
INSERT INTO permissions VALUES("28","add_purchase","Add Purchase","Add Purchase","","");
INSERT INTO permissions VALUES("29","edit_purchase","Edit Purchase","Edit Purchase","","");
INSERT INTO permissions VALUES("30","delete_purchase","Delete Purchase","Delete Purchase","","");
INSERT INTO permissions VALUES("31","manage_banking_transaction","Manage Banking & Transactions","Manage Banking & Transactions","","");
INSERT INTO permissions VALUES("32","manage_bank_account","Manage Bank Accounts","Manage Bank Accounts","","");
INSERT INTO permissions VALUES("33","add_bank_account","Add Bank Account","Add Bank Account","","");
INSERT INTO permissions VALUES("34","edit_bank_account","Edit Bank Account","Edit Bank Account","","");
INSERT INTO permissions VALUES("35","delete_bank_account","Delete Bank Account","Delete Bank Account","","");
INSERT INTO permissions VALUES("36","manage_deposit","Manage Deposit","Manage Deposit","","");
INSERT INTO permissions VALUES("37","add_deposit","Add Deposit","Add Deposit","","");
INSERT INTO permissions VALUES("38","edit_deposit","Edit Deposit","Edit Deposit","","");
INSERT INTO permissions VALUES("39","delete_deposit","Delete Deposit","Delete Deposit","","");
INSERT INTO permissions VALUES("40","manage_balance_transfer","Manage Balance Transfer","Manage Balance Transfer","","");
INSERT INTO permissions VALUES("41","add_balance_transfer","Add Balance Transfer","Add Balance Transfer","","");
INSERT INTO permissions VALUES("42","edit_balance_transfer","Edit Balance Transfer","Edit Balance Transfer","","");
INSERT INTO permissions VALUES("43","delete_balance_transfer","Delete Balance Transfer","Delete Balance Transfer","","");
INSERT INTO permissions VALUES("44","manage_transaction","Manage Transactions","Manage Transactions","","");
INSERT INTO permissions VALUES("45","manage_expense","Manage Expense","Manage Expense","","");
INSERT INTO permissions VALUES("46","add_expense","Add Expense","Add Expense","","");
INSERT INTO permissions VALUES("47","edit_expense","Edit Expense","Edit Expense","","");
INSERT INTO permissions VALUES("48","delete_expense","Delete Expense","Delete Expense","","");
INSERT INTO permissions VALUES("49","manage_report","Manage Report","Manage Report","","");
INSERT INTO permissions VALUES("50","manage_stock_on_hand","Manage Inventory Stock On Hand","Manage Inventory Stock On Hand","","");
INSERT INTO permissions VALUES("51","manage_sale_report","Manage Sales Report","Manage Sales Report","","");
INSERT INTO permissions VALUES("52","manage_sale_history_report","Manage Sales History Report","Manage Sales History Report","","");
INSERT INTO permissions VALUES("53","manage_purchase_report","Manage Purchase Report","Manage Purchase Report","","");
INSERT INTO permissions VALUES("54","manage_team_report","Manage Team Member Report","Manage Team Member Report","","");
INSERT INTO permissions VALUES("55","manage_expense_report","Manage Expense Report","Manage Expense Report","","");
INSERT INTO permissions VALUES("56","manage_income_report","Manage Income Report","Manage Income Report","","");
INSERT INTO permissions VALUES("57","manage_income_vs_expense","Manage Income vs Expense","Manage Income vs Expense","","");
INSERT INTO permissions VALUES("58","manage_setting","Manage Settings","Manage Settings","","");
INSERT INTO permissions VALUES("59","manage_company_setting","Manage Company Setting","Manage Company Setting","","");
INSERT INTO permissions VALUES("60","manage_team_member","Manage Team Member","Manage Team Member","","");
INSERT INTO permissions VALUES("61","add_team_member","Add Team Member","Add Team Member","","");
INSERT INTO permissions VALUES("62","edit_team_member","Edit Team Member","Edit Team Member","","");
INSERT INTO permissions VALUES("63","delete_team_member","Delete Team Member","Delete Team Member","","");
INSERT INTO permissions VALUES("64","manage_role","Manage Roles","Manage Roles","","");
INSERT INTO permissions VALUES("65","add_role","Add Role","Add Role","","");
INSERT INTO permissions VALUES("66","edit_role","Edit Role","Edit Role","","");
INSERT INTO permissions VALUES("67","delete_role","Delete Role","Delete Role","","");
INSERT INTO permissions VALUES("68","manage_location","Manage Location","Manage Location","","");
INSERT INTO permissions VALUES("69","add_location","Add Location","Add Location","","");
INSERT INTO permissions VALUES("70","edit_location","Edit Location","Edit Location","","");
INSERT INTO permissions VALUES("71","delete_location","Delete Location","Delete Location","","");
INSERT INTO permissions VALUES("72","manage_general_setting","Manage General Settings","Manage General Settings","","");
INSERT INTO permissions VALUES("73","manage_item_category","Manage Item Category","Manage Item Category","","");
INSERT INTO permissions VALUES("74","add_item_category","Add Item Category","Add Item Category","","");
INSERT INTO permissions VALUES("75","edit_item_category","Edit Item Category","Edit Item Category","","");
INSERT INTO permissions VALUES("76","delete_item_category","Delete Item Category","Delete Item Category","","");
INSERT INTO permissions VALUES("77","manage_income_expense_category","Manage Income Expense Category","Manage Income Expense Category","","");
INSERT INTO permissions VALUES("78","add_income_expense_category","Add Income Expense Category","Add Income Expense Category","","");
INSERT INTO permissions VALUES("79","edit_income_expense_category","Edit Income Expense Category","Edit Income Expense Category","","");
INSERT INTO permissions VALUES("80","delete_income_expense_category","Delete Income Expense Category","Delete Income Expense Category","","");
INSERT INTO permissions VALUES("81","manage_unit","Manage Unit","Manage Unit","","");
INSERT INTO permissions VALUES("82","add_unit","Add Unit","Add Unit","","");
INSERT INTO permissions VALUES("83","edit_unit","Edit Unit","Edit Unit","","");
INSERT INTO permissions VALUES("84","delete_unit","Delete Unit","Delete Unit","","");
INSERT INTO permissions VALUES("85","manage_db_backup","Manage Database Backup","Manage Database Backup","","");
INSERT INTO permissions VALUES("86","add_db_backup","Add Database Backup","Add Database Backup","","");
INSERT INTO permissions VALUES("87","delete_db_backup","Delete Database Backup","Delete Database Backup","","");
INSERT INTO permissions VALUES("88","manage_email_setup","Manage Email Setup","Manage Email Setup","","");
INSERT INTO permissions VALUES("89","manage_finance","Manage Finance","Manage Finance","","");
INSERT INTO permissions VALUES("90","manage_tax","Manage Taxs","Manage Taxs","","");
INSERT INTO permissions VALUES("91","add_tax","Add Tax","Add Tax","","");
INSERT INTO permissions VALUES("92","edit_tax","Edit Tax","Edit Tax","","");
INSERT INTO permissions VALUES("93","delete_tax","Delete Tax","Delete Tax","","");
INSERT INTO permissions VALUES("94","manage_currency","Manage Currency","Manage Currency","","");
INSERT INTO permissions VALUES("95","add_currency","Add Currency","Add Currency","","");
INSERT INTO permissions VALUES("96","edit_currency","Edit Currency","Edit Currency","","");
INSERT INTO permissions VALUES("97","delete_currency","Delete Currency","Delete Currency","","");
INSERT INTO permissions VALUES("98","manage_payment_term","Manage Payment Term","Manage Payment Term","","");
INSERT INTO permissions VALUES("99","add_payment_term","Add Payment Term","Add Payment Term","","");
INSERT INTO permissions VALUES("100","edit_payment_term","Edit Payment Term","Edit Payment Term","","");
INSERT INTO permissions VALUES("101","delete_payment_term","Delete Payment Term","Delete Payment Term","","");
INSERT INTO permissions VALUES("102","manage_payment_method","Manage Payment Method","Manage Payment Method","","");
INSERT INTO permissions VALUES("103","add_payment_method","Add Payment Method","Add Payment Method","","");
INSERT INTO permissions VALUES("104","edit_payment_method","Edit Payment Method","Edit Payment Method","","");
INSERT INTO permissions VALUES("105","delete_payment_method","Delete Payment Method","Delete Payment Method","","");
INSERT INTO permissions VALUES("106","manage_payment_gateway","Manage Payment Method","Manage Payment Gateway","","");
INSERT INTO permissions VALUES("107","manage_email_template","Manage Email Template","Manage Email Template","","");
INSERT INTO permissions VALUES("108","manage_quotation_email_template","Manage Quotation Template","Manage Quotation Email Template","","");
INSERT INTO permissions VALUES("109","manage_invoice_email_template","Manage Invoice Email Template","Manage Invoice Email Template","","");
INSERT INTO permissions VALUES("110","manage_payment_email_template","Manage Payment Email Template","Manage Payment Email Template","","");
INSERT INTO permissions VALUES("111","manage_preference","Manage Preference","Manage Preference","","");
INSERT INTO permissions VALUES("112","manage_barcode","Manage barcode/label","Manage barcode/label","","");
INSERT INTO permissions VALUES("113","download_db_backup","Download Database Backup","Download Database Backup","","");





CREATE TABLE IF NOT EXISTS `preference` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `field` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO preference VALUES("1","preference","row_per_page","25");
INSERT INTO preference VALUES("2","preference","date_format","2");
INSERT INTO preference VALUES("3","preference","date_sepa","/");
INSERT INTO preference VALUES("4","preference","soft_name","goBilling");
INSERT INTO preference VALUES("5","company","site_short_name","BV");
INSERT INTO preference VALUES("6","preference","percentage","0");
INSERT INTO preference VALUES("7","preference","quantity","0");
INSERT INTO preference VALUES("8","preference","date_format_type","mm/dd/yyyy");
INSERT INTO preference VALUES("9","company","company_name","Ben Venture");
INSERT INTO preference VALUES("10","company","company_email","BenVenture@yahoo.com");
INSERT INTO preference VALUES("11","company","company_phone","123465798");
INSERT INTO preference VALUES("12","company","company_street","Lajpath Nagar");
INSERT INTO preference VALUES("13","company","company_city","Delhi");
INSERT INTO preference VALUES("14","company","company_state","Delhi");
INSERT INTO preference VALUES("15","company","company_zipCode","700345");
INSERT INTO preference VALUES("16","company","company_country_id","India");
INSERT INTO preference VALUES("17","company","dflt_lang","en");
INSERT INTO preference VALUES("18","company","dflt_currency_id","3");
INSERT INTO preference VALUES("19","company","sates_type_id","1");
INSERT INTO preference VALUES("20","company","company_gstin","");





CREATE TABLE IF NOT EXISTS `purch_order_details` (
  `po_detail_item` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_no` int(11) NOT NULL,
  `item_code` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `qty_invoiced` double NOT NULL DEFAULT '0',
  `unit_price` double NOT NULL DEFAULT '0',
  `tax_type_id` int(11) NOT NULL,
  `hsn` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `quantity_ordered` double NOT NULL DEFAULT '0',
  `quantity_received` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`po_detail_item`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO purch_order_details VALUES("19","11","N326","N326","20","2000","4","-","20","20");
INSERT INTO purch_order_details VALUES("18","10","22222","TABLET","1","15000","2","7676766166","1","1");
INSERT INTO purch_order_details VALUES("17","10","LENEVO","LED TV","1","10","3","LEDHSN","1","1");
INSERT INTO purch_order_details VALUES("16","9","22222","TABLET","1","15000","5","7676766166","1","1");
INSERT INTO purch_order_details VALUES("12","6","HP","HP Pro Book","100","10","2","hsn_no","100","100");
INSERT INTO purch_order_details VALUES("13","7","1001","Camera","1","100","5","8001","1","1");
INSERT INTO purch_order_details VALUES("14","8","76767","iphone","10","10000","5","78787772","10","10");
INSERT INTO purch_order_details VALUES("15","9","LENEVO","LED TV","1","10","3","LEDHSN","1","1");
INSERT INTO purch_order_details VALUES("20","12","TT1","pen","1","50","2","-","1","1");
INSERT INTO purch_order_details VALUES("21","12","76767","iphone","1","10000","5","78787772","1","1");
INSERT INTO purch_order_details VALUES("22","13","N326","JEANS","200","500","5","-","200","200");
INSERT INTO purch_order_details VALUES("23","14","LENEVO","LENEVO","13","1450","3","LEDHSN","13","13");
INSERT INTO purch_order_details VALUES("24","15","N326","JEANS","10","0","8","-","10","10");
INSERT INTO purch_order_details VALUES("25","15","IT100","Item Name","10","0","2","HSN100","10","10");
INSERT INTO purch_order_details VALUES("26","16","HP","HP Pro Book","10","10","2","hsn_no","10","10");
INSERT INTO purch_order_details VALUES("27","17","LENEVO","LED TV","1","10","3","LEDHSN","1","1");
INSERT INTO purch_order_details VALUES("28","18","KK","DELL Laptop","9","20000","5","15","9","9");





CREATE TABLE IF NOT EXISTS `purch_orders` (
  `order_no` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `supplier_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `comments` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `ord_date` date NOT NULL,
  `reference` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `into_stock_location` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `total` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`order_no`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO purch_orders VALUES("9","2","1","","2017-07-29","PO-0004","PL","17711.5");
INSERT INTO purch_orders VALUES("8","3","1","","2017-07-27","PO-0003","PL","118000");
INSERT INTO purch_orders VALUES("7","1","1","","2017-07-27","PO-0002","PL","118");
INSERT INTO purch_orders VALUES("6","1","1","","2017-07-26","PO-0001","PL","1100");
INSERT INTO purch_orders VALUES("10","1","1","","2017-07-29","PO-0005","PL","16511.5");
INSERT INTO purch_orders VALUES("11","4","1","","2017-08-02","PO-0006","JA","42000");
INSERT INTO purch_orders VALUES("12","1","1","test note","2017-08-03","PO-0007","PL","11855");
INSERT INTO purch_orders VALUES("13","3","1","","2017-08-18","PO-0008","PL","118000");
INSERT INTO purch_orders VALUES("14","4","1","","2017-08-24","PO-0009","JA","21677.5");
INSERT INTO purch_orders VALUES("15","1","1","AAAAA","2017-08-27","PO-0010","PL","0");
INSERT INTO purch_orders VALUES("16","4","1","","2017-08-27","PO-0011","PL","110");
INSERT INTO purch_orders VALUES("17","4","1","","2017-08-31","PO-0012","JA","11.5");
INSERT INTO purch_orders VALUES("18","6","4","","2017-08-31","PO-0013","PH","212400");





CREATE TABLE IF NOT EXISTS `purchase_prices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `stock_id` char(30) COLLATE utf8_unicode_ci NOT NULL,
  `price` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO purchase_prices VALUES("1","TESTITEM","5");
INSERT INTO purchase_prices VALUES("2","TESTITEM2","0");
INSERT INTO purchase_prices VALUES("3","TESTITEM3","10");
INSERT INTO purchase_prices VALUES("4","HP","10");
INSERT INTO purchase_prices VALUES("5","LENEVO","10");
INSERT INTO purchase_prices VALUES("6","1001","100");
INSERT INTO purchase_prices VALUES("7","76767","10000");
INSERT INTO purchase_prices VALUES("8","22222","15000");
INSERT INTO purchase_prices VALUES("9","TT1","50");
INSERT INTO purchase_prices VALUES("10","N326","0");
INSERT INTO purchase_prices VALUES("11","910001","0");
INSERT INTO purchase_prices VALUES("12","RP01","1410");
INSERT INTO purchase_prices VALUES("13","DF","0");
INSERT INTO purchase_prices VALUES("14","DFGDFG","0");
INSERT INTO purchase_prices VALUES("15","HYDF","0");
INSERT INTO purchase_prices VALUES("16","IT100","0");
INSERT INTO purchase_prices VALUES("17","FVFVE","3");
INSERT INTO purchase_prices VALUES("18","RS123","7000");
INSERT INTO purchase_prices VALUES("19","10","0");
INSERT INTO purchase_prices VALUES("20","222","0");
INSERT INTO purchase_prices VALUES("21","06","0");
INSERT INTO purchase_prices VALUES("22","8","8");
INSERT INTO purchase_prices VALUES("23","KK","20000");
INSERT INTO purchase_prices VALUES("24","123","0");





CREATE TABLE IF NOT EXISTS `role_user` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO role_user VALUES("1","1");
INSERT INTO role_user VALUES("2","1");
INSERT INTO role_user VALUES("3","2");
INSERT INTO role_user VALUES("4","1");
INSERT INTO role_user VALUES("5","2");





CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO roles VALUES("1","admin","Admin","Admin User","","");
INSERT INTO roles VALUES("2","Guest","Guest","Guest User","","");





CREATE TABLE IF NOT EXISTS `sale_prices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `stock_id` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `sales_type_id` tinyint(4) NOT NULL,
  `curr_abrev` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `price` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO sale_prices VALUES("1","TESTITEM","1","USD","10");
INSERT INTO sale_prices VALUES("2","TESTITEM","2","USD","0");
INSERT INTO sale_prices VALUES("3","TESTITEM2","1","USD","0");
INSERT INTO sale_prices VALUES("4","TESTITEM2","2","USD","0");
INSERT INTO sale_prices VALUES("5","TESTITEM3","1","USD","15");
INSERT INTO sale_prices VALUES("6","TESTITEM3","2","USD","0");
INSERT INTO sale_prices VALUES("7","HP","1","USD","20");
INSERT INTO sale_prices VALUES("8","HP","2","USD","0");
INSERT INTO sale_prices VALUES("9","LENEVO","1","USD","15");
INSERT INTO sale_prices VALUES("10","LENEVO","2","USD","0");
INSERT INTO sale_prices VALUES("11","1001","1","USD","120");
INSERT INTO sale_prices VALUES("12","1001","2","USD","0");
INSERT INTO sale_prices VALUES("13","76767","1","USD","12000");
INSERT INTO sale_prices VALUES("14","76767","2","USD","0");
INSERT INTO sale_prices VALUES("15","22222","1","USD","20000");
INSERT INTO sale_prices VALUES("16","22222","2","USD","0");
INSERT INTO sale_prices VALUES("17","TT1","1","USD","100");
INSERT INTO sale_prices VALUES("18","TT1","2","USD","0");
INSERT INTO sale_prices VALUES("19","N326","1","USD","0");
INSERT INTO sale_prices VALUES("20","N326","2","USD","0");
INSERT INTO sale_prices VALUES("21","910001","1","USD","0");
INSERT INTO sale_prices VALUES("22","910001","2","USD","0");
INSERT INTO sale_prices VALUES("23","RP01","1","USD","1804.8");
INSERT INTO sale_prices VALUES("24","RP01","2","USD","0");
INSERT INTO sale_prices VALUES("25","DF","1","USD","0");
INSERT INTO sale_prices VALUES("26","DF","2","USD","0");
INSERT INTO sale_prices VALUES("27","DFGDFG","1","USD","0");
INSERT INTO sale_prices VALUES("28","DFGDFG","2","USD","0");
INSERT INTO sale_prices VALUES("29","HYDF","1","USD","0");
INSERT INTO sale_prices VALUES("30","HYDF","2","USD","0");
INSERT INTO sale_prices VALUES("31","IT100","1","USD","0");
INSERT INTO sale_prices VALUES("32","IT100","2","USD","0");
INSERT INTO sale_prices VALUES("33","FVFVE","1","USD","10");
INSERT INTO sale_prices VALUES("34","FVFVE","2","USD","0");
INSERT INTO sale_prices VALUES("35","RS123","1","USD","10000");
INSERT INTO sale_prices VALUES("36","RS123","2","USD","0");
INSERT INTO sale_prices VALUES("37","10","1","USD","0");
INSERT INTO sale_prices VALUES("38","10","2","USD","0");
INSERT INTO sale_prices VALUES("39","222","1","USD","0");
INSERT INTO sale_prices VALUES("40","222","2","USD","0");
INSERT INTO sale_prices VALUES("41","06","1","USD","0");
INSERT INTO sale_prices VALUES("42","06","2","USD","0");
INSERT INTO sale_prices VALUES("43","8","1","USD","10");
INSERT INTO sale_prices VALUES("44","8","2","USD","0");
INSERT INTO sale_prices VALUES("45","KK","1","USD","45000");
INSERT INTO sale_prices VALUES("46","KK","2","USD","0");
INSERT INTO sale_prices VALUES("47","123","1","USD","10");
INSERT INTO sale_prices VALUES("48","123","2","USD","0");





CREATE TABLE IF NOT EXISTS `sales_order_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_no` int(11) NOT NULL,
  `trans_type` mediumint(9) NOT NULL,
  `stock_id` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `tax_type_id` tinyint(4) NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `unit_price` double NOT NULL DEFAULT '0',
  `qty_sent` double NOT NULL DEFAULT '0',
  `quantity` double NOT NULL DEFAULT '0',
  `is_inventory` double NOT NULL,
  `hsn` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `discount_percent` double NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=208 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO sales_order_details VALUES("1","1","201","TESTITEM","2","Test Item","10","0","2","1","","0","","");
INSERT INTO sales_order_details VALUES("3","3","201","TESTITEM","2","Test Item","10","0","2","1","","0","","");
INSERT INTO sales_order_details VALUES("134","24","201","22222","5","TABLET","20000","1","1","1","7676766166","0","","");
INSERT INTO sales_order_details VALUES("5","5","201","TESTITEM","3","Test Item","10","0","2","1","","0","","");
INSERT INTO sales_order_details VALUES("53","24","201","LENEVO","3","LED TV","15","0","20","1","LEDHSN","0","","");
INSERT INTO sales_order_details VALUES("7","7","201","TESTITEM","2","Test Item","10","0","2","1","","0","","");
INSERT INTO sales_order_details VALUES("52","24","201","HP","2","HP Pro Book","20","0","1","1","hsn_no","0","","");
INSERT INTO sales_order_details VALUES("9","9","201","TESTITEM","3","Test Item","10","0","1","1","","0","","");
INSERT INTO sales_order_details VALUES("64","32","201","LENEVO","6","LED TV","15","0","188","1","LEDHSN","10","","");
INSERT INTO sales_order_details VALUES("63","31","202","1001","5","Camera","120","0","10","1","8001","0","","");
INSERT INTO sales_order_details VALUES("55","25","202","HP","2","HP Pro Book","20","0","1","1","hsn_no","0","","");
INSERT INTO sales_order_details VALUES("56","25","202","LENEVO","3","LED TV","15","0","1","1","LEDHSN","0","","");
INSERT INTO sales_order_details VALUES("57","25","202","","4","Custom Item","10","0","1","0","CustomHSN","0","","");
INSERT INTO sales_order_details VALUES("58","26","201","HP","2","HP Pro Book","20","0","10","1","hsn_no","0","","");
INSERT INTO sales_order_details VALUES("59","27","202","HP","2","HP Pro Book","20","0","10","1","hsn_no","0","","");
INSERT INTO sales_order_details VALUES("60","28","201","1001","5","Camera","120","0","1","1","8001","0","","");
INSERT INTO sales_order_details VALUES("61","29","202","1001","5","Camera","120","0","1","1","8001","0","","");
INSERT INTO sales_order_details VALUES("62","30","201","1001","5","Camera","120","0","10","1","8001","0","","");
INSERT INTO sales_order_details VALUES("35","20","201","TESTITEM2","3","Test Item 2","0","0","1","1","-","0","","");
INSERT INTO sales_order_details VALUES("37","20","201","TESTITEM","2","Test Item","10","0","1","1","-","0","","");
INSERT INTO sales_order_details VALUES("68","36","201","76767","5","iphone","12000","0","1","1","78787772","10","","");
INSERT INTO sales_order_details VALUES("39","20","201","TESTITEM3","3","Test Item 3","15","0","1","1","testhsn","0","","");
INSERT INTO sales_order_details VALUES("67","35","202","LENEVO","5","LED TV","15","0","111","1","LEDHSN","0","","");
INSERT INTO sales_order_details VALUES("41","20","201","","2","Custom Invoice Item","20","0","1","0","","0","","");
INSERT INTO sales_order_details VALUES("65","33","202","LENEVO","5","LED TV","15","0","188","1","LEDHSN","10","","");
INSERT INTO sales_order_details VALUES("66","34","201","LENEVO","5","LED TV","15","0","111","1","LEDHSN","0","","");
INSERT INTO sales_order_details VALUES("69","37","202","76767","5","iphone","12000","0","1","1","78787772","10","","");
INSERT INTO sales_order_details VALUES("70","38","201","76767","5","iphone","12000","0","10","1","78787772","2","","");
INSERT INTO sales_order_details VALUES("71","39","202","76767","4","iphone","12000","0","112","1","78787772","2","","");
INSERT INTO sales_order_details VALUES("72","35","201","76767","5","iphone","12000","0","1","1","78787772","0","","");
INSERT INTO sales_order_details VALUES("73","40","201","22222","2","TABLET","20000","0","1","1","7676766166","0","","");
INSERT INTO sales_order_details VALUES("74","41","202","22222","2","TABLET","20000","0","1","1","7676766166","0","","");
INSERT INTO sales_order_details VALUES("75","42","201","22222","5","TABLET","20000","0","1","1","7676766166","0","","");
INSERT INTO sales_order_details VALUES("76","43","202","22222","5","TABLET","20000","0","1","1","7676766166","0","","");
INSERT INTO sales_order_details VALUES("77","44","201","22222","6","TABLET","20000","0","1","1","7676766166","0","","");
INSERT INTO sales_order_details VALUES("78","45","202","22222","5","TABLET","20000","0","1","1","7676766166","0","","");
INSERT INTO sales_order_details VALUES("79","46","201","TT1","2","pen","100","0","1","1","-","0","","");
INSERT INTO sales_order_details VALUES("80","46","201","22222","5","TABLET","20000","0","1","1","7676766166","0","","");
INSERT INTO sales_order_details VALUES("84","49","201","TT1","2","pen","100","0","1","1","-","0","","");
INSERT INTO sales_order_details VALUES("83","48","201","TT1","2","pen","100","0","1","1","-","0","","");
INSERT INTO sales_order_details VALUES("85","50","202","TT1","2","pen","100","0","1","1","-","0","","");
INSERT INTO sales_order_details VALUES("86","51","201","","6","Co","10000","0","1","0","4757205 ","0","","");
INSERT INTO sales_order_details VALUES("87","52","202","","6","Co","10000","0","1","0","4757205 ","0","","");
INSERT INTO sales_order_details VALUES("88","51","201","","6","Mo","500","0","1","0","","0","","");
INSERT INTO sales_order_details VALUES("89","52","202","","6","Mo","500","0","1","0","","0","","");
INSERT INTO sales_order_details VALUES("90","53","201","","3","Comp","40000","0","1","0","","0","","");
INSERT INTO sales_order_details VALUES("91","54","202","","3","Comp","40000","0","1","0","-","0","","");
INSERT INTO sales_order_details VALUES("92","54","201","LENEVO","5","LED TV","15","0","1","1","LEDHSN","0","","");
INSERT INTO sales_order_details VALUES("93","55","201","910001","5","DFAH SUPPORT SERVICE","50000","0","1","1","99922","0","","");
INSERT INTO sales_order_details VALUES("94","56","201","910001","5","DFAH SUPPORT SERVICE","48000","0","1","1","99922","0","","");
INSERT INTO sales_order_details VALUES("95","57","202","910001","5","DFAH SUPPORT SERVICE","48000","0","1","1","99922","0","","");
INSERT INTO sales_order_details VALUES("96","58","201","910001","7","DFAH SUPPORT SERVICE","10000","0","1","1","99922","0","","");
INSERT INTO sales_order_details VALUES("97","59","202","910001","7","DFAH SUPPORT SERVICE","10000","0","1","1","99922","0","","");
INSERT INTO sales_order_details VALUES("98","59","201","LENEVO","3","LED TV","15","0","1","1","LEDHSN","0","","");
INSERT INTO sales_order_details VALUES("99","60","201","22222","5","TABLET","20000","0","1","1","7676766166","0","","");
INSERT INTO sales_order_details VALUES("100","61","201","22222","5","TABLET","20000","0","1","1","7676766166","0","","");
INSERT INTO sales_order_details VALUES("101","62","202","22222","5","TABLET","20000","0","1","1","7676766166","0","","");
INSERT INTO sales_order_details VALUES("102","61","201","N326","4","JEANS","0","0","5","1","-","0","","");
INSERT INTO sales_order_details VALUES("103","62","202","N326","4","JEANS","0","0","5","1","-","0","","");
INSERT INTO sales_order_details VALUES("104","63","201","","5","test","2000","0","1","0","54654","0","","");
INSERT INTO sales_order_details VALUES("105","64","202","","5","test","2000","0","1","0","54654","0","","");
INSERT INTO sales_order_details VALUES("106","65","201","910001","5","DFAH SUPPORT SERVICE","5000","0","1","1","99922","0","","");
INSERT INTO sales_order_details VALUES("107","66","202","910001","5","DFAH SUPPORT SERVICE","5000","0","1","1","99922","0","","");
INSERT INTO sales_order_details VALUES("108","65","201","N326","4","JEANS","999","0","1","1","-","0","","");
INSERT INTO sales_order_details VALUES("109","66","202","N326","4","JEANS","999","0","1","1","-","0","","");
INSERT INTO sales_order_details VALUES("110","67","201","LENEVO","6","LED TV","15","0","1","1","LEDHSN","0","","");
INSERT INTO sales_order_details VALUES("111","68","202","LENEVO","6","LED TV","15","0","1","1","LEDHSN","0","","");
INSERT INTO sales_order_details VALUES("112","69","201","","5","TProd1","200","0","1","0","112233","0","","");
INSERT INTO sales_order_details VALUES("113","70","202","","5","TProd1","200","0","1","0","112233","0","","");
INSERT INTO sales_order_details VALUES("114","71","201","","5","CSub","500","0","1","0","1111","0","","");
INSERT INTO sales_order_details VALUES("115","72","202","","5","CSub","500","0","1","0","1111","0","","");
INSERT INTO sales_order_details VALUES("116","73","201","","7","test11","110","0","1","0","","0","","");
INSERT INTO sales_order_details VALUES("117","74","202","","7","test11","110","0","1","0","","0","","");
INSERT INTO sales_order_details VALUES("118","35","201","910001","5","DFAH SUPPORT SERVICE","100","0","1","1","99922","0","","");
INSERT INTO sales_order_details VALUES("119","35","201","HP","2","HP Pro Book","20","0","1","1","hsn_no","0","","");
INSERT INTO sales_order_details VALUES("120","75","201","N326","4","JEANS","0","0","1","1","-","0","","");
INSERT INTO sales_order_details VALUES("121","76","202","N326","4","JEANS","0","0","1","1","-","0","","");
INSERT INTO sales_order_details VALUES("122","75","201","HP","2","HP Pro Book","20","0","1","1","hsn_no","0","","");
INSERT INTO sales_order_details VALUES("123","76","202","HP","2","HP Pro Book","20","0","1","1","hsn_no","0","","");
INSERT INTO sales_order_details VALUES("124","77","201","N326","5","JEANS","200","0","1","1","-","0","","");
INSERT INTO sales_order_details VALUES("125","78","202","N326","5","JEANS","200","0","1","1","-","0","","");
INSERT INTO sales_order_details VALUES("126","79","201","N326","1","JEANS","12","0","1","1","-","10","","");
INSERT INTO sales_order_details VALUES("127","80","202","N326","5","JEANS","10","0","1","1","-","1","","");
INSERT INTO sales_order_details VALUES("128","81","201","DFGDFG","6","Sarojni","2000","0","1","1","fgdfgdfg","0","","");
INSERT INTO sales_order_details VALUES("129","81","201","N326","8","JEANS","1000","0","10","1","-","0","","");
INSERT INTO sales_order_details VALUES("130","82","202","N326","8","JEANS","1000","0","10","1","-","0","","");
INSERT INTO sales_order_details VALUES("131","82","202","DFGDFG","6","Sarojni","2000","0","1","1","fgdfgdfg","0","","");
INSERT INTO sales_order_details VALUES("132","83","201","","5","sdfds","220","0","1","0","dsfds","0","","");
INSERT INTO sales_order_details VALUES("133","84","202","","6","sdfds","220","0","1","0","dsfds","0","","");
INSERT INTO sales_order_details VALUES("135","85","201","FVFVE","5","TSHIRT","10","0","1","1","fvve","0","","");
INSERT INTO sales_order_details VALUES("136","86","201","FVFVE","5","TSHIRT","10","0","1","1","fvve","0","","");
INSERT INTO sales_order_details VALUES("137","87","202","FVFVE","5","TSHIRT","15","0","1","1","fvve","0","","");
INSERT INTO sales_order_details VALUES("138","88","201","DFGDFG","6","Sarojni","5000","0","1","1","fgdfgdfg","0","","");
INSERT INTO sales_order_details VALUES("139","89","202","DFGDFG","5","Sarojni","5000","0","1","1","fgdfgdfg","0","","");
INSERT INTO sales_order_details VALUES("140","88","201","76767","7","iphone","2000","0","1","1","78787772","10","","");
INSERT INTO sales_order_details VALUES("141","89","202","76767","5","iphone","2000","0","1","1","78787772","10","","");
INSERT INTO sales_order_details VALUES("142","88","201","HP","5","HP Pro Book","2000","0","1","1","hsn_no","0","","");
INSERT INTO sales_order_details VALUES("143","89","202","HP","5","HP Pro Book","2000","0","1","1","hsn_no","0","","");
INSERT INTO sales_order_details VALUES("144","90","201","","5","table","120000","0","1","0","","0","","");
INSERT INTO sales_order_details VALUES("145","91","202","","5","table","120000","0","1","0","","0","","");
INSERT INTO sales_order_details VALUES("146","92","201","76767","5","iphone","12000","0","1","1","78787772","0","","");
INSERT INTO sales_order_details VALUES("147","93","202","76767","5","iphone","12000","0","1","1","78787772","0","","");
INSERT INTO sales_order_details VALUES("148","92","201","HP","2","HP Pro Book","20","0","1","1","hsn_no","0","","");
INSERT INTO sales_order_details VALUES("149","93","202","HP","2","HP Pro Book","20","0","1","1","hsn_no","0","","");
INSERT INTO sales_order_details VALUES("150","94","201","76767","5","iphone","12000","0","1","1","78787772","0","","");
INSERT INTO sales_order_details VALUES("151","95","202","76767","5","iphone","12000","0","1","1","78787772","0","","");
INSERT INTO sales_order_details VALUES("152","96","201","76767","5","iphone","12000","0","1","1","78787772","0","","");
INSERT INTO sales_order_details VALUES("153","97","202","76767","7","iphone","12000","0","1","1","78787772","0","","");
INSERT INTO sales_order_details VALUES("154","98","201","76767","5","iphone","12000","0","1","1","78787772","0","","");
INSERT INTO sales_order_details VALUES("155","99","202","76767","5","iphone","12000","0","1","1","78787772","0","","");
INSERT INTO sales_order_details VALUES("156","100","201","HP","2","HP Pro Book","20","0","10","1","hsn_no","0","","");
INSERT INTO sales_order_details VALUES("157","101","202","HP","2","HP Pro Book","20","0","10","1","hsn_no","0","","");
INSERT INTO sales_order_details VALUES("158","100","201","22222","5","TABLET","20000","0","10","1","7676766166","0","","");
INSERT INTO sales_order_details VALUES("159","101","202","22222","5","TABLET","20000","0","10","1","7676766166","0","","");
INSERT INTO sales_order_details VALUES("160","102","201","HP","5","HP Pro Book","20","0","1","1","hsn_no","0","","");
INSERT INTO sales_order_details VALUES("161","103","202","HP","5","HP Pro Book","20","0","1","1","hsn_no","0","","");
INSERT INTO sales_order_details VALUES("162","104","201","HP","5","HP Pro Book","20","0","1","1","hsn_no","0","","");
INSERT INTO sales_order_details VALUES("163","105","202","HP","5","HP Pro Book","20","0","1","1","hsn_no","0","","");
INSERT INTO sales_order_details VALUES("164","106","201","","1","test","1200","0","1","0","","0","","");
INSERT INTO sales_order_details VALUES("165","107","202","","1","test","1200","0","1","0","","0","","");
INSERT INTO sales_order_details VALUES("166","106","201","","1","","0","0","1","0","","0","","");
INSERT INTO sales_order_details VALUES("167","107","202","","1","","0","0","1","0","","0","","");
INSERT INTO sales_order_details VALUES("168","106","201","","1","","0","0","1","0","","0","","");
INSERT INTO sales_order_details VALUES("169","107","202","","1","","0","0","1","0","","0","","");
INSERT INTO sales_order_details VALUES("170","107","201","N326","6","JEANS","200","0","1","1","-","0","","");
INSERT INTO sales_order_details VALUES("171","107","202","","7"," ","200","0","1","0","","0","","");
INSERT INTO sales_order_details VALUES("172","108","201","","2","software","1000","0","1","0","23432343","10","","");
INSERT INTO sales_order_details VALUES("173","109","202","","2","software","1000","0","1","0","23432343","10","","");
INSERT INTO sales_order_details VALUES("174","110","201","910001","2","DFAH SUPPORT SERVICE","0","0","1","1","99922","0","","");
INSERT INTO sales_order_details VALUES("175","111","202","910001","6","DFAH SUPPORT SERVICE","5000","0","1","1","99922","0","","");
INSERT INTO sales_order_details VALUES("176","112","201","","2","aa","100","0","1","0","aaa","0","","");
INSERT INTO sales_order_details VALUES("178","112","201","","1","","0","0","1","0","","0","","");
INSERT INTO sales_order_details VALUES("192","119","201","DFGDFG","3","Sarojni","500","0","10","1","fgdfgdfg","0","","");
INSERT INTO sales_order_details VALUES("180","113","201","910001","5","DFAH SUPPORT SERVICE","0","0","1","1","99922","0","","");
INSERT INTO sales_order_details VALUES("195","122","202","910001","5","DFAH SUPPORT SERVICE","400","0","10","1","99922","5","","");
INSERT INTO sales_order_details VALUES("193","120","202","DFGDFG","5","Sarojni","500","0","10","1","fgdfgdfg","0","","");
INSERT INTO sales_order_details VALUES("194","121","201","910001","5","DFAH SUPPORT SERVICE","400","0","10","1","99922","5","","");
INSERT INTO sales_order_details VALUES("201","24","201","","1","mnmnmnmn","222222","0","1","0","-","0","","");
INSERT INTO sales_order_details VALUES("196","123","201","22222","2","TABLET","25000","0","1","1","7676766166","0","","");
INSERT INTO sales_order_details VALUES("197","124","202","22222","2","TABLET","25000","0","1","1","7676766166","0","","");
INSERT INTO sales_order_details VALUES("198","125","201","","7","cfgh","3566777","0","1","0","","0","","");
INSERT INTO sales_order_details VALUES("199","126","202","","7","cfgh","3566777","0","1","0","","0","","");
INSERT INTO sales_order_details VALUES("200","24","201","","1","1","0","0","1","0","-","0","","");
INSERT INTO sales_order_details VALUES("202","127","201","KK","5","DELL Laptop","45000","0","2","1","15","0","","");
INSERT INTO sales_order_details VALUES("203","127","201","","4","DELL Services","2000","0","1","0","","0","","");
INSERT INTO sales_order_details VALUES("204","128","201","KK","5","DELL Laptop","45000","0","2","1","15","0","","");
INSERT INTO sales_order_details VALUES("205","129","202","KK","5","DELL Laptop","45000","0","2","1","15","0","","");
INSERT INTO sales_order_details VALUES("206","128","201","","4","services","2000","0","1","0","","0","","");
INSERT INTO sales_order_details VALUES("207","129","202","","4","services","2000","0","1","0","","0","","");





CREATE TABLE IF NOT EXISTS `sales_orders` (
  `order_no` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `trans_type` mediumint(9) NOT NULL,
  `invoice_type` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `debtor_no` int(11) NOT NULL,
  `branch_id` tinyint(4) NOT NULL,
  `person_id` int(11) NOT NULL,
  `reference` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `customer_ref` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_reference_id` int(11) NOT NULL,
  `order_reference` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comments` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ord_date` date NOT NULL,
  `order_type` int(11) NOT NULL,
  `from_stk_loc` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_id` mediumint(9) NOT NULL,
  `total` double NOT NULL DEFAULT '0',
  `paid_amount` double NOT NULL DEFAULT '0',
  `payment_term` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`order_no`)
) ENGINE=MyISAM AUTO_INCREMENT=130 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO sales_orders VALUES("1","201","indirectOrder","1","1","1","SO-0001","","0","","","2017-07-24","0","PL","2","23","0","0","2017-07-23 19:10:35","");
INSERT INTO sales_orders VALUES("3","201","indirectOrder","1","1","1","SO-0002","","0","","","2017-07-24","0","PL","2","23","0","0","2017-07-23 23:37:02","");
INSERT INTO sales_orders VALUES("24","201","directOrder","1","1","1","SO-0011","","0","","","2017-07-26","0","PL","2","246189","0","2","2017-07-26 09:43:38","2017-08-31 05:21:41");
INSERT INTO sales_orders VALUES("5","201","indirectOrder","5","5","1","SO-0003","","0","","","2017-07-24","0","PL","2","23","0","0","2017-07-24 00:00:44","");
INSERT INTO sales_orders VALUES("25","202","indirectInvoice","1","1","1","INV-0001","","24","SO-0011","","2017-07-26","0","PL","2","50.75","50.75","2","2017-07-26 09:43:54","");
INSERT INTO sales_orders VALUES("7","201","indirectOrder","1","1","1","SO-0004","","0","","","2017-07-24","0","PL","2","23","0","0","2017-07-24 00:03:18","");
INSERT INTO sales_orders VALUES("26","201","indirectOrder","2","2","1","SO-0012","","0","","","2017-07-26","0","PL","2","230","0","0","2017-07-26 10:14:42","");
INSERT INTO sales_orders VALUES("9","201","indirectOrder","5","5","1","SO-0005","","0","","","2017-07-24","0","PL","2","11.5","0","0","2017-07-24 00:03:36","");
INSERT INTO sales_orders VALUES("27","202","directInvoice","2","2","1","INV-0002","","26","SO-0012","","2017-07-26","0","PL","2","230","100","1","2017-07-26 10:14:42","");
INSERT INTO sales_orders VALUES("28","201","indirectOrder","1","1","1","SO-0013","","0","","","2017-07-27","0","PL","2","141.6","0","0","2017-07-27 00:45:20","");
INSERT INTO sales_orders VALUES("29","202","directInvoice","1","1","1","INV-0003","","28","SO-0013","","2017-07-27","0","PL","2","141.6","0","1","2017-07-27 00:45:20","");
INSERT INTO sales_orders VALUES("31","202","directInvoice","1","1","1","INV-0004","","30","SO-0014","","2017-07-27","0","PL","2","1416","0","1","2017-07-27 09:44:38","");
INSERT INTO sales_orders VALUES("20","201","indirectOrder","6","6","1","SO-0010","","0","","","2017-07-26","0","PL","2","51.75","0","0","2017-07-25 17:50:34","");
INSERT INTO sales_orders VALUES("30","201","indirectOrder","1","1","1","SO-0014","","0","","","2017-07-27","0","PL","2","1416","0","0","2017-07-27 09:44:38","");
INSERT INTO sales_orders VALUES("32","201","indirectOrder","1","1","1","SO-0015","","0","","","2017-07-27","0","JA","1","2766.42","0","0","2017-07-27 10:46:22","");
INSERT INTO sales_orders VALUES("33","202","directInvoice","1","1","1","INV-0005","","32","SO-0015","","2017-07-27","0","JA","1","2994.84","0","1","2017-07-27 10:46:22","2017-07-27 10:51:09");
INSERT INTO sales_orders VALUES("34","201","indirectOrder","3","3","1","SO-0016","","0","","","2017-07-27","0","JA","2","1964.7","0","0","2017-07-27 11:23:44","");
INSERT INTO sales_orders VALUES("35","202","directInvoice","3","3","1","INV-0006","","34","SO-0016","","2017-07-27","0","JA","2","16264.7","16242.7","2","2017-07-27 11:23:44","2017-08-23 11:40:54");
INSERT INTO sales_orders VALUES("36","201","indirectOrder","3","3","1","SO-0017","","0","","","2017-07-27","0","PL","1","12744","0","0","2017-07-27 11:38:30","");
INSERT INTO sales_orders VALUES("37","202","directInvoice","3","3","1","INV-0007","","36","SO-0017","","2017-07-27","0","PL","1","12744","12744","2","2017-07-27 11:38:30","");
INSERT INTO sales_orders VALUES("38","201","indirectOrder","3","3","1","SO-0018","","0","","","2017-07-27","0","PL","2","138768","0","0","2017-07-27 11:54:44","");
INSERT INTO sales_orders VALUES("39","202","directInvoice","1","3","1","INV-0008","","38","SO-0018","INVOICE ORDER TERMS AND CONDITIONS\n\n1.	Prices And Payment\n\n1.1.	Payments are to be made in U.S funds. Unless otherwise specified all invoices are due net 30 days from date of Shipment. PRICES INVOICED WILL BE THOSE IN EFFECT AT TIME OF SHIPMENT. All p","2017-07-27","0","PL","2","1382976","138768","2","2017-07-27 11:54:44","2017-07-29 08:36:19");
INSERT INTO sales_orders VALUES("85","201","directOrder","2","2","1","SO-0043","","0","","","2017-08-25","0","JA","2","11.8","0","2","2017-08-25 16:59:41","");
INSERT INTO sales_orders VALUES("86","201","indirectOrder","1","1","1","SO-0044","","0","","","2017-08-25","0","PL","2","11.8","0","0","2017-08-25 17:00:19","");
INSERT INTO sales_orders VALUES("87","202","directInvoice","1","1","1","INV-0029","","86","SO-0044","","2017-08-25","0","PL","2","17.7","17.7","5","2017-08-25 17:00:19","2017-08-25 17:03:58");
INSERT INTO sales_orders VALUES("40","201","indirectOrder","2","2","1","SO-0019","","0","","","2017-07-29","0","PL","2","22000","0","0","2017-07-29 05:54:59","");
INSERT INTO sales_orders VALUES("41","202","directInvoice","2","2","1","INV-0009","","40","SO-0019","","2017-07-29","0","PL","2","22000","0","4","2017-07-29 05:54:59","");
INSERT INTO sales_orders VALUES("42","201","indirectOrder","1","1","1","SO-0020","","0","","","2017-07-29","0","PL","2","23600","0","0","2017-07-29 05:56:27","");
INSERT INTO sales_orders VALUES("43","202","directInvoice","1","1","1","INV-0010","","42","SO-0020","","2017-07-29","0","PL","2","23600","0","4","2017-07-29 05:56:27","");
INSERT INTO sales_orders VALUES("44","201","indirectOrder","1","1","1","SO-0021","","0","","","2017-07-29","0","PL","2","21800","0","0","2017-07-29 05:57:20","");
INSERT INTO sales_orders VALUES("45","202","directInvoice","1","1","1","INV-0011","","44","SO-0021","","2017-07-29","0","PL","2","23600","0","4","2017-07-29 05:57:20","2017-07-29 05:58:33");
INSERT INTO sales_orders VALUES("46","201","directOrder","4","4","1","SO-0022","","0","","","2017-07-30","0","PL","2","23710","0","2","2017-07-30 05:42:43","");
INSERT INTO sales_orders VALUES("48","201","directOrder","5","5","1","SO-0023","","0","","","2017-07-30","0","PL","2","110","0","2","2017-07-30 05:45:02","");
INSERT INTO sales_orders VALUES("49","201","directOrder","4","4","1","SO-0024","","0","","","2017-07-30","0","PL","2","110","0","2","2017-07-30 06:17:05","");
INSERT INTO sales_orders VALUES("50","202","indirectInvoice","4","4","1","INV-0012","","49","SO-0024","","2017-07-30","0","PL","2","110","0","2","2017-07-30 06:17:18","");
INSERT INTO sales_orders VALUES("51","201","indirectOrder","1","1","1","SO-0025","","0","","","2017-07-30","0","PL","2","11445","0","0","2017-07-30 16:56:50","");
INSERT INTO sales_orders VALUES("52","202","directInvoice","1","1","1","INV-0013","","51","SO-0025","","2017-07-30","0","PL","2","11445","0","4","2017-07-30 16:56:50","");
INSERT INTO sales_orders VALUES("53","201","indirectOrder","4","4","1","SO-0026","","0","","","2017-07-30","0","PL","2","46000","0","0","2017-07-30 16:59:11","");
INSERT INTO sales_orders VALUES("54","202","directInvoice","4","4","1","INV-0014","","53","SO-0026","test note","2017-07-30","0","PL","2","46017.7","0","4","2017-07-30 16:59:11","2017-08-03 09:45:42");
INSERT INTO sales_orders VALUES("55","201","directOrder","6","6","1","SO-0027","","0","","","2017-08-05","0","PL","2","59000","0","2","2017-08-05 14:55:18","2017-08-05 14:58:04");
INSERT INTO sales_orders VALUES("56","201","indirectOrder","6","6","1","SO-0028","","0","","dfah service","2017-08-05","0","PL","2","56640","0","0","2017-08-05 14:58:58","");
INSERT INTO sales_orders VALUES("57","202","directInvoice","6","6","1","INV-0015","","56","SO-0028","dfah service","2017-08-05","0","PL","2","56640","0","5","2017-08-05 14:58:58","");
INSERT INTO sales_orders VALUES("58","201","directOrder","6","6","1","SO-0029","","0","","test not","2017-08-05","0","PL","2","10900","0","2","2017-08-05 15:00:49","");
INSERT INTO sales_orders VALUES("59","202","indirectInvoice","4","6","1","INV-0016","","58","SO-0029","test not","2017-08-05","0","PL","2","10917.25","10900","2","2017-08-05 15:01:03","2017-08-06 05:28:35");
INSERT INTO sales_orders VALUES("60","201","directOrder","3","3","1","SO-0030","","0","","","2017-08-08","0","JA","2","23600","0","2","2017-08-08 10:12:37","");
INSERT INTO sales_orders VALUES("61","201","indirectOrder","2","2","1","SO-0031","","0","","","2017-08-08","0","JA","2","23600","0","0","2017-08-08 12:10:27","");
INSERT INTO sales_orders VALUES("62","202","directInvoice","2","2","1","INV-0017","","61","SO-0031","","2017-08-08","0","JA","2","23600","0","4","2017-08-08 12:10:27","");
INSERT INTO sales_orders VALUES("63","201","indirectOrder","1","1","1","SO-0032","","0","","","2017-08-10","0","PL","2","2360","0","0","2017-08-10 05:38:53","");
INSERT INTO sales_orders VALUES("64","202","directInvoice","1","1","1","INV-0018","","63","SO-0032","","2017-08-10","0","PL","2","2360","0","4","2017-08-10 05:38:53","");
INSERT INTO sales_orders VALUES("65","201","indirectOrder","1","1","1","SO-0033","","0","","","2017-08-11","0","PL","2","6948.95","0","0","2017-08-11 18:09:44","");
INSERT INTO sales_orders VALUES("66","202","directInvoice","1","1","1","INV-0019","","65","SO-0033","","2017-08-11","0","PL","2","6948.95","6948.95","4","2017-08-11 18:09:44","");
INSERT INTO sales_orders VALUES("67","201","indirectOrder","3","3","1","SO-0034","","0","","","2017-08-18","0","JA","2","16.35","0","0","2017-08-18 12:17:02","");
INSERT INTO sales_orders VALUES("68","202","directInvoice","3","3","1","INV-0020","","67","SO-0034","","2017-08-18","0","JA","2","16.35","0","4","2017-08-18 12:17:02","");
INSERT INTO sales_orders VALUES("69","201","indirectOrder","1","1","1","SO-0035","","0","","","2017-08-21","0","JA","2","236","0","0","2017-08-21 13:16:04","");
INSERT INTO sales_orders VALUES("70","202","directInvoice","1","1","1","INV-0021","","69","SO-0035","","2017-08-21","0","JA","2","236","0","4","2017-08-21 13:16:04","");
INSERT INTO sales_orders VALUES("71","201","indirectOrder","7","7","1","SO-0036","","0","","","2017-08-21","0","PL","2","590","0","0","2017-08-21 13:19:54","");
INSERT INTO sales_orders VALUES("72","202","directInvoice","7","7","1","INV-0022","","71","SO-0036","","2017-08-21","0","PL","2","590","590","3","2017-08-21 13:19:54","");
INSERT INTO sales_orders VALUES("73","201","indirectOrder","3","3","1","SO-0037","","0","","","2017-08-22","0","JA","2","119.9","0","0","2017-08-22 12:17:05","");
INSERT INTO sales_orders VALUES("74","202","directInvoice","3","3","1","INV-0023","","73","SO-0037","","2017-08-22","0","JA","2","119.9","0","5","2017-08-22 12:17:05","");
INSERT INTO sales_orders VALUES("75","201","indirectOrder","1","1","1","SO-0038","","0","","","2017-08-23","0","JA","2","22","0","0","2017-08-23 14:17:04","");
INSERT INTO sales_orders VALUES("76","202","directInvoice","1","1","1","INV-0024","","75","SO-0038","","2017-08-23","0","JA","2","22","0","1","2017-08-23 14:17:04","");
INSERT INTO sales_orders VALUES("77","201","indirectOrder","1","1","1","SO-0039","","0","","","2017-08-23","0","PL","2","236","0","0","2017-08-23 20:10:07","");
INSERT INTO sales_orders VALUES("78","202","directInvoice","1","1","1","INV-0025","","77","SO-0039","","2017-08-23","0","PL","2","236","0","2","2017-08-23 20:10:07","");
INSERT INTO sales_orders VALUES("79","201","indirectOrder","1","1","1","SO-0040","","0","","","2017-08-24","0","PL","2","10.8","0","0","2017-08-24 07:14:42","");
INSERT INTO sales_orders VALUES("80","202","directInvoice","1","1","1","INV-0026","","79","SO-0040","","2017-08-24","0","PL","2","11.68","12.744","5","2017-08-24 07:14:42","2017-08-25 11:30:35");
INSERT INTO sales_orders VALUES("81","201","directOrder","9","9","1","SO-0041","","0","","","2017-08-24","0","JA","2","14980","0","2","2017-08-24 13:25:12","2017-08-28 12:54:09");
INSERT INTO sales_orders VALUES("82","202","indirectInvoice","9","9","1","INV-0027","","81","SO-0041","","2017-08-24","0","PL","2","14980","0","2","2017-08-24 13:26:13","");
INSERT INTO sales_orders VALUES("83","201","indirectOrder","1","1","1","SO-0042","","0","","","2017-08-25","0","JA","2","259.6","0","0","2017-08-25 08:25:17","");
INSERT INTO sales_orders VALUES("84","202","directInvoice","1","1","1","INV-0028","","83","SO-0042","","2017-08-25","0","JA","2","239.8","0","2","2017-08-25 08:25:17","2017-08-25 09:28:05");
INSERT INTO sales_orders VALUES("88","201","indirectOrder","2","2","1","SO-0045","","0","","","2017-08-26","0","PL","2","9772","0","0","2017-08-25 21:22:20","");
INSERT INTO sales_orders VALUES("89","202","directInvoice","2","2","1","INV-0030","","88","SO-0045","","2017-08-26","0","PL","2","10384","0","5","2017-08-25 21:22:20","2017-08-25 21:27:39");
INSERT INTO sales_orders VALUES("90","201","indirectOrder","1","1","1","SO-0046","","0","","","2017-08-26","0","JA","2","141600","0","0","2017-08-26 07:28:06","");
INSERT INTO sales_orders VALUES("91","202","directInvoice","1","1","1","INV-0031","","90","SO-0046","","2017-08-26","0","JA","2","141600","0","4","2017-08-26 07:28:06","");
INSERT INTO sales_orders VALUES("92","201","indirectOrder","1","1","1","SO-0047","","0","","","2017-08-27","0","PL","2","14182","0","0","2017-08-27 05:09:04","");
INSERT INTO sales_orders VALUES("93","202","directInvoice","1","1","1","INV-0032","","92","SO-0047","","2017-08-27","0","PL","2","14182","15000","4","2017-08-27 05:09:04","2017-08-29 09:08:51");
INSERT INTO sales_orders VALUES("94","201","indirectOrder","2","2","1","SO-0048","","0","","","2017-08-27","0","JA","2","14160","0","0","2017-08-27 06:29:02","");
INSERT INTO sales_orders VALUES("95","202","directInvoice","2","2","1","INV-0033","","94","SO-0048","","2017-08-27","0","JA","2","14160","0","2","2017-08-27 06:29:02","");
INSERT INTO sales_orders VALUES("96","201","indirectOrder","10","10","1","SO-0049","","0","","","2017-08-27","0","PL","2","14160","0","0","2017-08-27 06:30:38","");
INSERT INTO sales_orders VALUES("97","202","directInvoice","10","10","1","INV-0034","","96","SO-0049","","2017-08-27","0","PL","2","13080","0","4","2017-08-27 06:30:38","2017-08-27 06:31:03");
INSERT INTO sales_orders VALUES("98","201","indirectOrder","10","10","1","SO-0050","","0","","","2017-08-27","0","PL","2","14160","0","0","2017-08-27 06:33:12","");
INSERT INTO sales_orders VALUES("99","202","directInvoice","10","10","1","INV-0035","","98","SO-0050","","2017-08-27","0","PL","2","14160","0","4","2017-08-27 06:33:12","");
INSERT INTO sales_orders VALUES("100","201","indirectOrder","7","7","1","SO-0051","","0","","","2017-08-27","0","PL","2","236220","0","0","2017-08-27 09:27:39","");
INSERT INTO sales_orders VALUES("101","202","directInvoice","7","7","1","INV-0036","","100","SO-0051","","2017-08-27","0","PL","2","236220","0","4","2017-08-27 09:27:39","");
INSERT INTO sales_orders VALUES("102","201","indirectOrder","1","1","1","SO-0052","","0","","","2017-08-28","0","PL","2","23.6","0","0","2017-08-27 19:13:52","");
INSERT INTO sales_orders VALUES("103","202","directInvoice","1","1","1","INV-0037","","102","SO-0052","","2017-08-28","0","PL","2","23.6","0","1","2017-08-27 19:13:52","");
INSERT INTO sales_orders VALUES("104","201","indirectOrder","2","2","1","SO-0053","","0","","","2017-08-28","0","JA","2","23.6","0","0","2017-08-27 19:14:28","");
INSERT INTO sales_orders VALUES("105","202","directInvoice","2","2","1","INV-0038","","104","SO-0053","","2017-08-28","0","JA","2","23.6","0","4","2017-08-27 19:14:28","");
INSERT INTO sales_orders VALUES("106","201","indirectOrder","11","11","1","SO-0054","","0","","","2017-08-28","0","PL","2","1200","0","0","2017-08-28 14:21:04","");
INSERT INTO sales_orders VALUES("107","202","directInvoice","11","11","1","INV-0039","","106","SO-0054","","2017-08-28","0","PL","2","436","0","5","2017-08-28 14:21:04","2017-08-28 15:21:39");
INSERT INTO sales_orders VALUES("108","201","directOrder","2","2","1","SO-0055","","0","","","2017-08-28","0","JA","2","990","0","2","2017-08-28 18:24:12","");
INSERT INTO sales_orders VALUES("109","202","indirectInvoice","2","2","1","INV-0040","","108","SO-0055","","2017-08-28","0","JA","2","990","0","2","2017-08-28 18:24:30","");
INSERT INTO sales_orders VALUES("110","201","indirectOrder","1","1","1","SO-0056","","0","","","2017-08-29","0","JA","2","0","0","0","2017-08-29 02:22:21","");
INSERT INTO sales_orders VALUES("111","202","directInvoice","1","1","1","INV-0041","","110","SO-0056","","2017-08-29","0","JA","2","5450","0","5","2017-08-29 02:22:21","2017-08-30 16:03:21");
INSERT INTO sales_orders VALUES("112","201","indirectOrder","2","2","1","SO-0057","","0","","","2017-08-29","0","PL","2","110","0","0","2017-08-29 11:59:25","");
INSERT INTO sales_orders VALUES("113","202","directInvoice","2","2","1","INV-0042","","112","SO-0057","","2017-08-29","0","PL","2","0","110","5","2017-08-29 11:59:25","2017-08-30 12:33:32");
INSERT INTO sales_orders VALUES("122","202","directInvoice","4","4","1","INV-0044","","121","SO-0059","","2017-08-30","0","JA","2","4484","4484","1","2017-08-30 17:37:20","");
INSERT INTO sales_orders VALUES("119","201","indirectOrder","1","1","1","SO-0058","","0","","","2017-08-30","0","JA","1","5750","0","0","2017-08-30 17:33:00","");
INSERT INTO sales_orders VALUES("120","202","directInvoice","1","1","1","INV-0043","","119","SO-0058","","2017-08-30","0","JA","1","5900","0","2","2017-08-30 17:33:00","2017-08-30 21:58:50");
INSERT INTO sales_orders VALUES("121","201","indirectOrder","4","4","1","SO-0059","","0","","","2017-08-30","0","JA","2","4484","0","0","2017-08-30 17:37:20","");
INSERT INTO sales_orders VALUES("123","201","indirectOrder","1","1","1","SO-0060","","0","","","2017-08-31","0","PL","2","27500","0","0","2017-08-31 01:06:35","");
INSERT INTO sales_orders VALUES("124","202","directInvoice","1","1","1","INV-0045","","123","SO-0060","","2017-08-31","0","PL","2","27500","27500","4","2017-08-31 01:06:35","");
INSERT INTO sales_orders VALUES("125","201","indirectOrder","1","1","1","SO-0061","","0","","","2017-08-31","0","JA","2","3887786.93","0","0","2017-08-31 03:00:31","");
INSERT INTO sales_orders VALUES("126","202","directInvoice","1","1","1","INV-0046","","125","SO-0061","","2017-08-31","0","JA","2","3887786.93","0","4","2017-08-31 03:00:31","");
INSERT INTO sales_orders VALUES("127","201","directOrder","15","15","4","SO-0062","","0","","","2017-08-31","0","PH","2","108300","0","2","2017-08-31 12:37:26","");
INSERT INTO sales_orders VALUES("128","201","indirectOrder","15","15","4","SO-0063","","0","","keep the data for future","2017-08-31","0","PH","2","108300","0","0","2017-08-31 12:40:14","");
INSERT INTO sales_orders VALUES("129","202","directInvoice","15","15","4","INV-0047","","128","SO-0063","keep the data for future","2017-08-31","0","PH","2","108300","108300","4","2017-08-31 12:40:14","");





CREATE TABLE IF NOT EXISTS `sales_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sales_type` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `tax_included` tinyint(4) NOT NULL,
  `factor` double NOT NULL,
  `defaults` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO sales_types VALUES("1","Retail","1","0","1");
INSERT INTO sales_types VALUES("2","Wholesale","0","0","0");





CREATE TABLE IF NOT EXISTS `security_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sections` text COLLATE utf8_unicode_ci,
  `areas` text COLLATE utf8_unicode_ci,
  `inactive` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO security_role VALUES("1","System Administrator","System Administrator","a:26:{s:8:\"category\";s:3:\"100\";s:4:\"unit\";s:3:\"600\";s:3:\"loc\";s:3:\"200\";s:4:\"item\";s:3:\"300\";s:4:\"user\";s:3:\"400\";s:4:\"role\";s:3:\"500\";s:8:\"customer\";s:3:\"700\";s:8:\"purchase\";s:3:\"900\";s:8:\"supplier\";s:4:\"1000\";s:7:\"payment\";s:4:\"1400\";s:6:\"backup\";s:4:\"1500\";s:5:\"email\";s:4:\"1600\";s:9:\"emailtemp\";s:4:\"1700\";s:10:\"preference\";s:4:\"1800\";s:3:\"tax\";s:4:\"1900\";s:10:\"currencies\";s:4:\"2100\";s:11:\"paymentterm\";s:4:\"2200\";s:13:\"paymentmethod\";s:4:\"2300\";s:14:\"companysetting\";s:4:\"2400\";s:10:\"iecategory\";s:4:\"2600\";s:7:\"expense\";s:4:\"2700\";s:7:\"deposit\";s:4:\"3000\";s:9:\"quotation\";s:4:\"2800\";s:7:\"invoice\";s:4:\"2900\";s:12:\"bank_account\";s:4:\"3100\";s:21:\"bank_account_transfer\";s:4:\"3200\";}","a:59:{s:7:\"cat_add\";s:3:\"101\";s:8:\"cat_edit\";s:3:\"102\";s:10:\"cat_delete\";s:3:\"103\";s:8:\"unit_add\";s:3:\"601\";s:9:\"unit_edit\";s:3:\"602\";s:11:\"unit_delete\";s:3:\"603\";s:7:\"loc_add\";s:3:\"201\";s:8:\"loc_edit\";s:3:\"202\";s:10:\"loc_delete\";s:3:\"203\";s:8:\"item_add\";s:3:\"301\";s:9:\"item_edit\";s:3:\"302\";s:11:\"item_delete\";s:3:\"303\";s:8:\"user_add\";s:3:\"401\";s:9:\"user_edit\";s:3:\"402\";s:11:\"user_delete\";s:3:\"403\";s:12:\"customer_add\";s:3:\"701\";s:13:\"customer_edit\";s:3:\"702\";s:15:\"customer_delete\";s:3:\"703\";s:12:\"purchase_add\";s:3:\"901\";s:13:\"purchase_edit\";s:3:\"902\";s:15:\"purchase_delete\";s:3:\"903\";s:12:\"supplier_add\";s:4:\"1001\";s:13:\"supplier_edit\";s:4:\"1002\";s:15:\"supplier_delete\";s:4:\"1003\";s:11:\"payment_add\";s:4:\"1401\";s:12:\"payment_edit\";s:4:\"1402\";s:14:\"payment_delete\";s:4:\"1403\";s:10:\"backup_add\";s:4:\"1501\";s:15:\"backup_download\";s:4:\"1502\";s:7:\"tax_add\";s:4:\"1901\";s:8:\"tax_edit\";s:4:\"1902\";s:10:\"tax_delete\";s:4:\"1903\";s:14:\"currencies_add\";s:4:\"2101\";s:15:\"currencies_edit\";s:4:\"2102\";s:17:\"currencies_delete\";s:4:\"2103\";s:15:\"paymentterm_add\";s:4:\"2201\";s:16:\"paymentterm_edit\";s:4:\"2202\";s:18:\"paymentterm_delete\";s:4:\"2203\";s:17:\"paymentmethod_add\";s:4:\"2301\";s:18:\"paymentmethod_edit\";s:4:\"2302\";s:20:\"paymentmethod_delete\";s:4:\"2303\";s:11:\"expense_add\";s:4:\"2701\";s:12:\"expense_edit\";s:4:\"2702\";s:14:\"expense_delete\";s:4:\"2703\";s:11:\"deposit_add\";s:4:\"3001\";s:12:\"deposit_edit\";s:4:\"3002\";s:14:\"deposit_delete\";s:4:\"3003\";s:13:\"quotation_add\";s:4:\"2801\";s:14:\"quotation_edit\";s:4:\"2802\";s:16:\"quotation_delete\";s:4:\"2803\";s:11:\"invoice_add\";s:4:\"2901\";s:12:\"invoice_edit\";s:4:\"2902\";s:14:\"invoice_delete\";s:4:\"2903\";s:16:\"bank_account_add\";s:4:\"3101\";s:17:\"bank_account_edit\";s:4:\"3102\";s:19:\"bank_account_delete\";s:4:\"3103\";s:25:\"bank_account_transfer_add\";s:4:\"3201\";s:26:\"bank_account_transfer_edit\";s:4:\"3202\";s:28:\"bank_account_transfer_delete\";s:4:\"3203\";}","0","2017-07-22 18:13:53","");





CREATE TABLE IF NOT EXISTS `shipment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_no` int(11) NOT NULL,
  `trans_type` int(11) NOT NULL,
  `comments` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `packed_date` date NOT NULL,
  `delivery_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;






CREATE TABLE IF NOT EXISTS `shipment_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `shipment_id` int(11) NOT NULL,
  `order_no` int(11) NOT NULL,
  `stock_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tax_type_id` tinyint(4) NOT NULL,
  `unit_price` double NOT NULL,
  `quantity` double NOT NULL,
  `discount_percent` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;






CREATE TABLE IF NOT EXISTS `states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `country_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

INSERT INTO states VALUES("1","Andaman and Nicobar Islands","100");
INSERT INTO states VALUES("2","Andhra Pradesh","100");
INSERT INTO states VALUES("3","Arunachal Pradesh","100");
INSERT INTO states VALUES("4","Assam","100");
INSERT INTO states VALUES("5","Bihar","100");
INSERT INTO states VALUES("6","Chandigarh","100");
INSERT INTO states VALUES("7","Chhattisgarh","100");
INSERT INTO states VALUES("8","Dadra and Nagar Haveli","100");
INSERT INTO states VALUES("9","Daman and Diu","100");
INSERT INTO states VALUES("10","Delhi","100");
INSERT INTO states VALUES("11","Goa","100");
INSERT INTO states VALUES("12","Gujarat","100");
INSERT INTO states VALUES("13","Haryana","100");
INSERT INTO states VALUES("14","Himachal Pradesh","100");
INSERT INTO states VALUES("15","Jammu and Kashmir","100");
INSERT INTO states VALUES("16","Jharkhand","100");
INSERT INTO states VALUES("17","Karnataka","100");
INSERT INTO states VALUES("18","Kenmore","100");
INSERT INTO states VALUES("19","Kerala","100");
INSERT INTO states VALUES("20","Lakshadweep","100");
INSERT INTO states VALUES("21","Madhya Pradesh","100");
INSERT INTO states VALUES("22","Maharashtra","100");
INSERT INTO states VALUES("23","Manipur","100");
INSERT INTO states VALUES("24","Meghalaya","100");
INSERT INTO states VALUES("25","Mizoram","100");
INSERT INTO states VALUES("26","Nagaland","100");
INSERT INTO states VALUES("27","Narora","100");
INSERT INTO states VALUES("28","Natwar","100");
INSERT INTO states VALUES("29","Odisha","100");
INSERT INTO states VALUES("30","Paschim Medinipur","100");
INSERT INTO states VALUES("31","Pondicherry","100");
INSERT INTO states VALUES("32","Punjab","100");
INSERT INTO states VALUES("33","Rajasthan","100");
INSERT INTO states VALUES("34","Sikkim","100");
INSERT INTO states VALUES("35","Tamil Nadu","100");
INSERT INTO states VALUES("36","Telangana","100");
INSERT INTO states VALUES("37","Tripura","100");
INSERT INTO states VALUES("38","Uttar Pradesh","100");
INSERT INTO states VALUES("39","Uttarakhand","100");
INSERT INTO states VALUES("40","Vaishali","100");
INSERT INTO states VALUES("41","West Bengal","100");





CREATE TABLE IF NOT EXISTS `stock_category` (
  `category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `dflt_units` int(11) NOT NULL,
  `inactive` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO stock_category VALUES("1","Default","1","0","2017-07-22 18:13:53","");
INSERT INTO stock_category VALUES("2","Hardware","1","0","2017-07-22 18:13:53","");
INSERT INTO stock_category VALUES("3","Health & Beauty","1","0","2017-07-22 18:13:53","");
INSERT INTO stock_category VALUES("4","SERVICE","2","0","2017-08-05 14:52:36","");
INSERT INTO stock_category VALUES("5","Software","1","0","2017-08-28 12:41:35","");
INSERT INTO stock_category VALUES("6","FRUITS","3","0","2017-08-31 11:23:53","");
INSERT INTO stock_category VALUES("7","Electronic Gudgets","1","0","2017-08-31 11:24:58","");





CREATE TABLE IF NOT EXISTS `stock_master` (
  `stock_id` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` tinyint(4) NOT NULL,
  `tax_type_id` tinyint(4) NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `long_description` text COLLATE utf8_unicode_ci NOT NULL,
  `units` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `hsn` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `inactive` tinyint(4) NOT NULL DEFAULT '0',
  `deleted_status` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`stock_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO stock_master VALUES("TT1","3","2","pen","dsfsda","Each","","0","0");
INSERT INTO stock_master VALUES("22222","3","5","TABLET","","Each","7676766166","0","0");
INSERT INTO stock_master VALUES("1001","2","5","CAMERA","","Each","80011","0","1");
INSERT INTO stock_master VALUES("76767","2","5","iphone","phone","Each","78787772","0","0");
INSERT INTO stock_master VALUES("HP","2","1","HP PRO BOOK","","Each","hsn_no","0","0");
INSERT INTO stock_master VALUES("LENEVO","1","3","LED TV","","Each","LEDHSN","0","0");
INSERT INTO stock_master VALUES("N326","2","4","JEANS","silky fab","Each","","0","0");
INSERT INTO stock_master VALUES("910001","1","5","DFAH SUPPORT SERVICE","DFAH SUPPORT SERVICE","Each","99922","0","0");
INSERT INTO stock_master VALUES("RP01","2","8","RP01","Rp01","Each","Rp01","0","0");
INSERT INTO stock_master VALUES("DF","2","2","SDFDSF","dfsf","ACTIVITY","dfsd","1","0");
INSERT INTO stock_master VALUES("DFGDFG","3","1","Sarojni","dfgdfgdfgdfg","Each","fgdfgdfg","0","0");
INSERT INTO stock_master VALUES("HYDF","1","1","gjhdfgj","","Each","","0","0");
INSERT INTO stock_master VALUES("IT100","2","2","Item Name","Sample","Each","HSN100","0","0");
INSERT INTO stock_master VALUES("FVFVE","2","6","TSHIRT","","Each","fvve","0","0");
INSERT INTO stock_master VALUES("RS123","2","7","FURNITURE","","Each","","0","0");
INSERT INTO stock_master VALUES("10","2","5","WEBSIT4","Testing.","Each","23433454","0","0");
INSERT INTO stock_master VALUES("222","2","6","assess","","Each","444544","0","0");
INSERT INTO stock_master VALUES("06","3","4","nana","ewq","ACTIVITY","2","0","0");
INSERT INTO stock_master VALUES("8","3","4","banana","dsdd","Each","2","0","0");
INSERT INTO stock_master VALUES("KK","7","5","DELL Laptop","DELL Inspiron 15","Each","15","0","0");
INSERT INTO stock_master VALUES("123","1","1","abc","","Each","","0","0");





CREATE TABLE IF NOT EXISTS `stock_moves` (
  `trans_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `stock_id` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `order_no` int(11) NOT NULL,
  `trans_type` smallint(6) NOT NULL DEFAULT '0',
  `loc_code` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `tran_date` date NOT NULL,
  `person_id` int(11) DEFAULT NULL,
  `order_reference` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `reference` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `transaction_reference_id` int(11) NOT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `qty` double NOT NULL DEFAULT '0',
  `price` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`trans_id`)
) ENGINE=MyISAM AUTO_INCREMENT=100 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO stock_moves VALUES("34","76767","38","202","PL","2017-07-27","1","SO-0018","store_out_39","39","","-112","0");
INSERT INTO stock_moves VALUES("35","76767","34","202","JA","2017-07-28","1","SO-0006","store_out_35","35","","-1","0");
INSERT INTO stock_moves VALUES("36","22222","40","202","PL","2017-07-29","1","SO-0019","store_out_41","41","","-1","0");
INSERT INTO stock_moves VALUES("33","76767","36","202","PL","2017-07-27","1","SO-0017","store_out_37","37","","-1","0");
INSERT INTO stock_moves VALUES("32","76767","0","102","PL","2017-07-27","1","","store_in_8","8","","10","10000");
INSERT INTO stock_moves VALUES("31","LENEVO","34","202","JA","2017-07-27","1","SO-0016","store_out_35","35","","-111","0");
INSERT INTO stock_moves VALUES("30","LENEVO","32","202","JA","2017-07-27","1","SO-0015","store_out_33","33","","-188","0");
INSERT INTO stock_moves VALUES("29","1001","0","102","PL","2017-07-27","1","","store_in_7","7","","1","100");
INSERT INTO stock_moves VALUES("28","1001","30","202","PL","2017-07-27","1","SO-0014","store_out_31","31","","-10","0");
INSERT INTO stock_moves VALUES("23","HP","24","202","PL","2017-07-26","1","SO-0011","store_out_25","25","","-1","20");
INSERT INTO stock_moves VALUES("24","LENEVO","24","202","PL","2017-07-26","1","SO-0011","store_out_25","25","","-1","15");
INSERT INTO stock_moves VALUES("25","HP","26","202","PL","2017-07-26","1","SO-0012","store_out_27","27","","-10","0");
INSERT INTO stock_moves VALUES("26","HP","0","102","PL","2017-07-26","1","","store_in_6","6","","100","10");
INSERT INTO stock_moves VALUES("27","1001","28","202","PL","2017-07-27","1","SO-0013","store_out_29","29","","-1","0");
INSERT INTO stock_moves VALUES("37","22222","42","202","PL","2017-07-29","1","SO-0020","store_out_43","43","","-1","0");
INSERT INTO stock_moves VALUES("38","22222","44","202","PL","2017-07-29","1","SO-0021","store_out_45","45","","-1","0");
INSERT INTO stock_moves VALUES("39","LENEVO","0","102","PL","2017-07-29","1","","store_in_9","9","","1","10");
INSERT INTO stock_moves VALUES("40","22222","0","102","PL","2017-07-29","1","","store_in_9","9","","1","15000");
INSERT INTO stock_moves VALUES("41","LENEVO","0","102","PL","2017-07-29","1","","store_in_10","10","","1","10");
INSERT INTO stock_moves VALUES("42","22222","0","102","PL","2017-07-29","1","","store_in_10","10","","1","15000");
INSERT INTO stock_moves VALUES("46","N326","0","102","JA","2017-08-02","1","","store_in_11","11","","20","40000");
INSERT INTO stock_moves VALUES("45","TT1","49","202","PL","2017-07-30","1","SO-0024","store_out_50","50","","-1","100");
INSERT INTO stock_moves VALUES("47","TT1","0","102","PL","2017-08-03","1","","store_in_12","12","","1","50");
INSERT INTO stock_moves VALUES("48","76767","0","102","PL","2017-08-03","1","","store_in_12","12","","1","10000");
INSERT INTO stock_moves VALUES("49","LENEVO","53","202","PL","2017-08-03","1","SO-0014","store_out_54","54","","-1","0");
INSERT INTO stock_moves VALUES("50","910001","56","202","PL","2017-08-05","1","SO-0028","store_out_57","57","","-1","0");
INSERT INTO stock_moves VALUES("51","910001","58","202","PL","2017-08-05","1","SO-0029","store_out_59","59","","-1","10000");
INSERT INTO stock_moves VALUES("52","LENEVO","58","202","PL","2017-08-06","1","SO-0016","store_out_59","59","","-1","0");
INSERT INTO stock_moves VALUES("53","22222","61","202","JA","2017-08-08","1","SO-0031","store_out_62","62","","-1","0");
INSERT INTO stock_moves VALUES("54","N326","61","202","JA","2017-08-08","1","SO-0031","store_out_62","62","","-5","0");
INSERT INTO stock_moves VALUES("55","910001","65","202","PL","2017-08-11","1","SO-0033","store_out_66","66","","-1","0");
INSERT INTO stock_moves VALUES("56","N326","65","202","PL","2017-08-11","1","SO-0033","store_out_66","66","","-1","0");
INSERT INTO stock_moves VALUES("57","LENEVO","67","202","JA","2017-08-18","1","SO-0034","store_out_68","68","","-1","0");
INSERT INTO stock_moves VALUES("58","N326","0","102","PL","2017-08-18","1","","store_in_13","13","","200","500");
INSERT INTO stock_moves VALUES("59","910001","34","202","JA","2017-08-23","1","SO-0006","store_out_35","35","","-1","0");
INSERT INTO stock_moves VALUES("60","HP","34","202","JA","2017-08-23","1","SO-0006","store_out_35","35","","-1","0");
INSERT INTO stock_moves VALUES("61","N326","75","202","JA","2017-08-23","1","SO-0038","store_out_76","76","","-1","0");
INSERT INTO stock_moves VALUES("62","HP","75","202","JA","2017-08-23","1","SO-0038","store_out_76","76","","-1","0");
INSERT INTO stock_moves VALUES("63","N326","77","202","PL","2017-08-23","1","SO-0039","store_out_78","78","","-1","0");
INSERT INTO stock_moves VALUES("64","N326","79","202","PL","2017-08-24","1","SO-0040","store_out_80","80","","-1","0");
INSERT INTO stock_moves VALUES("65","LENEVO","0","102","JA","2017-08-24","1","","store_in_14","14","","13","18850");
INSERT INTO stock_moves VALUES("66","N326","81","202","PL","2017-08-24","1","SO-0041","store_out_82","82","","-10","1000");
INSERT INTO stock_moves VALUES("67","DFGDFG","81","202","PL","2017-08-24","1","SO-0041","store_out_82","82","","-1","2000");
INSERT INTO stock_moves VALUES("68","FVFVE","86","202","PL","2017-08-25","1","SO-0044","store_out_87","87","","-1","0");
INSERT INTO stock_moves VALUES("69","DFGDFG","88","202","PL","2017-08-26","1","SO-0045","store_out_89","89","","-1","0");
INSERT INTO stock_moves VALUES("70","76767","88","202","PL","2017-08-26","1","SO-0045","store_out_89","89","","-1","0");
INSERT INTO stock_moves VALUES("71","HP","88","202","PL","2017-08-26","1","SO-0045","store_out_89","89","","-1","0");
INSERT INTO stock_moves VALUES("72","76767","92","202","PL","2017-08-27","1","SO-0047","store_out_93","93","","-1","0");
INSERT INTO stock_moves VALUES("73","HP","92","202","PL","2017-08-27","1","SO-0047","store_out_93","93","","-1","0");
INSERT INTO stock_moves VALUES("74","N326","0","102","PL","2017-08-27","1","","store_in_15","15","","10","0");
INSERT INTO stock_moves VALUES("75","IT100","0","102","PL","2017-08-27","1","","store_in_15","15","","10","0");
INSERT INTO stock_moves VALUES("76","76767","94","202","JA","2017-08-27","1","SO-0048","store_out_95","95","","-1","0");
INSERT INTO stock_moves VALUES("77","76767","96","202","PL","2017-08-27","1","SO-0049","store_out_97","97","","-1","0");
INSERT INTO stock_moves VALUES("78","76767","98","202","PL","2017-08-27","1","SO-0050","store_out_99","99","","-1","0");
INSERT INTO stock_moves VALUES("79","HP","0","102","PL","2017-08-27","1","","store_in_16","16","","10","10");
INSERT INTO stock_moves VALUES("80","HP","100","202","PL","2017-08-27","1","SO-0051","store_out_101","101","","-10","0");
INSERT INTO stock_moves VALUES("81","22222","100","202","PL","2017-08-27","1","SO-0051","store_out_101","101","","-10","0");
INSERT INTO stock_moves VALUES("82","HP","102","202","PL","2017-08-28","1","SO-0052","store_out_103","103","","-1","0");
INSERT INTO stock_moves VALUES("83","HP","104","202","JA","2017-08-28","1","SO-0053","store_out_105","105","","-1","0");
INSERT INTO stock_moves VALUES("84","N326","106","202","PL","2017-08-28","1","SO-0039","store_out_107","107","","-1","0");
INSERT INTO stock_moves VALUES("85","910001","110","202","JA","2017-08-29","1","SO-0056","store_out_111","111","","-1","0");
INSERT INTO stock_moves VALUES("86","910001","112","202","PL","2017-08-29","1","SO-0042","store_out_113","113","","-1","0");
INSERT INTO stock_moves VALUES("98","KK","0","102","PH","2017-08-31","4","","store_in_18","18","","9","20000");
INSERT INTO stock_moves VALUES("97","22222","123","202","PL","2017-08-31","1","SO-0060","store_out_124","124","","-1","0");
INSERT INTO stock_moves VALUES("94","DFGDFG","119","202","JA","2017-08-30","1","SO-0058","store_out_120","120","","-10","0");
INSERT INTO stock_moves VALUES("95","910001","121","202","JA","2017-08-30","1","SO-0059","store_out_122","122","","-10","0");
INSERT INTO stock_moves VALUES("96","LENEVO","0","102","JA","2017-08-31","1","","store_in_17","17","","1","10");
INSERT INTO stock_moves VALUES("99","KK","128","202","PH","2017-08-31","4","SO-0063","store_out_129","129","","-2","0");





CREATE TABLE IF NOT EXISTS `stock_transfer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(11) NOT NULL,
  `source` varchar(55) COLLATE utf8_unicode_ci NOT NULL,
  `destination` varchar(55) COLLATE utf8_unicode_ci NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `qty` int(11) NOT NULL,
  `transfer_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;






CREATE TABLE IF NOT EXISTS `suppliers` (
  `supplier_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `supp_name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `contact` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `zipcode` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `gstin` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `inactive` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`supplier_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO suppliers VALUES("1","Theodore Lowe","lowe@yahoo.com","West","(793) 151-623","West","Assam","74577","IN","gstin623","0","2017-07-26 09:29:51","");
INSERT INTO suppliers VALUES("2","Melvin Porter","melvin@gmail.com","Washington","(959) 119-8364","Washington","","74755","US","gstin8364","0","2017-07-26 09:30:50","");
INSERT INTO suppliers VALUES("3","shiv","nileshpatel.en@gmail.com","ghgh, 6767  hghatg","","GN","Haryana","8787878","IN","87878727778","0","2017-07-27 11:36:05","");
INSERT INTO suppliers VALUES("4","reza","reza@gmail.com","dhaka","25478","dhaka","Chhattisgarh","1212","IN","2586","0","2017-07-30 05:39:30","");
INSERT INTO suppliers VALUES("5","karthik","kkk@gmail.com","vlp","6363463656","coimbatore","Tamil Nadu","Y87777","IN","3","0","2017-08-30 09:45:20","");
INSERT INTO suppliers VALUES("6","Milan Seth","Milu@gmail.com","xyz Road","1231231235","New Delhi","Delhi","12345","IN","","0","2017-08-31 12:16:38","");





CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `real_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `role_id` int(11) NOT NULL DEFAULT '1',
  `phone` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `picture` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `inactive` tinyint(4) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO users VALUES("1","","$2y$10$Zdboxmsf66MOKoag4Hn7fujb1vdXd0Z/iqQ.EZfUngr.5SxrseD0G","Admin","1","","admin@techvill.net","imgonline-com-ua-twotoone-rZIp12l0k7fy7inR.jpg","0","0XWDmykGiYMC9VdiUuzsh36UQAUA1KIVsCNeAgnECKNzrY35E9Wf2hVWlZyQ","2017-07-22 18:14:10","2017-08-31 05:37:07");
INSERT INTO users VALUES("2","","$2y$10$bcq8ySJmLLPT9TnpwGqaROizpV.j5o1pDGDUfr2O3FbrC.L2AXx2e","Infinity Worker","1","123456789","rkumarisahu@gmail.com","","0","GWJoKWykYYCCgmWlvIsyIdarYrWamS7qOiBjjhsh1usjjkTXFPkxLh8UJoCI","2017-08-28 12:21:10","2017-08-31 11:15:24");
INSERT INTO users VALUES("3","","$2y$10$p9.GuB1BCXFGJX4eEpVU7.xZzI7/PZTJR0S/Xg.NEsWu9JPME6ePK","Omaid Shuja","2","923219394194`","omaidshuja@gmail.com","","0","","2017-08-29 19:06:10","");
INSERT INTO users VALUES("4","","$2y$10$Nt94qQ4X4XmEPYy.YBlmR.N7RWWHSNeV4wYC4OeFqtzPpTCRdTHq6","Ben Lait","1","1234567890","benlait@yahoo.com","24ba51c7-d3e2-434a-9e1b-55f1b10d530b_6.jpg","0","E9j5gc041a63KDClMvTOlkevbgkXsHafzMuVGnM3GHyy1nLbUGSCDwIkDqXX","2017-08-31 11:08:11","2017-08-31 11:19:10");
INSERT INTO users VALUES("5","","$2y$10$GrohJqRDCDdHqOPI5svJUOgKC7gq/Xzjcvos4cmZInl.d7MWm8cca","Marttin Henning","2","7878909012","martinh@yahoo.com","","0","Af7vTluX0knGcYArzpL5UBdcwBYQIjVbCQnRX87o20uDV0oQsOQKpRVj2OoX","2017-08-31 11:11:06","2017-08-31 11:19:53");



