<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewFieldsUserIdToTableItemCode extends Migration
{
    /**
     * Run the migrations.
     *
     * Add use_id field due to separate the user items for each user
     *
     * @return void
     */
    public function up()
    {
        Schema::table('item_code', function (Blueprint $table) {
            $table->integer('user_id')->after('stock_id');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_code', function (Blueprint $table) {
            $table->dropColumn('user_id');
            $table->dropColumn('deleted_at');
        });
    }
}
