<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldCheckNoToTableBankTrans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bank_trans', function (Blueprint $table) {
            $table->string('check_no')->after('payment_method');
            $table->string('ref_no')->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bank_trans', function (Blueprint $table) {
            $table->dropColumn('check_no');
            $table->dropColumn('ref_no');
        });
    }
}
