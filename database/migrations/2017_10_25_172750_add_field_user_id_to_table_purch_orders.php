<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldUserIdToTablePurchOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purch_orders', function (Blueprint $table) {
            $table->integer('user_id')->after('order_no');
            $table->integer('deleted');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purch_orders', function (Blueprint $table) {
            $table->dropColumn('user_id');
            $table->dropColumn('deleted');
        });
    }
}
