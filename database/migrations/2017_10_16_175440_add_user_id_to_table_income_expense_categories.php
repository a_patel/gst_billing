<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserIdToTableIncomeExpenseCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('income_expense_categories', function (Blueprint $table) {
            $table->integer('user_id')->after('id');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('income_expense_categories', function (Blueprint $table) {
            $table->dropColumn('user_id');
            $table->dropColumn('deleted_at');
        });
    }
}
