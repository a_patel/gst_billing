<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserIdAndDeletedFieldToTableEmailTempDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('email_temp_details', function (Blueprint $table) {
            $table->integer('user_id')->after('id');
            $table->integer('deleted');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('email_temp_details', function (Blueprint $table) {
            $table->dropColumn('user_id');
            $table->dropColumn('deleted');
        });
    }
}
