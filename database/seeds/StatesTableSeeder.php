<?php

use Illuminate\Database\Seeder;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$countryState = array(
            array(
                'name'           => 'Andaman and Nicobar Islands',
                'country_id'     => 100
            ),
            array(
                'name'           => 'Andhra Pradesh',
                'country_id'     => 100
                
            ),
            array(
                'name'            => 'Arunachal Pradesh',
                'country_id'      => 100
                
            ),

            array(
                'name'            => 'Assam',
                'country_id'      => 100
                
            ),
            array(
                'name'             => 'Bihar',
                'country_id'       => 100
                
            ),
            array(
            	'name'             => 'Chandigarh',
            	'country_id'       => 100

            ),
            array(
                'name'             => 'Chhattisgarh',
                'country_id'       => 100
            ),
            array(
                'name'             => 'Dadra and Nagar Haveli',
                'country_id'       => 100
            ),
            array(
                'name'             => 'Daman and Diu',
                'country_id'       => 100
            ),
            array(
                'name'             => 'Delhi',
                'country_id'       => 100
            ),
            array(
                'name'             => 'Goa',
                'country_id'       => 100
            ),
            array(
                'name'             => 'Gujarat',
                'country_id'       => 100
            ),
            array(
                'name'             => 'Haryana',
                'country_id'       => 100
            ),
            array(
                'name'             => 'Himachal Pradesh',
                'country_id'       => 100
            ),
            array(
                'name'             => 'Jammu and Kashmir',
                'country_id'       => 100
            ),
            array(
                'name'             => 'Jharkhand',
                'country_id'       => 100
            ),
            array(
                'name'             => 'Karnataka',
                'country_id'       => 100
            ),
            array(
                'name'             => 'Kenmore',
                'country_id'       => 100
            ),
            array(
                'name'             => 'Kerala',
                'country_id'       => 100
            ),
            array(
                'name'             => 'Lakshadweep',
                'country_id'       => 100
            ),
            array(
                'name'             => 'Madhya Pradesh',
                'country_id'       => 100
            ),
            array(
                'name'             => 'Maharashtra',
                'country_id'       => 100
            ),
            array(
                'name'             => 'Manipur',
                'country_id'       => 100
            ),
            array(
                'name'             => 'Meghalaya',
                'country_id'       => 100
            ),
            array(
                'name'             => 'Mizoram',
                'country_id'       => 100
            )
        );
        DB::table('states')->truncate();
		DB::table('states')->insert($countryState);
        
    }
}
