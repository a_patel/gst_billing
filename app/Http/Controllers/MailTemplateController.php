<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use App\Http\Start\Helpers;

class MailTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['menu'] = 'setting';
        $data['list_menu'] = 'mail_temp';
        //$data['tempData'] = DB::table('email_temp')->get();
        
        return view('admin.mailTemp.temp_list', $data);
    }

    public function customerInvTemp($module, $id)
    {
        $data['menu'] = 'setting';
        $data['sub_menu'] = 'mail-temp';
        $data['list_menu'] = $module;
        //$data['list_menu'] = 'menu-'.$id;
        if($module == 'quotation'){
            $data['url'] = 'customer-invoice-temp';
            $temp_id = 1;
        }
        if($module == 'sales'){
            $data['url'] = 'customer-sales-invoice-temp';
            $temp_id = 2;
        }
        if($module == 'payment'){
            $data['url'] = 'customer-payment-notification-temp';
            $temp_id = 3;
        }
        $data['tempId'] = $id;
        $data['temp_Data'] = DB::table('email_temp_details')->where('temp_id',$temp_id)->where('user_id',$id)->get();
        //d( $data['temp_Data'],1);
        return view('admin.mailTemp.customer_invoice', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $module , $id)
    {
        if($module == 'quotation'){
            $url = 'customer-invoice-temp';
            $temp_id = 1;
        }elseif($module == 'sales'){
            $url = 'customer-sales-invoice-temp';
            $temp_id = 2;
        }
        elseif($module == 'payment'){
            $url = 'customer-payment-notification-temp';
            $temp_id = 3;
        }else{
            $url = 'customer-invoice-temp';
            $temp_id = 1;
        }
        $data[] = $request->en;
        $data[] = $request->ar;
        $data[] = $request->ch;
        $data[] = $request->fr;
        $data[] = $request->po;
        $data[] = $request->rh;
        $data[] = $request->sp;
        $data[] = $request->tu;

        $array = $data;
        array_unshift($array,"");
        unset($array[0]);

        for ($i=1; $i < 9 ; $i++) {
            DB::table('email_temp_details')->where([['user_id',$id],['temp_id',$temp_id],['lang_id', $i]])->update($array[$i]);
        }
        \Session::flash('success',trans('message.success.save_success'));
         return redirect()->intended("$url/$module/$id");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
