<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use DB;

class RegisterController extends Controller
{
    /* Register page view */
    public function register()
    {
        $data                = [];
        $data['companyData'] = \DB::table('preference')->where(['id' => 9])->first();
        return view('auth.register', $data);
    }

    /* User registration */
    public function createUser(Request $request)
    {
        $this->validate($request, User::$register_validation_rule);
        //if (Auth::attempt($data)) {
        $admin_data = \DB::table('admin_data')->first();
        $user = User::create([
                'real_name' => $request->name,
                'email'     => $request->email,
                'password'  => bcrypt($request->password),
                'expire_date'  => Carbon::now()->addDays($admin_data->trial_days),
            ]);
        \DB::table('role_user')->insert([
            ['user_id' => $user->id, 'role_id' => 1]
        ]);
        $lang = ['en','ar','ch','fr','po','rs','sp','tu'];
        $temp = [1, 2, 3, 4, 5];
        foreach ($temp as $tkey=>$t){
            $tempData = [];
            foreach ($lang as $key=>$value){
                $tempData[$key]['temp_id'] = $t;
                $tempData[$key]['user_id'] = $user->id;
                $tempData[$key]['subject'] = 'subject';
                $tempData[$key]['body'] = 'body';
                $tempData[$key]['lang'] = $value;
                $tempData[$key]['lang_id'] = $key + 1;
            }
            DB::table('email_temp_details')->insert($tempData);
        }
        $tempData       = [];
        $preferencedata = [
            'row_per_page' => 10,
            'date_format' => 1,
            'date_sepa' => '-',
            'percentage' => 0,
            'quantity' => 0,
            'date_format_type' => 'dd/mm/yyyy',
        ];
        $i=0;
        foreach ($preferencedata as $key=>$value){
            $tempData[$i]['user_id'] = $user->id;
            $tempData[$i]['category'] = 'preference';
            $tempData[$i]['field'] = $key;
            $tempData[$i]['value'] = $value;
            $i++;
        }
        DB::table('preference')->insert($tempData);

        \Session::flash('success',trans('message.success.added_success'));
        return redirect()->intended('login');
        //}
    }

}
