<?php

namespace App\Http\Controllers;

use App\Model\ItemUnit;
use App\Models\Event;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Session;
use App\Http\Start\Helpers;

class EventController extends Controller
{
    public function __construct(){
    /**
     * Set the database connection. reference app\helper.php
     */    
        //selectDatabase();
    }
    /**
     * Display a listing of the Item Units.
     *
     * @return Unit list page view
     */
    public function index()
    {
        $data['menu'] = 'setting';
        $data['sub_menu'] = 'general';
        $data['list_menu'] = 'event';
        $data['eventData'] = Event::where('user_id', auth()->user()->id)->get();
        foreach ($data['eventData'] as $event){
            $event->start_time = date('Y-m-d', strtotime($event->start_time));
            $event->end_time = date('Y-m-d', strtotime($event->end_time));
        }
        return view('admin.event.event_list', $data);
    }


    /**
     * Show the form for creating a new Item Unit.
     *
     * @return Unit create page view
     */
    public function create()
    {
        $data['menu'] = 'setting';
        $data['sub_menu'] = 'unit';
        $data['header'] = 'unit';
        $data['breadcrumb'] = 'addUnit';
        return view('admin.unit.unit_add', $data);
    }

    /**
     * Store a newly created Item Unit in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return redirect Unit list page view
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:2',
            'start_time' => 'required',
            'end_time' => 'required',
        ]);
        $data = $request->all();
        $data['user_id'] = auth()->user()->id;
        $data['full_day_event'] = 1;
        $data['start_time'] = DbDateFormat($data['start_time']);
        $data['end_time'] = DbDateFormat($data['end_time']);

        $event = Event::create($data);

        if ($event) {
            \Session::flash('success',trans('message.success.save_success'));
            return redirect()->intended('event');
        } else {
            return back()->withInput()->withErrors(['email' => "Invalid Request !"]);
        }
    }

    /**
     * Display the specified Item Unit.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified Item Unit.
     *
     * @param  int  $id
     * @return Unit edit page view
     */
    public function edit()
    {

        $id = $_POST['id'];
        $EventData = Event::where('id', $id)->first();
        
        $return_arr['name'] = $EventData->name;
        $return_arr['start_time'] = $EventData->start_time;
        $return_arr['end_time'] = $EventData->end_time;
        $return_arr['id'] = $EventData->id;

        echo json_encode($return_arr);
    }

    /**
     * Update the specified Item Unit in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return redirect Unit list page view
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:2',
            'start_time' => 'required',
            'end_time' => 'required',
        ]);
        $data['name'] = $request->name;
        $data['start_time'] = DbDateFormat($request->start_time);
        $data['end_time'] = DbDateFormat($request->end_time);

        Event::where('id', $request->id)->update($data);
        \Session::flash('success',trans('message.success.update_success'));
            return redirect()->intended('event');
    }

    /**
     * Remove the specified Item Unit from storage.
     *
     * @param  int  $id
     * @return Unit list page view
     */
    public function destroy($id)
    {
        if (isset($id)) {
            Event::where('id', '=', $id)->delete();
            \Session::flash('success',trans('message.success.delete_success'));
            return redirect()->intended('event');
        }
    }
}
