<?php

namespace App\Http\Controllers;

use App\Http\Controllers\EmailController;
use App\Model\Customer;
use App\Model\PaymentMethod;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Model\Payment;
use App\Model\Shipment;
use Validator;
use DB;
use Session;
use Auth;
use PDF;
use App\Http\Start\Helpers;

class PaymentController extends Controller
{

    public function __construct(Auth $auth, Payment $payment,Shipment $shipment,EmailController $email){
          $this->auth = $auth::user(); 
          $this->payment = $payment; 
          $this->shipment = $shipment;
          $this->email = $email;
    }

   /**
    * Payment list
    */
    public function index(){
        $data['menu'] = 'sales';
        $data['sub_menu'] = 'payment/list';
       $data['paymentList'] = DB::table('payment_history')
                             ->leftjoin('debtors_master','debtors_master.debtor_no','=','payment_history.customer_id')
                             ->leftjoin('payment_terms','payment_terms.id','=','payment_history.payment_type_id')
                             ->leftjoin('sales_orders','sales_orders.reference','=','payment_history.invoice_reference')
                            ->where(['debtors_master.deleted_at'=>null,'debtors_master.user_id'=>auth()->user()->id])
                            ->where(['sales_orders.deleted'=>0,'sales_orders.user_id'=>auth()->user()->id])
                             ->select('payment_history.*','debtors_master.name','payment_terms.name as pay_type','sales_orders.order_no as invoice_id','sales_orders.order_reference_id as order_id')
                             ->orderBy('payment_history.payment_date','DESC')
                             ->get();

        $data['accounts'] = DB::table('bank_accounts')->where(['deleted'=>0,'user_id'=>auth()->user()->id])->pluck('account_name','id');
        $data['incomeCategories'] = DB::table('income_expense_categories')
                                    ->where('type','income')
                                    ->where(['deleted_at'=>null,'user_id'=>auth()->user()->id])
                                    ->pluck('name','id');

        return view('admin.payment.paymentList', $data);     
    }

   /**
    * Payment list
    */
    public function paymentFiltering(){
        $data['menu'] = 'sales';
        $data['sub_menu'] = 'payment/list';
        $data['customer'] = $customer = isset($_GET['customer']) ? $_GET['customer'] : NULL;
        $data['method'] = $method = isset($_GET['method']) ? $_GET['method'] : NULL;
        
        $data['customerList'] = Customer::where('user_id',auth()->user()->id)->select('debtor_no','name')->where(['inactive'=>0])->get();
        $data['methodList'] = PaymentMethod::where('user_id',auth()->user()->id)->select('id','name')->get();

        $fromDate = DB::table('payment_history')->select('payment_date')->orderBy('payment_date','asc')->first();
        
        if(isset($_GET['from'])){
            $data['from'] = $from = $_GET['from'];
        }else{
           $data['from'] = $from = isset($fromDate->payment_date) ? formatDate(date("d-m-Y", strtotime($fromDate->payment_date))) : date('d-m-Y'); 
        }
        
        if(isset($_GET['to'])){
            $data['to'] = $to = $_GET['to'];
        }else{
            $data['to'] = $to = formatDate(date('d-m-Y'));
        }

        $data['paymentList'] = $this->payment->paymentFilter($from, $to, $customer, $method);
        return view('admin.payment.filterPaymentList', $data);     
    }

    /**
     * Create new payment.
     *
     */
    public function createPayment(Request $request)
    {
        $this->validate($request, [
            'account_no'=>'required',
            'payment_type_id' => 'required',
            'category_id'=>'required',
            'amount' => 'required|numeric',
            'payment_date'=>'required',
            'description' =>'required'
        ]);

        // Transaction Table
        $data['account_no'] = $request->account_no;
        $data['trans_date'] = DbDateFormat($request->payment_date);
        $data['description'] = $request->description;
        $data['amount'] = abs($request->amount);
        $data['category_id'] = $request->category_id;
        $data['reference'] = $request->reference;
        $data['person_id'] = $this->auth->id;
        $data['trans_type'] = 'cash-in-by-sale';
        $data['payment_method'] = $request->payment_type_id;
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['user_id'] = auth()->user()->id;
        $data['check_no'] = ($request->check_no) ? $request->check_no : '';
        $data['ref_no'] = $request->invoice_reference;
        $transactionId = DB::table('bank_trans')->insertGetId($data);

        // Payment Table
        $payment['transaction_id'] = $transactionId;
        $payment['invoice_reference'] = $request->invoice_reference;
        $payment['order_reference'] = $request->order_reference;
        $payment['payment_type_id'] = $request->payment_type_id;
        $payment['amount'] =  abs($request->amount);
        $payment['payment_date'] = DbDateFormat($request->payment_date);
        $payment['reference'] =  $request->reference; 
        $payment['person_id'] = $this->auth->id;
        $payment['customer_id'] = $request->customer_id; 
        
        $orderNo = $request->order_no;
        $invoiceNo = $request->invoice_no; 
        $payment = DB::table('payment_history')->insertGetId($payment);
        
        if (!empty($payment)) {
            $paidAmount = $this->payment->updatePayment($request->invoice_reference,$request->amount);
            \Session::flash('success',trans('message.success.save_success'));
            return redirect()->intended('invoice/view-detail-invoice/'.$orderNo.'/'.$invoiceNo);
        }
    }

    /**
    * Delete payment by id 
    **/
    public function delete(Request $request){
        $id = $request['id'];
        $paymentInfo = DB::table('payment_history')
                     ->where('id',$id)
                     ->select('id','order_reference','invoice_reference','amount','transaction_id')
                     ->first();
        //d($paymentInfo,1);
        $totalPaidAmount = DB::table('sales_orders')
                     ->where(['order_reference'=>$paymentInfo->order_reference,'reference'=>$paymentInfo->invoice_reference])
                     ->sum('paid_amount');
        $newAmount   = ($totalPaidAmount-$paymentInfo->amount);
        $update      = DB::table('sales_orders')
                     ->where(['order_reference'=>$paymentInfo->order_reference,'reference'=>$paymentInfo->invoice_reference])
                     ->update(['paid_amount'=>$newAmount]);

        DB::table('payment_history')->where('id',$id)->delete();
        
        if($paymentInfo->transaction_id){
            DB::table('bank_trans')->where('id',$paymentInfo->transaction_id)->update(['deleted'=>1]);
        }
        \Session::flash('success',trans('message.success.save_success'));
        return redirect()->intended('payment/list');
    }

    /**
    * Display receipt of payment
    */

    public function viewReceipt($id){
        $data['menu'] = 'sales';
        $data['sub_menu'] = 'payment/list';        
        $data['paymentInfo'] = DB::table('payment_history')
                     ->leftjoin('payment_terms','payment_terms.id','=','payment_history.payment_type_id')
                     ->leftjoin('sales_orders','sales_orders.reference','=','payment_history.invoice_reference')
                     ->leftjoin('debtors_master','debtors_master.debtor_no','=','payment_history.customer_id')
                     ->leftjoin('cust_branch','cust_branch.branch_code','=','sales_orders.branch_id')
                     ->leftjoin('countries','countries.id','=','cust_branch.billing_country_id')
                     ->where('payment_history.id',$id)
                     ->select('payment_history.*','payment_terms.name as payment_method','cust_branch.br_name','cust_branch.br_address','cust_branch.billing_street','cust_branch.billing_city','cust_branch.billing_state','cust_branch.billing_zip_code','cust_branch.billing_country_id','sales_orders.ord_date as invoice_date','sales_orders.total as invoice_amount','sales_orders.order_reference_id','countries.country','debtors_master.email','debtors_master.phone','debtors_master.name')      
                     ->first();
      
        //Right part start
        $data['paymentsList'] = DB::table('payment_history')
                            ->where(['order_reference'=>$data['paymentInfo']->order_reference])
                            ->leftjoin('payment_terms','payment_terms.id','=','payment_history.payment_type_id')
                            ->select('payment_history.*','payment_terms.name')
                            ->orderBy('payment_date','DESC')
                            ->get();

        $data['orderInfo']  = DB::table('sales_orders')->where('order_no',$data['paymentInfo']->order_reference_id)->select('reference','order_no','invoice_type')->first();
         //Right part end
       $lang = Session::get('dflt_lang');
        $data['emailInfo'] = DB::table('email_temp_details')->where(['temp_id'=>1,'lang'=>$lang])->select('subject','body')->first();
       $data['invoiceType'] = '';
       //$data['invoiced_status'] = 'yes';
       //d($data['orderInfo'],1);
        return view('admin.payment.viewReceipt', $data); 
    }

    /**
    * Create receipt of payment
    */

    public function createReceiptPdf($id){        
        $data['paymentInfo'] = DB::table('payment_history')
                     ->leftjoin('payment_terms','payment_terms.id','=','payment_history.payment_type_id')
                     ->leftjoin('sales_orders','sales_orders.reference','=','payment_history.invoice_reference')
                     ->leftjoin('debtors_master','debtors_master.debtor_no','=','payment_history.customer_id')
                     ->leftjoin('cust_branch','cust_branch.branch_code','=','sales_orders.branch_id')
                     ->leftjoin('countries','countries.id','=','cust_branch.billing_country_id')
                     ->where('payment_history.id',$id)
                     ->select('payment_history.*','payment_terms.name as payment_method','cust_branch.br_name','cust_branch.br_address','cust_branch.billing_street','cust_branch.billing_city','cust_branch.billing_state','cust_branch.billing_zip_code','cust_branch.billing_country_id','sales_orders.ord_date as invoice_date','sales_orders.total as invoice_amount','sales_orders.order_reference_id','countries.country','debtors_master.email','debtors_master.phone','debtors_master.name')      
                     ->first();
        $pref                      = DB::table('preference')->where('user_id', auth()->user()->id)->where('category', 'company')->get();
        $companyData               = AssColumn($a=$pref, $column='id');
        $array_data = array();
        foreach($companyData as $row)
        {
          $array_data[$row->category][$row->field] = $row->value;         
        }
        $data['companyInfo'] = $array_data;

        //return view('admin.payment.paymentReceiptPdf', $data);  
        $pdf = PDF::loadView('admin.payment.paymentReceiptPdf', $data);
        $pdf->setPaper(array(0,0,750,1060), 'portrait');
        
       return $pdf->download('payment_'.time().'.pdf',array("Attachment"=>0));

    }

    /**
    * Print receipt of payment
    */

    public function printReceipt($id){        

        $data['paymentInfo'] = DB::table('payment_history')
                     ->leftjoin('payment_terms','payment_terms.id','=','payment_history.payment_type_id')
                     ->leftjoin('sales_orders','sales_orders.reference','=','payment_history.invoice_reference')
                     ->leftjoin('debtors_master','debtors_master.debtor_no','=','payment_history.customer_id')
                     ->leftjoin('cust_branch','cust_branch.branch_code','=','sales_orders.branch_id')
                     ->leftjoin('countries','countries.id','=','cust_branch.billing_country_id')
                     ->where('payment_history.id',$id)
                     ->select('payment_history.*','payment_terms.name as payment_method','cust_branch.br_name','cust_branch.br_address','cust_branch.billing_street','cust_branch.billing_city','cust_branch.billing_state','cust_branch.billing_zip_code','cust_branch.billing_country_id','sales_orders.ord_date as invoice_date','sales_orders.total as invoice_amount','sales_orders.order_reference_id','countries.country','debtors_master.email','debtors_master.phone','debtors_master.name')      
                     ->first();  
        $pref                      = DB::table('preference')->where('user_id', auth()->user()->id)->where('category', 'company')->get();
        $companyData               = AssColumn($a=$pref, $column='id');
        $array_data = array();
        foreach($companyData as $row)
        {
          $array_data[$row->category][$row->field] = $row->value;         
        }
        $data['companyInfo'] = $array_data;                  
       
        //return view('admin.payment.printReceipt', $data); 
        $pdf = PDF::loadView('admin.payment.printReceipt', $data);
        $pdf->setPaper(array(0,0,750,1060), 'portrait');
        
        return $pdf->stream('payment_'.time().'.pdf',array("Attachment"=>0));

    }

    /**
    * Send email to customer for payment information
    */
    public function sendPaymentInformationByEmail(Request $request){
        $this->email->sendEmail($request['email'],$request['subject'],$request['message']);
        \Session::flash('success',trans('message.email.email_send_success'));
        return redirect()->intended('payment/view-receipt/'.$request['id']);
    }

    /**
    * Pay all amount
    */
    public function payAllAmount($order_no){
        $allInvoiced = DB::table('sales_orders')->where('order_reference_id',$order_no)->select('order_no as inv_no','order_reference','reference','debtor_no as customer_id','payment_id','total as invoiced_amount','paid_amount')->get();
        //d($allInvoiced,1);
        foreach ($allInvoiced as $key => $value) {
            $amount = ($value->invoiced_amount - $value->paid_amount);
           // d($amount,1);
            DB::table('sales_orders')->where('order_no',$value->inv_no)->update(['paid_amount'=>$value->invoiced_amount]);
            if(abs($amount) >= 0){
            $payment[$key]['invoice_reference'] = (string)$value->reference;
            $payment[$key]['order_reference'] = (string)$value->order_reference;
            $payment[$key]['payment_type_id'] = $value->payment_id;
            $payment[$key]['amount'] = $amount ;
            $payment[$key]['payment_date'] = DbDateFormat(date('d-m-Y'));
            $payment[$key]['reference'] =  'by all pay'; 
            $payment[$key]['person_id'] = $this->auth->id; 
            $payment[$key]['customer_id'] = $value->customer_id;
            //d($payment,1);
            $payments = DB::table('payment_history')->insertGetId($payment[$key]);
        }
    }
    \Session::flash('success',trans('message.extra_text.payment_success'));
    return redirect()->intended('order/view-order-details/'.$order_no);
    }

    public function editPayment(Request $request){
        $data = [];
        $payment_id = $request->pid;
        $transaction_id = $request->trid;
        $paymentInfo = DB::table('payment_history as ph')
                      ->leftjoin('bank_trans as bt','bt.id','=','ph.transaction_id')
                       ->select("ph.*",'bt.account_no','bt.category_id','bt.attachment')
                      ->where(['ph.id'=>$payment_id])
                      ->first();
        //d($paymentInfo,1);
        $data['pid'] = $paymentInfo->id;
        $data['trid'] = $paymentInfo->transaction_id;
        $data['amount'] = $paymentInfo->amount;
        $data['account_no'] = $paymentInfo->account_no;
        $data['category_id'] = $paymentInfo->category_id;
        $data['attachment'] = $paymentInfo->attachment;
        $data['invoice_reference'] = $paymentInfo->invoice_reference;
        return json_encode($data);
    }

     public function updatePayment(Request $request){
        
        $preAmount = $request->preAmount;
        $invoiceRef = $request->invoice_ref;
        
        $totalPaidAmount = DB::table('sales_orders')
                     ->where(['reference'=>$invoiceRef])
                     ->sum('paid_amount');

        $newAmount   = ($totalPaidAmount-$preAmount)+$request->amount;

       DB::table('sales_orders')
                     ->where(['reference'=>$invoiceRef])
                     ->update(['paid_amount'=>$newAmount]);


        $payment_id = $request->pid;
        $transaction_id = $request->trid;  
        $amount = $request->amount;
        $account_no = $request->account_no;
        $category_id = $request->category_id;
        $status = $request->status;
        
       DB::table('payment_history')
                     ->where(['id'=>$payment_id])
                     ->update(['amount'=>$amount,'status'=>$status]);

        $trans['account_no'] = $account_no; 
        $trans['category_id'] = $category_id;
        $trans['amount'] = $amount;

       DB::table('bank_trans')
                     ->where(['id'=>$transaction_id])
                     ->update($trans);

     \Session::flash('success',trans('message.extra_text.payment_success'));
     return redirect()->intended('payment/list');

     }
}