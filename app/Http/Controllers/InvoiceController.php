<?php

namespace App\Http\Controllers;

use App\Http\Controllers\EmailController;
use Illuminate\Http\Request;
use App\Model\Orders;
use App\Http\Requests;
use App\Model\Sales;
use App\Model\Shipment;
use App\Model\Deposit;
use App\Http\Start\Helpers;
use DB;
use PDF;
use Session;

class InvoiceController extends Controller
{
    public function __construct(Orders $orders,Sales $sales,Shipment $shipment,EmailController $email, Deposit $deposit){

        $this->order = $orders;
        $this->sale = $sales;
        $this->shipment = $shipment;
        $this->email = $email;
        $this->deposit = $deposit;
    }

    /**
    * Preview of Invoice details
    * @params order_no, invoice_no
    **/

    public function viewInvoiceDetails($orderNo,$invoiceNo){
        $data['menu']     = 'sales';
        $data['sub_menu'] = 'sales/direct-invoice';

        $data['saleDataOrder'] = DB::table('sales_orders')
                            ->where('order_no', '=', $orderNo)
                            ->where('sales_orders.user_id', '=', auth()->user()->id)
                            ->leftJoin('location','location.loc_code','=','sales_orders.from_stk_loc')
                            ->select("sales_orders.*","location.location_name")
                            ->first();

        $data['invoiceType'] = $data['saleDataOrder']->invoice_type;
        $data['saleDataInvoice'] = DB::table('sales_orders')
                    ->where('order_no', '=', $invoiceNo)
                    ->where('sales_orders.user_id', '=', auth()->user()->id)
                    ->leftJoin('location','location.loc_code','=','sales_orders.from_stk_loc')
                    ->leftJoin('invoice_payment_terms','invoice_payment_terms.id','=','sales_orders.payment_term')
                    ->select("sales_orders.*","location.location_name",'invoice_payment_terms.days_before_due')
                    ->first();                    
        $data['invoiceData'] = $this->order->getSalseOrderByID($invoiceNo,$data['saleDataInvoice']->from_stk_loc);
        $data['customerInfo']  = DB::table('sales_orders')
                             ->where('sales_orders.order_no',$orderNo)
                            ->where('sales_orders.user_id', '=', auth()->user()->id)
                             ->leftjoin('debtors_master','debtors_master.debtor_no','=','sales_orders.debtor_no')
                             ->leftjoin('cust_branch','cust_branch.branch_code','=','sales_orders.branch_id')
                             ->leftjoin('countries','countries.id','=','cust_branch.shipping_country_id')
                             ->select('debtors_master.debtor_no','debtors_master.name','debtors_master.phone','debtors_master.gstin','debtors_master.email','cust_branch.br_name','cust_branch.br_address','cust_branch.billing_street','cust_branch.billing_city','cust_branch.billing_state','cust_branch.billing_zip_code','countries.country','cust_branch.billing_country_id')                            
                             ->first();

        $pref                      = DB::table('preference')->where('user_id', auth()->user()->id)->where('category', 'company')->get();
        $companyData               = AssColumn($a=$pref, $column='id');
        $array_data = array();
        foreach($companyData as $row)
        {
          $array_data[$row->category][$row->field] = $row->value;         
        }
        $data['companyInfo'] = $array_data;
        $data['orderInfo']  = DB::table('sales_orders')->where('user_id', '=', auth()->user()->id)->where('order_no',$orderNo)->select('reference','order_no')->first();

        $data['payments']   = DB::table('payment_terms')->where('deleted_at', '=', null )->where('user_id', '=', auth()->user()->id)->get();
        $data['paymentsList'] = DB::table('payment_history')
                                ->where(['order_reference'=>$data['orderInfo']->reference])
                                ->leftjoin('payment_terms','payment_terms.id','=','payment_history.payment_type_id')
                                ->select('payment_history.*','payment_terms.name')
                                ->orderBy('payment_date','DESC')
                                ->get();

        $data['invoice_no'] = $invoiceNo;
        
        $data['invoiced_status'] = 'yes';
        $data['invoiced_date'] = $data['saleDataInvoice']->ord_date;

        $lang = Session::get('dflt_lang');
        $data['emailInfo'] = DB::table('email_temp_details')->where('user_id', '=', auth()->user()->id)->where(['temp_id'=>4,'lang'=>$lang])->select('subject','body')->first();
        $data['due_date']  = formatDate(date('Y-m-d', strtotime("+".$data['saleDataInvoice']->days_before_due."days")));
        
        $data['accounts'] = DB::table('bank_accounts')->where(['deleted'=>0,'user_id'=>auth()->user()->id])->where('deleted', '=', 0)->pluck('account_name','id');
        $data['incomeCategories'] = DB::table('income_expense_categories')
                                    ->where('user_id', '=', auth()->user()->id)
                                    ->where('type','income')
                                    ->orWhere('type','no')
                                    ->pluck('name','id');

        return view('admin.invoice.viewInvoiceDetails', $data);
    }
    
    /**
    * Send email to customer for Invoice information
    */
    public function sendInvoiceInformationByEmail(Request $request){

        $orderNo      = $request['order_id'];
        $invoiceNo    = $request['invoice_id'];
        $invoiceName  = 'invoice_'.time().'.pdf';
        
        if(isset($request['invoice_pdf']) && $request['invoice_pdf']=='on'){
            $this->invoicePdfEmail($orderNo,$invoiceNo,$invoiceName);
            $this->email->sendEmailWithAttachment($request['email'],$request['subject'],$request['message'],$invoiceName);
         }else{
            $this->email->sendEmail($request['email'],$request['subject'],$request['message']);
         }   
        Session::flash('success',trans('message.email.email_send_success'));
        return redirect()->intended('invoice/view-detail-invoice/'.$orderNo.'/'.$invoiceNo);

    }
    
    /**
    * Invoice print
    */
    public function invoicePrint($orderNo,$invoiceNo){

        $data['saleDataInvoice'] = DB::table('sales_orders')
                    ->where('order_no', '=', $invoiceNo)
                    ->leftJoin('location','location.loc_code','=','sales_orders.from_stk_loc')
                    ->leftJoin('invoice_payment_terms','invoice_payment_terms.id','=','sales_orders.payment_term')
                    ->leftJoin('bank_trans','bank_trans.ref_no','=','sales_orders.reference')
                    ->select("sales_orders.*","location.location_name",'invoice_payment_terms.days_before_due','bank_trans.check_no')
                    ->first(); 

        $data['invoiceData'] = $this->order->getSalseOrderByID($invoiceNo,$data['saleDataInvoice']->from_stk_loc);

        $data['customerInfo']  = DB::table('sales_orders')
                             ->where('sales_orders.order_no',$orderNo)
                             ->leftjoin('debtors_master','debtors_master.debtor_no','=','sales_orders.debtor_no')
                             ->leftjoin('cust_branch','cust_branch.branch_code','=','sales_orders.branch_id')
                             ->leftjoin('countries','countries.id','=','cust_branch.shipping_country_id')
                             ->select('debtors_master.debtor_no','debtors_master.name','debtors_master.phone','debtors_master.gstin','debtors_master.email','cust_branch.br_name','cust_branch.br_address','cust_branch.billing_street','cust_branch.billing_city','cust_branch.billing_state','cust_branch.billing_zip_code','countries.country','cust_branch.billing_country_id')                            
                             ->first();

        $pref                  = DB::table('preference')->where('user_id', auth()->user()->id)->where('category', 'company')->get();
        $companyData            = AssColumn($a=$pref, $column='id');
        $array_data = array();
        foreach($companyData as $row)
        {
          $array_data[$row->category][$row->field] = $row->value;         
        }
        $data['companyInfo'] = $array_data;

        $data['orderInfo']   = DB::table('sales_orders')->where('order_no',$orderNo)->select('reference','order_no')->first();
        $data['due_date']    = formatDate(date('Y-m-d', strtotime("+".$data['saleDataInvoice']->days_before_due."days")));
        //return view('admin.invoice.invoicePdf', $data);
        $pdf = PDF::loadView('admin.invoice.invoicePrint', $data);
        $pdf->setPaper(array(0,0,750,1060), 'portrait');
        return $pdf->stream('invoice_'.time().'.pdf',array("Attachment"=>0));        
    }

    /**
    * Generate pdf for invoice
    */
    public function invoicePdf($orderNo,$invoiceNo){

        $data['taxInfo'] = $this->sale->calculateTaxRow($invoiceNo);
        $data['saleDataInvoice'] = DB::table('sales_orders')
                    ->where('order_no', '=', $invoiceNo)
                    ->leftJoin('location','location.loc_code','=','sales_orders.from_stk_loc')
                    ->leftJoin('invoice_payment_terms','invoice_payment_terms.id','=','sales_orders.payment_term')
                    ->leftJoin('bank_trans','bank_trans.ref_no','=','sales_orders.reference')
                    ->select("sales_orders.*","location.location_name",'invoice_payment_terms.days_before_due','bank_trans.check_no')
                    ->first();                    
        $data['invoiceData'] = $this->order->getSalseOrderByID($invoiceNo,$data['saleDataInvoice']->from_stk_loc);
        $data['customerInfo']  = DB::table('sales_orders')
                             ->where('sales_orders.order_no',$orderNo)
                             ->leftjoin('debtors_master','debtors_master.debtor_no','=','sales_orders.debtor_no')
                             ->leftjoin('cust_branch','cust_branch.branch_code','=','sales_orders.branch_id')
                             ->leftjoin('countries','countries.id','=','cust_branch.shipping_country_id')
                             ->select('debtors_master.debtor_no','debtors_master.name','debtors_master.phone','debtors_master.gstin','debtors_master.email','cust_branch.br_name','cust_branch.br_address','cust_branch.billing_street','cust_branch.billing_city','cust_branch.billing_state','cust_branch.billing_zip_code','countries.country','cust_branch.billing_country_id')                            
                             ->first();
        $pref                  = DB::table('preference')->where('user_id', auth()->user()->id)->where('category', 'company')->get();
        $companyData           = AssColumn($a=$pref, $column='id');
        $array_data = array();
        foreach($companyData as $row)
        {
          $array_data[$row->category][$row->field] = $row->value;         
        }
        $data['companyInfo'] = $array_data;        
        $data['orderInfo']   = DB::table('sales_orders')->where('order_no',$orderNo)->select('reference','order_no')->first();
        $data['due_date']    = formatDate(date('Y-m-d', strtotime("+".$data['saleDataInvoice']->days_before_due."days")));
        
        $pdf = PDF::loadView('admin.invoice.invoicePdf', $data);
        $pdf->setPaper(array(0,0,750,1060), 'portrait');
        return $pdf->download('invoice_'.time().'.pdf',array("Attachment"=>0));        
    }

    public function destroy($id)
    {
        
        if(isset($id)) {
            $record = \DB::table('sales_orders')->where('order_no', $id)->first();
            if($record) {
                
                $invoice_id = $id;
                $order_id = $record->order_reference_id;
                $invoice_reference = $record->reference;
                $order_reference = $record->order_reference;

                DB::table('sales_orders')->where('order_no', '=', $invoice_id)->delete();
                DB::table('sales_order_details')->where('order_no', '=', $invoice_id)->delete();
                DB::table('stock_moves')->where('reference', '=', 'store_out_'.$invoice_id)->delete();
                DB::table('payment_history')->where('invoice_reference', '=', $invoice_reference)->delete();

                \Session::flash('success',trans('message.success.delete_success'));
                return redirect()->intended('sales/list');
            }
        }
    }
    public function invoicePdfEmail($orderNo,$invoiceNo,$invoiceName){

        $data['saleDataInvoice'] = DB::table('sales_orders')
                    ->where('order_no', '=', $invoiceNo)
                    ->leftJoin('location','location.loc_code','=','sales_orders.from_stk_loc')
                    ->leftJoin('invoice_payment_terms','invoice_payment_terms.id','=','sales_orders.payment_term')
                    ->select("sales_orders.*","location.location_name",'invoice_payment_terms.days_before_due')
                    ->first();                    
        $data['invoiceData'] = $this->order->getSalseOrderByID($invoiceNo,$data['saleDataInvoice']->from_stk_loc);

        $data['customerInfo']  = DB::table('sales_orders')
                             ->where('sales_orders.order_no',$orderNo)
                             ->leftjoin('debtors_master','debtors_master.debtor_no','=','sales_orders.debtor_no')
                             ->leftjoin('cust_branch','cust_branch.branch_code','=','sales_orders.branch_id')
                             ->leftjoin('countries','countries.id','=','cust_branch.shipping_country_id')
                             ->select('debtors_master.debtor_no','debtors_master.name','debtors_master.phone','debtors_master.gstin','debtors_master.email','cust_branch.br_name','cust_branch.br_address','cust_branch.billing_street','cust_branch.billing_city','cust_branch.billing_state','cust_branch.billing_zip_code','countries.country','cust_branch.billing_country_id')                            
                             ->first();
        $pref                  = DB::table('preference')->where('user_id', auth()->user()->id)->where('category', 'company')->get();
        $companyData           = AssColumn($a=$pref, $column='id');
        $array_data = array();
        foreach($companyData as $row)
        {
          $array_data[$row->category][$row->field] = $row->value;         
        }
        $data['companyInfo'] = $array_data; 
                
        $data['orderInfo']  = DB::table('sales_orders')->where('order_no',$orderNo)->select('reference','order_no')->first();
        $data['due_date']  = formatDate(date('Y-m-d', strtotime("+".$data['saleDataInvoice']->days_before_due."days")));
        $pdf = PDF::loadView('admin.invoice.invoicePdf', $data);
        $pdf->setPaper(array(0,0,750,1060), 'portrait');
        return $pdf->save(public_path().'/uploads/invoices/'.$invoiceName);        
    }

}
