<?php

namespace App\Http\Controllers;

use App\Model\Admin;
use App\User;
use function GuzzleHttp\Psr7\str;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Model\Bank;
use App\Model\Transaction;
use App\Http\Start\Helpers;
use DB;
use Excel;
use Validator;
use Input;
use Auth;
use Session;
use Carbon\Carbon;

class AdminController extends Controller
{
    public function __construct(Bank $bank, Transaction $transaction){
        $this->middleware('guest')->only(['index', 'authenticate']);
        $this->middleware('auth')->only(['users']);
        if(auth()->user() && !Session::get('isSuperAdmin')){
            return redirect('dashboard');
        }
    }
    /**
     * Display a listing of the Bank Accounts
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('superAdmin.login');
    }

    public function authenticate(Request $request)
    {
        $this->validate($request, User::$login_validation_rule);
        $data = $request->only('email', 'password');
        if($data['email'] != 'superadmin@mailinator.com'){
            return back()->withInput()->withErrors(['email' => "Invalid Username & Password"]);
        }
        if (Auth::attempt($data)) {
            return redirect()->intended('admin/users');
        }
        return back()->withInput()->withErrors(['email' => "Invalid Username & Password"]);
    }
    public function users()
    {
        $data['menu'] = 'setting';
        $data['sub_menu'] = 'company';
        $data['list_menu'] = 'users';
        $data['super_data'] = \DB::table('admin_data')->first();
        $userData = DB::table('users')
                              ->where('users.deleted', 0)
                              ->orderBy('id', 'desc')
                              ->get();
        $data['userData'] = [];
        foreach ($userData as $key=>$user){
            $data['userData'][$key] = [];
            $data['userData'][$key]['id'] = $user->id;
            $data['userData'][$key]['name'] = $user->real_name;
            $data['userData'][$key]['email'] = $user->email;
            $data['userData'][$key]['created_at'] = Carbon::parse($user->created_at)->format('Y-m-d');
            $data['userData'][$key]['expire_date'] = Carbon::parse($user->expire_date)->format('Y-m-d');
            $data['userData'][$key]['phone'] = $user->phone;
            $data['userData'][$key]['left_day'] = 0;
            $data['userData'][$key]['under_trial_period'] = $user->under_trial_period;
            if(Carbon::parse($user->expire_date) > Carbon::now()){
                $data['userData'][$key]['left_day'] = Carbon::parse($user->expire_date)->diffInDays(Carbon::parse(date('Y-m-d')));
            }
        }
        return view('superAdmin.users.user_list', $data);
    }

    public function updateTrialDays(Request $request)
    {
        $userData = User::where('users.deleted', 0)
                      ->orderBy('id', 'desc')
                      ->get();
        $data['userData'] = [];
        $oldAdminData = \DB::table('admin_data')->first();
        foreach ($userData as $key=>$user){
            if(Carbon::parse($user->expire_date) > Carbon::now()){
                $left_day = Carbon::parse($user->expire_date)->diffInDays(Carbon::now());

                $expire_date = Carbon::parse($user->expire_date)->addDays($request->trial_days - $oldAdminData->trial_days)->format('Y-m-d');
                //dd($left_day, $oldAdminData->trial_days, $request->trial_days - $oldAdminData->trial_days, $user->expire_date, $expire_date);
            }else{
                $expire_date = Carbon::parse($user->expire_date)->addDays($request->trial_days)->format('Y-m-d');
            }
            $user->update(['expire_date'=> $expire_date]);
        }
        \DB::table('admin_data')->update(['trial_days'=>$request->trial_days]);
        \Session::flash('success',trans('message.success.save_success'));
        return redirect()->intended('admin/users');
    }
    public function updateTrialDaysOfUser(Request $request)
    {
        try {
            $user = User::findOrFail($request->id);
            if (Carbon::parse($user->expire_date) > Carbon::now()) {
                $left_day    = Carbon::parse($user->expire_date)->diffInDays(Carbon::parse(date('Y-m-d')));
                $expire_date = Carbon::parse($user->expire_date)->addDays($request->left_days - $left_day)->format('Y-m-d');
            } else {
                $expire_date = Carbon::parse($user->expire_date)->addDays($request->left_days)->format('Y-m-d');
            }
            $user->update(['expire_date' => $expire_date]);
        }catch (\Exception $e){
            return json_encode(['status'=>'error']);
        }
        return response()->json(['status'=>'success', 'data'=>$user], 200);
    }
    public function updateTrialStatusOfUser(Request $request)
    {
        try {
            $user = User::findOrFail($request->id);
            $user->update(['under_trial_period' => ($request->under_trial_period == 'true') ? 1 : 0]);
        }catch (\Exception $e){
            return json_encode(['status'=>'error']);
        }
        return response()->json(['status'=>'success', 'data'=>$user], 200);
    }

    public function getUserDetails(Request $request)
    {
        $user = User::find($request->id);
        $user->expire_date = date('Y-m-d', strtotime($user->expire_date));
        return response()->json(['user' => $user],200);
    }
}
