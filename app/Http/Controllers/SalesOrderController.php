<?php

namespace App\Http\Controllers;

use App\Model\Customer;
use App\Model\Location;
use App\Model\PaymentMethod;
use App\Model\PaymentTerm;
use App\Model\SalesOrderDetail;
use App\Model\StockMove;
use App\Model\Tax;
use Illuminate\Http\Request;
use App\Http\Controllers\EmailController;
use App\Model\Orders;
use App\Http\Requests;
use App\Model\Sales;
use App\Model\Shipment;
use DB;
use PDF;
use Session;
use App\Http\Start\Helpers;

class SalesOrderController extends Controller
{
    public function __construct(Orders $orders,Sales $sales,Shipment $shipment,EmailController $email){

        $this->order = $orders;
        $this->sale = $sales;
        $this->shipment = $shipment;
        $this->email = $email;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['menu'] = 'sales';
        $data['sub_menu'] = 'order/list';
        $data['orderData'] = $this->order->getAllSalseOrder(NULL, NULL, NULL, NULL);
        $data['comData'] = DB::table('preference')->where('user_id', auth()->user()->id)->where('category', 'company')->get();
        return view('admin.salesOrder.orderList', $data);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function orderFiltering()
    {
        $data['menu'] = 'sales';
        $data['sub_menu'] = 'order/list';

        $data['location'] = $location = isset($_GET['location']) ? $_GET['location'] : NULL;
        $data['customer'] = $customer = isset($_GET['customer']) ? $_GET['customer'] : NULL;

        $data['customerList'] = Customer::select('debtor_no','name')->where('user_id', auth()->user()->id)->where(['inactive'=>0])->get();
        $data['locationList']      = Location::select('loc_code','location_name')->where('user_id', auth()->user()->id)->get();

        $fromDate = DB::table('sales_orders')->select('ord_date')->where('user_id', auth()->user()->id)->where('trans_type',SALESORDER)->orderBy('ord_date','asc')->first();
        
        if(isset($_GET['from'])){
            $data['from'] = $from = $_GET['from'];
        }else{
           $data['from'] = $from = isset($fromDate->ord_date) ? formatDate(date("d-m-Y", strtotime($fromDate->ord_date))) : date("d-m-Y"); 
        }
        
        if(isset($_GET['to'])){
            $data['to'] = $to = $_GET['to'];
        }else{
            $data['to'] = $to = formatDate(date('d-m-Y'));
        }

        $data['orderData'] = $this->order->getAllSalseOrder($from, $to, $location, $customer);
        $data['comData'] = DB::table('preference')->where('user_id', auth()->user()->id)->where('category', 'company')->get();
        return view('admin.salesOrder.orderListFilter', $data);
    }

    /**
     * Show the form for creating a new resource.
     **/
    public function create()
    {
        $data['menu']         = 'sales';
        $data['sub_menu']     = 'order/list';
        $data['customerData'] = Customer::where('user_id', auth()->user()->id)->where(['inactive'=>0])->get();
        $data['locData']      = Location::where('user_id', auth()->user()->id)->get();
        $data['payments']      = PaymentMethod::where('user_id', auth()->user()->id)->get();

        $data['salesType'] = DB::table('sales_types')->select('sales_type','id','defaults')->get();
       // d($data['salesType'],1);
        $order_count = DB::table('sales_orders')->where('user_id', auth()->user()->id)->where('trans_type',SALESORDER)->count();

        if($order_count>0){
        $orderReference = DB::table('sales_orders')->where('user_id', auth()->user()->id)->where('trans_type',SALESORDER)->select('reference')->orderBy('order_no','DESC')->first();
        $ref = substr($orderReference->reference, strrpos($orderReference->reference, '-') + 1);
        //$ref = explode("-",$orderReference->reference);
        //$data['order_count'] = (int) $ref[1];
        $data['order_count'] = (int) $ref;
        }else{
            $data['order_count'] = 0 ;
        }

        $taxTypeList = Tax::where('user_id', auth()->user()->id)->get();
        
        $taxOptions = '';
        $selectStart = "<select class='form-control taxList' name='tax_id[]'>";
        $selectEnd = "</select>";
        
        $selectStartCustom = "<select class='form-control taxListCustom' name='tax_id_custom[]'>";
        $selectEndCustom = "</select>";

        foreach ($taxTypeList as $key => $value) {
            $taxOptions .= "<option value='".$value->id."' taxrate='".$value->tax_rate."'>".$value->name.'('.$value->tax_rate.')'."</option>";          
        }

        $data['tax_type'] = $selectStart.$taxOptions.$selectEnd;
        $data['tax_type_custom'] = $selectStartCustom.$taxOptions.$selectEndCustom;
        
        return view('admin.salesOrder.orderAdd', $data);
    }

    /**
     * Store a newly created resource in storage.
     **/
    public function store(Request $request)
    {
        $userId = \Auth::user()->id;
        $this->validate($request, [
            'reference'=>'required|unique:sales_orders',
            'from_stk_loc' => 'required',
            'ord_date' => 'required',
            'debtor_no' => 'required',
            //'branch_id' => 'required',
            'payment_id' => 'required',
            //'item_quantity' => 'required',
        ]);

        $itemQuantity = $request->item_quantity;        
        $itemIds      = $request->item_id;
        $itemDiscount = $request->discount;
        $taxIds       = $request->tax_id;
        $unitPrice    = $request->unit_price;
        $description  = $request->description;
        $stock_id     = $request->stock_id;
        $hsn          = $request->hsn;

        if(!empty($itemIds)){
        foreach ($itemQuantity as $key => $itemQty) {
            $product[$itemIds[$key]] = $itemQty;
         }
        }

        // create salesOrder 
        $salesOrder['debtor_no']    = $request->debtor_no;
        $salesOrder['branch_id']    = $request->debtor_no;
        $salesOrder['payment_id']   = $request->payment_id;
        $salesOrder['person_id']    = $userId;
        $salesOrder['reference']    = $request->reference;
        $salesOrder['comments']     = $request->comments;
        $salesOrder['trans_type']   = SALESORDER;
        $salesOrder['invoice_type'] = 'directOrder';
        $salesOrder['ord_date']     = DbDateFormat($request->ord_date);
        $salesOrder['from_stk_loc'] = $request->from_stk_loc;
        $salesOrder['payment_term'] = 2;
        $salesOrder['total']        = $request->total;
        $salesOrder['user_id']        = auth()->user()->id;
       // d($salesOrder,1);
        $salesOrder               = Sales::create($salesOrder);
        //$salesOrderId               = \DB::table('sales_orders')->insertGetId($salesOrder);

        if(count($itemIds)>0){
        for ($i=0; $i < count($itemIds); $i++) {
            foreach ($product as $key => $item) {
                
                if($itemIds[$i] == $key){
                    // create salesOrderDetail 
                    $salesOrderDetail[$i]['order_no']         = $salesOrder->id;
                    $salesOrderDetail[$i]['stock_id']         = $stock_id[$i];
                    $salesOrderDetail[$i]['description']      = $description[$i];
                    $salesOrderDetail[$i]['hsn']              = $hsn[$i];
                    $salesOrderDetail[$i]['qty_sent']         = 0;
                    $salesOrderDetail[$i]['quantity']         = $item;
                    $salesOrderDetail[$i]['trans_type']       = SALESORDER;
                    $salesOrderDetail[$i]['discount_percent'] = $itemDiscount[$i];
                    $salesOrderDetail[$i]['tax_type_id']      = (isset($taxIds[$i])) ? $taxIds[$i] : '';
                    $salesOrderDetail[$i]['unit_price']       = $unitPrice[$i];
                    $salesOrderDetail[$i]['is_inventory']     = 1;
                }
            }
        }
        }
        
        if(count($itemIds) > 0){
        for ($i=0; $i < count($salesOrderDetail); $i++) { 
            
            DB::table('sales_order_details')->insertGetId($salesOrderDetail[$i]);
        }
    }
        // Custom items
        $tax_id_custom         = $request->tax_id_custom;
        $custom_items_discount = $request->custom_items_discount;
        $custom_items_hsn      = $request->custom_items_hsn;
        $custom_items_name     = $request->custom_items_name;
        $custom_items_rate     = $request->custom_items_rate;
        $custom_items_qty      = $request->custom_items_qty;
        $custom_items_amount   = $request->custom_items_amount;
       // d($custom_items_name,1);
        if(!empty($custom_items_name)){
          
            foreach ($custom_items_name as $key=>$value) {
               $items['order_no']         = $salesOrder->id;
               $items['trans_type']       = SALESORDER;
               $items['tax_type_id']      = $tax_id_custom[$key];
               $items['discount_percent'] = $custom_items_discount[$key];
               $items['hsn']              = $custom_items_hsn[$key];
               $items['description']      = $custom_items_name[$key];
               $items['unit_price']       = $custom_items_rate[$key];
               $items['quantity']         = $custom_items_qty[$key];
               $items['is_inventory']     = 0; 

               DB::table('sales_order_details')->insert($items);              
            }
       }
        if(!empty($salesOrder)){
            \Session::flash('success',trans('message.success.save_success'));
            return redirect()->intended('order/view-order-details/'.$salesOrder->id);
        }

    }

    /**
     * Show the form for editing the specified resource.
     **/
    public function edit($orderNo)
    {
        $data['menu']         = 'sales';
        $data['sub_menu']     = 'order/list';
        $data['customerData'] = Customer::where('user_id', auth()->user()->id)->where(['inactive'=>0])->get();
        $data['locData']      = Location::where('user_id', auth()->user()->id)->get();
        $data['payments']      = PaymentMethod::where('user_id', auth()->user()->id)->get();

        $data['invoiceData']  = $this->order->getSalseInvoiceByID($orderNo);
        $data['saleData']     = DB::table('sales_orders')->where('order_no', '=', $orderNo)->first();
        $data['branchs']      = DB::table('cust_branch')->select('debtor_no','branch_code','br_name')->where('debtor_no',$data['saleData']->debtor_no)->orderBy('br_name','ASC')->get();
        $data['invoicedItem'] = DB::table('stock_moves')->where(['order_no'=>$orderNo])->lists('stock_id');
        $data['salesType']    = DB::table('sales_types')->select('sales_type','id')->get();

        //d($data['invoiceData'],1);

        $taxTypeList = Tax::where('user_id', auth()->user()->id)->get();
        $taxOptions        = '';
        $selectStart       = "<select class='form-control taxList' name='tax_id_new[]'>";
        $selectEnd         = "</select>";
        $selectStartCustom = "<select class='form-control taxListCustom' name='tax_id_custom[]'>";
        $selectEndCustom   = "</select>";        

        foreach ($taxTypeList as $key => $value) {
            $taxOptions .= "<option value='".$value->id."' taxrate='".$value->tax_rate."'>".$value->name.'('.$value->tax_rate.')'."</option>";          
        }
        $data['tax_type_new']    = $selectStart.$taxOptions.$selectEnd;
        $data['tax_types']       = $taxTypeList;
        $data['tax_type_custom'] = $selectStartCustom.$taxOptions.$selectEndCustom;

        return view('admin.salesOrder.orderEdit', $data);
    }

    /**
     * Update the specified resource in storage.
     **/
    public function update(Request $request)
    {

        $userId = \Auth::user()->id;
        $order_no = $request->order_no;
        $this->validate($request, [
            'from_stk_loc' => 'required',
            'ord_date' => 'required',
            'debtor_no' => 'required',
            //'branch_id' => 'required',
            'payment_id' => 'required'
        ]);
      
        $itemQty      = $request->item_quantity;
        $itemIds      = $request->item_id; 
        $unitPrice    = $request->unit_price;
        $taxIds       = $request->tax_id;                
        $itemDiscount = $request->discount;
        $itemPrice    = $request->item_price;
        $stock_id     = $request->stock_id;
        $description  = $request->description; 
        $itemRowIds   = $request->item_rowid;
        $hsn          = $request->hsn;
       // d($itemRowIds,1);
        // update sales_order table
        $salesOrder['ord_date']   = DbDateFormat($request->ord_date);
        $salesOrder['debtor_no']  = $request->debtor_no;
        $salesOrder['trans_type'] = SALESORDER;
        $salesOrder['branch_id']  = $request->debtor_no;
        $salesOrder['payment_id'] = $request->payment_id;
       
        $salesOrder['from_stk_loc'] = $request->from_stk_loc;
        $salesOrder['comments']     = $request->comments;
        $salesOrder['total']        = $request->total;
        $salesOrder['updated_at']   = date('Y-m-d H:i:s');
        //d($salesOrder,1);

        DB::table('sales_orders')->where('order_no', $order_no)->update($salesOrder);
        
        if(count($itemRowIds)>0) {
            
            $orderItemRowIds = DB::table('sales_order_details')->where('order_no',$order_no)->lists('id');
                // Delete items from order if no exists on updated orders
                foreach ($orderItemRowIds as $orderItemRowId) {
                  if(!in_array($orderItemRowId, $itemRowIds)){
                    DB::table('sales_order_details')->where(array('id'=>$orderItemRowId,'order_no'=>$order_no))->delete();
                  }
                }

                foreach ($itemRowIds as $key => $value) {
                        $salesOrderDetail['tax_type_id']      = $taxIds[$key];
                        $salesOrderDetail['description']      = $description[$key];
                        $salesOrderDetail['hsn']              = $hsn[$key];
                        $salesOrderDetail['unit_price']       = $unitPrice[$key];
                        $salesOrderDetail['quantity']         = $itemQty[$key];
                        $salesOrderDetail['discount_percent'] = $itemDiscount[$key];
                        DB::table('sales_order_details')->where(['id'=>$value])->update($salesOrderDetail);
                   }
        }else{
            DB::table('sales_order_details')->where('order_no',$order_no)->delete();
            DB::table('sales_orders')->where('order_no', $order_no)->delete();
        }

        if(isset($request->item_quantity_new)) 
        {
            $itemQty         = $request->item_quantity_new;
            $itemIdsNew      = $request->item_id_new; 
            $unitPriceNew    = $request->unit_price_new;
            $taxIdsNew       = $request->tax_id_new;                
            $itemDiscountNew = $request->discount_new;
            $itemPriceNew    = $request->item_price_new; 
            $descriptionNew  = $request->description_new;
            $stock_id_new    = $request->stock_id_new; 
            $hsn_new         = $request->hsn_new; 

            foreach ($itemQty as $key => $newItem) {
                $productNew[$itemIdsNew[$key]] = $newItem;
            }
            
            for ($i=0; $i < count($itemIdsNew); $i++) {
                foreach ($productNew as $key => $value) {
                    if($itemIdsNew[$i] == $key){
                       
                        // Insert new sales order detail
                        $salesOrderDetailNew[$i]['trans_type']       = SALESORDER;
                        $salesOrderDetailNew[$i]['order_no']         = $order_no;
                        $salesOrderDetailNew[$i]['stock_id']         = $stock_id_new[$i];
                        $salesOrderDetailNew[$i]['description']      = $descriptionNew[$i];
                        $salesOrderDetailNew[$i]['hsn']              = $hsn_new[$i];
                        $salesOrderDetailNew[$i]['qty_sent']         = $value;
                        $salesOrderDetailNew[$i]['quantity']         = $value;
                        $salesOrderDetailNew[$i]['discount_percent'] = $itemDiscountNew[$i];
                        $salesOrderDetailNew[$i]['tax_type_id']      = $taxIdsNew[$i];
                        $salesOrderDetailNew[$i]['unit_price']       = $itemPriceNew[$i];
                        $salesOrderDetailNew[$i]['is_inventory']     = 1;
                    }
                }
            }
           // d($salesOrderDetail,1);
            for ($i=0; $i < count($salesOrderDetailNew); $i++) { 
            
                DB::table('sales_order_details')->insertGetId($salesOrderDetailNew[$i]);
            }
        }

        // Custom items Start
        $tax_id_custom         = $request->tax_id_custom;
        $custom_items_discount = $request->custom_items_discount;
        $custom_items_hsn      = $request->custom_items_hsn;
        $custom_items_name     = $request->custom_items_name;
        $custom_items_rate     = $request->custom_items_rate;
        $custom_items_qty      = $request->custom_items_qty;
        $custom_items_amount   = $request->custom_items_amount;
        if(!empty($custom_items_name)){
            foreach ($custom_items_name as $key=>$value) {
               $items['order_no']         = $order_no;
               $items['trans_type']       = SALESORDER;
               $items['tax_type_id']      = $tax_id_custom[$key];
               $items['discount_percent'] = $custom_items_discount[$key];
               $items['description']      = $custom_items_name[$key];
               $items['unit_price']       = $custom_items_rate[$key];
               $items['quantity']         = $custom_items_qty[$key];
               $items['hsn']              = $custom_items_hsn[$key];
               $items['is_inventory']     = 0;
               DB::table('sales_order_details')->insert($items);              
            }
       }
       // Custom items End


        \Session::flash('success',trans('message.success.save_success'));
         return redirect()->intended('order/view-order-details/'.$order_no);
    }

    /**
     * Remove the specified resource from storage.
     **/
    public function destroy($id)
    {
        if(isset($id)) {
            $record = \DB::table('sales_orders')->where('order_no', $id)->first();
            if($record) {
                // Delete shipment information
                DB::table('shipment')->where('order_no', '=', $record->order_no)->delete();
                DB::table('shipment_details')->where('order_no', '=', $record->order_no)->delete();

                 // Delete Payment information
                DB::table('payment_history')->where('order_reference', '=', $record->reference)->delete();                

                // Delete invoice information
                $invoice = \DB::table('sales_orders')->where('order_reference_id', $record->order_no)->first();
                
               // d($invoice,1);
                Sales::where('order_reference_id', '=', $record->order_no)->delete();
                if(!empty($invoice)){
                SalesOrderDetail::where('order_no', '=', $invoice->order_no)->delete();
                }
                // Delete order information
                Sales::where('order_no', '=', $record->order_no)->delete();
                SalesOrderDetail::where('order_no', '=', $record->order_no)->delete();

                 // Delete Stock information
                StockMove::where('order_no', '=', $record->order_no)->delete();

                \Session::flash('success',trans('message.success.delete_success'));
                return redirect()->intended('order/list');
            }
        }
    }

    public function search(Request $request)
    {
           
            $data              = array();
            $data['status_no'] = 0;
            $data['message']   ='No Item Found!';
            $data['items']     = array();

            $item = DB::table('stock_master')
                ->where('stock_master.user_id',auth()->user()->id)
            ->where(['stock_master.inactive'=>0,'stock_master.deleted_status'=>0])
            ->where(function ($q1) use($request){
                $q1->where('stock_master.description','LIKE','%'.$request->search.'%')
                   ->orWhere('stock_master.stock_id','LIKE','%'.$request->search.'%');
            })
            ->leftJoin('item_tax_types','item_tax_types.id','=','stock_master.tax_type_id')
            ->leftJoin('item_code','stock_master.stock_id','=','item_code.stock_id')
            ->where('item_code.user_id',auth()->user()->id)
            ->select('stock_master.*','item_tax_types.tax_rate','item_tax_types.id as tax_id','item_code.id')
            ->get();
            if(!empty($item)){
                
                $data['status_no'] = 1;
                $data['message']   ='Item Found';

                $i = 0;
                foreach ($item as $key => $value) {
                    $itemPriceValue = DB::table('sale_prices')->where(['stock_id'=>$value->stock_id,'sales_type_id'=>$request['salesTypeId']])->select('price')->first();
                    if(!isset($itemPriceValue)){
                        $itemSalesPriceValue = 0;
                    }else{
                        $itemSalesPriceValue = $itemPriceValue->price;
                    }

                    $return_arr[$i]['id']          = $value->id;
                    $return_arr[$i]['stock_id']    = $value->stock_id;
                    $return_arr[$i]['description'] = $value->description;
                    $return_arr[$i]['hsn']         = $value->hsn?$value->hsn:'-';
                    $return_arr[$i]['units']       = $value->units;
                    $return_arr[$i]['price']       = $itemSalesPriceValue;
                    $return_arr[$i]['tax_rate']    = $value->tax_rate;
                    $return_arr[$i]['tax_id']      = $value->tax_id;

                    $i++;
                }
                //echo json_encode($return_arr);
                 $data['items'] = $return_arr;
            }
            echo json_encode($data);
            exit;            
    }

    /**
    * Check reference no if exists
    */
    public function referenceValidation(Request $request){
        
        $data = array();
        $ref = $request['ref'];
        $result = DB::table('sales_orders')->where("reference",$ref)->first();

        if(count($result)>0){
            $data['status_no'] = 1; 
        }else{
            $data['status_no'] = 0;
        }

        return json_encode($data);       
    }

    /**
    * Return customer Branches by customer id
    */
    public function customerBranches(Request $request){
        $debtor_no = $request['debtor_no'];
        $data['status_no'] = 0;
        $branchs = '';
        $result = DB::table('cust_branch')->select('debtor_no','branch_code','br_name')->where('debtor_no',$debtor_no)->orderBy('br_name','ASC')->get();
        if(!empty($result)){
            $data['status_no'] = 1;
            foreach ($result as $key => $value) {
            $branchs .= "<option value='".$value->branch_code."'>".$value->br_name."</option>";  
        }
        $data['branchs'] = $branchs; 
       }
        return json_encode($data);
    }

   

    /**
    * Preview of order details
    * @params order_no
    **/

    public function viewOrderDetails($orderNo){

        $data['menu']     = 'sales';
        $data['sub_menu'] = 'order/list';

        $data['saleData'] = DB::table('sales_orders')
                            ->where('order_no', '=', $orderNo)
                            ->leftJoin('location','location.loc_code','=','sales_orders.from_stk_loc')
                            ->select("sales_orders.*","location.location_name")
                            ->first();
        $data['invoiceType'] = $data['saleData']->invoice_type;
        $data['invoiceData'] = $this->order->getSalseOrderByID($orderNo,$data['saleData']->from_stk_loc);
        //d($data['invoiceData']);
        $data['customerInfo']  = DB::table('sales_orders')
                             ->where('sales_orders.order_no',$orderNo)
                             ->leftjoin('debtors_master','debtors_master.debtor_no','=','sales_orders.debtor_no')
                             ->leftjoin('cust_branch','cust_branch.branch_code','=','sales_orders.branch_id')
                             ->leftjoin('countries','countries.id','=','cust_branch.shipping_country_id')
                             ->select('debtors_master.debtor_no','debtors_master.name','debtors_master.phone','debtors_master.gstin','debtors_master.email','cust_branch.br_name','cust_branch.br_address','cust_branch.billing_street','cust_branch.billing_city','cust_branch.billing_state','cust_branch.billing_zip_code','cust_branch.billing_country_id','cust_branch.shipping_street','cust_branch.shipping_city','cust_branch.shipping_state','cust_branch.shipping_zip_code','cust_branch.shipping_country_id','countries.country')
                             ->first(); 

        $pref                  = DB::table('preference')->where('user_id', auth()->user()->id)->where('category', 'company')->get();
        $companyData           = AssColumn($a=$pref, $column='id');
        $array_data = array();
        foreach($companyData as $row)
        {
          $array_data[$row->category][$row->field] = $row->value;         
        }
        $data['companyInfo'] = $array_data; 

        $data['orderInfo']  = DB::table('sales_orders')->where('order_no',$orderNo)->select('reference','order_no')->first();
        $data['paymentsList'] = DB::table('payment_history')
                            ->where(['order_reference'=>$data['orderInfo']->reference])
                            ->leftjoin('payment_terms','payment_terms.id','=','payment_history.payment_type_id')
                            ->select('payment_history.*','payment_terms.name')
                            ->orderBy('payment_date','DESC')
                            ->get();
        $lang = Session::get('dflt_lang');
        $data['emailInfo'] = DB::table('email_temp_details')->where(['temp_id'=>5,'lang'=>$lang])->select('subject','body')->first();
        $checkInvoiced = DB::table('sales_orders')->where('order_reference_id',$orderNo)->first();
        if($checkInvoiced){
          $data['invoiced_date'] = $checkInvoiced->ord_date;
          $data['invoiced_status'] = 'yes';
        }else{
          $data['invoiced_status'] = 'no';
         }
        return view('admin.salesOrder.viewOrderDetails', $data);

    }
    /**
    * Create auto invoice
    *@params order_id
    */

    public function autoInvoiceCreate($orderNo){
        $userId = \Auth::user()->id;
        $invoiceCount = DB::table('sales_orders')->where('trans_type',SALESINVOICE)->count();
        if($invoiceCount>0){
        $invoiceReference = DB::table('sales_orders')->where('trans_type',SALESINVOICE)->select('reference')->orderBy('order_no','DESC')->first();
        $ref = explode("-",$invoiceReference->reference);
        $invoice_count = (int) $ref[1];
        }else{
            $invoice_count = 0 ;
        }

        $invoiceInfos = $this->order->getRestOrderItemsByOrderID($orderNo);
        $orderInfo = DB::table('sales_orders')->where('order_no', '=', $orderNo)->first();

        $payment_term = DB::table('invoice_payment_terms')->where('defaults',1)->select('id')->first();
        // Create salesOrder Invoice start
        $salesOrderInvoice['order_reference_id'] = $orderNo;
        $salesOrderInvoice['order_reference'] = $orderInfo->reference;
        $salesOrderInvoice['trans_type'] = SALESINVOICE;
        $salesOrderInvoice['invoice_type'] = 'indirectInvoice';
        $salesOrderInvoice['reference'] ='INV-'.sprintf("%04d", $invoice_count+1);
        $salesOrderInvoice['debtor_no'] = $orderInfo->debtor_no;
        $salesOrderInvoice['branch_id'] = $orderInfo->branch_id;
        $salesOrderInvoice['person_id']= $userId;
        $salesOrderInvoice['payment_id']= $orderInfo->payment_id;
        $salesOrderInvoice['comments'] = $orderInfo->comments;
        $salesOrderInvoice['ord_date'] = $orderInfo->ord_date;
        $salesOrderInvoice['from_stk_loc'] = $orderInfo->from_stk_loc;
        $salesOrderInvoice['payment_term']= 2;
        $salesOrderInvoice['total'] = $orderInfo->total;

        $salesOrderInvoice['created_at'] = date('Y-m-d H:i:s');        

        $orderInvoiceId = DB::table('sales_orders')->insertGetId($salesOrderInvoice);

        foreach($invoiceInfos as $i=>$invoiceInfo){
            $salesOrderDetailInvoice['order_no'] = $orderInvoiceId;
            $salesOrderDetailInvoice['stock_id'] = $invoiceInfo->stock_id;
            $salesOrderDetailInvoice['description'] = $invoiceInfo->description;
            $salesOrderDetailInvoice['hsn']         = $invoiceInfo->hsn;
            $salesOrderDetailInvoice['quantity'] = $invoiceInfo->quantity;
            $salesOrderDetailInvoice['trans_type'] = SALESINVOICE;
            $salesOrderDetailInvoice['discount_percent'] = $invoiceInfo->discount_percent;
            $salesOrderDetailInvoice['tax_type_id'] = $invoiceInfo->tax_type_id;
            $salesOrderDetailInvoice['unit_price'] = $invoiceInfo->unit_price;
            $salesOrderDetailInvoice['is_inventory'] = $invoiceInfo->is_inventory;

            DB::table('sales_order_details')->insertGetId($salesOrderDetailInvoice);
            // Create salesOrderDetailInvoice End

            // create stockMove 
            if($invoiceInfo->is_inventory == 1){
                $stockMove['stock_id'] = $invoiceInfo->stock_id;
                $stockMove['order_no'] = $orderNo;
                $stockMove['loc_code'] = $orderInfo->from_stk_loc;
                $stockMove['tran_date'] = date('Y-m-d');
                $stockMove['person_id'] = $userId;
                $stockMove['reference'] = 'store_out_'.$orderInvoiceId;
                $stockMove['transaction_reference_id'] = $orderInvoiceId;
                $stockMove['qty'] = '-'.$invoiceInfo->quantity;
                $stockMove['price'] = $invoiceInfo->unit_price;
                $stockMove['trans_type'] = SALESINVOICE;
                $stockMove['order_reference'] = $orderInfo->reference;
                DB::table('stock_moves')->insertGetId($stockMove);
        }
       
        }
            \Session::flash('success',trans('message.success.save_success'));
            return redirect()->intended('invoice/view-detail-invoice/'.$orderNo.'/'.$orderInvoiceId);
    }


    /**
    * Preview of order details
    * @params order_no
    **/

    public function orderPdf($orderNo){
       
        $data['saleData'] = DB::table('sales_orders')
                            ->where('order_no', '=', $orderNo)
                            ->leftJoin('location','location.loc_code','=','sales_orders.from_stk_loc')
                            ->select("sales_orders.*","location.location_name")
                            ->first();
        $data['invoiceData'] = $this->order->getSalseOrderByID($orderNo,$data['saleData']->from_stk_loc);
        $data['customerInfo']  = DB::table('sales_orders')
                             ->where('sales_orders.order_no',$orderNo)
                             ->leftjoin('debtors_master','debtors_master.debtor_no','=','sales_orders.debtor_no')
                             ->leftjoin('cust_branch','cust_branch.branch_code','=','sales_orders.branch_id')
                             ->leftjoin('countries','countries.id','=','cust_branch.shipping_country_id')
                             ->select('debtors_master.debtor_no','debtors_master.name','debtors_master.phone','debtors_master.gstin','debtors_master.email','cust_branch.br_name','cust_branch.br_address','cust_branch.billing_street','cust_branch.billing_city','cust_branch.billing_state','cust_branch.billing_zip_code','cust_branch.billing_country_id','cust_branch.shipping_street','cust_branch.shipping_city','cust_branch.shipping_state','cust_branch.shipping_zip_code','cust_branch.shipping_country_id','countries.country')                            
                             ->first();

        $pref                  = DB::table('preference')->where('user_id', auth()->user()->id)->where('category', 'company')->get();
        $companyData           = AssColumn($a=$pref, $column='id');
        $array_data = array();
        foreach($companyData as $row)
        {
          $array_data[$row->category][$row->field] = $row->value;         
        }
        $data['companyInfo'] = $array_data; 

        $pdf = PDF::loadView('admin.salesOrder.orderPdf', $data);
        $pdf->setPaper(array(0,0,750,1060), 'portrait');
        return $pdf->download('order_'.time().'.pdf',array("Attachment"=>0));
    }
    public function orderPrint($orderNo){
       
        $data['saleData'] = DB::table('sales_orders')
                            ->where('order_no', '=', $orderNo)
                            ->leftJoin('location','location.loc_code','=','sales_orders.from_stk_loc')
                            ->select("sales_orders.*","location.location_name")
                            ->first();
        $data['invoiceData'] = $this->order->getSalseOrderByID($orderNo,$data['saleData']->from_stk_loc);
        $data['customerInfo']  = DB::table('sales_orders')
                             ->where('sales_orders.order_no',$orderNo)
                             ->leftjoin('debtors_master','debtors_master.debtor_no','=','sales_orders.debtor_no')
                             ->leftjoin('cust_branch','cust_branch.branch_code','=','sales_orders.branch_id')
                             ->leftjoin('countries','countries.id','=','cust_branch.shipping_country_id')
                             ->select('debtors_master.debtor_no','debtors_master.name','debtors_master.phone','debtors_master.gstin','debtors_master.email','cust_branch.br_name','cust_branch.br_address','cust_branch.billing_street','cust_branch.billing_city','cust_branch.billing_state','cust_branch.billing_zip_code','cust_branch.billing_country_id','cust_branch.shipping_street','cust_branch.shipping_city','cust_branch.shipping_state','cust_branch.shipping_zip_code','cust_branch.shipping_country_id','countries.country')                            
                             ->first();

        $pref                  = DB::table('preference')->where('user_id', auth()->user()->id)->where('category', 'company')->get();
        $companyData           = AssColumn($a=$pref, $column='id');
        $array_data = array();
        foreach($companyData as $row)
        {
          $array_data[$row->category][$row->field] = $row->value;         
        }
        $data['companyInfo'] = $array_data; 

        $pdf = PDF::loadView('admin.salesOrder.orderPrint', $data);
        $pdf->setPaper(array(0,0,750,1060), 'portrait');
        return $pdf->stream('order_'.time().'.pdf',array("Attachment"=>0));
    
        return view('admin.salesOrder.orderPrint', $data);
    }
    /**
    * Send email to customer for Invoice information
    */
    public function sendOrderInformationByEmail(Request $request){
        
        $orderNo = $request['order_id'];
        $invoiceName = 'quotation.pdf';

        if(isset($request['quotation_pdf']) && $request['quotation_pdf']=='on'){
            $this->orderPdfEmail($orderNo,$invoiceName);
            $this->email->sendEmailWithAttachment($request['email'],$request['subject'],$request['message'],$invoiceName);
         }else{
            $this->email->sendEmail($request['email'],$request['subject'],$request['message']);
         } 

        \Session::flash('success',trans('message.email.email_send_success'));
        return redirect()->intended('order/view-order-details/'.$request['order_id']);
    }


    public function orderPdfEmail($orderNo,$invoiceName){

        $data['saleData'] = DB::table('sales_orders')
                            ->where('order_no', '=', $orderNo)
                            ->leftJoin('location','location.loc_code','=','sales_orders.from_stk_loc')
                            ->select("sales_orders.*","location.location_name")
                            ->first();
        $data['invoiceData'] = $this->order->getSalseOrderByID($orderNo,$data['saleData']->from_stk_loc);
        $data['customerInfo']  = DB::table('sales_orders')
                             ->where('sales_orders.order_no',$orderNo)
                             ->leftjoin('debtors_master','debtors_master.debtor_no','=','sales_orders.debtor_no')
                             ->leftjoin('cust_branch','cust_branch.branch_code','=','sales_orders.branch_id')
                             ->leftjoin('countries','countries.id','=','cust_branch.shipping_country_id')
                             ->select('debtors_master.debtor_no','debtors_master.name','debtors_master.phone','debtors_master.email','cust_branch.br_name','cust_branch.br_address','cust_branch.billing_street','cust_branch.billing_city','cust_branch.billing_state','cust_branch.billing_zip_code','cust_branch.billing_country_id','cust_branch.shipping_street','cust_branch.shipping_city','cust_branch.shipping_state','cust_branch.shipping_zip_code','cust_branch.shipping_country_id','countries.country')                            
                             ->first();
                             
        $pref                  = DB::table('preference')->where('user_id', auth()->user()->id)->where('category', 'company')->get();
        $companyData           = AssColumn($a=$pref, $column='id');
        $array_data = array();
        foreach($companyData as $row)
        {
          $array_data[$row->category][$row->field] = $row->value;         
        }
        $data['companyInfo'] = $array_data; 

        $pdf = PDF::loadView('admin.salesOrder.orderPdf', $data);
        $pdf->setPaper(array(0,0,750,1060), 'portrait');
        return $pdf->save(public_path().'/uploads/invoices/'.$invoiceName);        
    }

}
