<?php

namespace App\Http\Controllers;

use App\Model\Currency;
use App\Model\PaymentGateway;
use App\Model\PaymentMethod;
use App\Model\PaymentTerm;
use Illuminate\Http\Request;
use App\Http\Controllers\EmailController;
use App\Http\Requests;
use DB;
use Validator;
use App\Http\Start\Helpers;
use Session;

class SettingController extends Controller
{
    

    public function __construct(EmailController $email){
        $this->email = $email;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = \Auth::user()->id;
        $data['menu'] = 'setting';
        $data['sub_menu'] = 'general';
        $data['list_menu'] = 'profile';
        $data['roleData'] = DB::table('security_role')->get();
        $data['userData'] = DB::table('users')->where('id', '=', $id)->first();
        
        return view('admin.setting.editProfile', $data);
    }

    public function mailTemp()
    {
        $data['menu'] = 'setting';
        $data['sub_menu'] = 'mail-temp';
        $data['tempData'] = DB::table('email_temp')->get();
        
        return view('admin.mailTemp.temp_list', $data);
    }

    public function finance()
    {
        $data['menu'] = 'setting';
        $data['sub_menu'] = 'finance';
        $data['list_menu'] = 'tax';
        
        return view('admin.setting.finance', $data);
    }

    public function currency()
    {
        $data['menu'] = 'setting';
        $data['sub_menu'] = 'finance';
        $data['list_menu'] = 'currency';
        $data['currencyData'] = Currency::where('user_id', auth()->user()->id)->get();
        
        return view('admin.setting.currency', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:2',
            'symbol' => 'required',
        ]);

        $data['name'] = $request->name;
        $data['symbol'] = $request->symbol;
        $data['user_id'] = auth()->user()->id;

        $currency = Currency::create($data);

        if ($currency) {
            \Session::flash('success', trans('message.success.save_success'));
            return redirect()->intended('currency');
        } else {
            return back()->withInput()->withErrors(['email' => "Invalid Request !"]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $id = $_POST['id'];

        $currData = Currency::where('id', $id)->first();
        
        $return_arr['name'] = $currData->name;
        $return_arr['symbol'] = $currData->symbol;
        $return_arr['id'] = $currData->id;

        echo json_encode($return_arr);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:2',
            'symbol' => 'required',
            'id' => 'required',
        ]);

        $id = $request->id;
        $data['name'] = $request->name;
        $data['symbol'] = $request->symbol;

        Currency::where('id', $id)->update($data);

        \Session::flash('success',trans('message.success.update_success'));
            return redirect()->intended('currency');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (isset($id)) {
            $record = Currency::where('id', $id)->first();
            if ($record) {
                Currency::where('id', '=', $id)->delete();
                \Session::flash('success',trans('message.success.delete_success'));
                return redirect()->intended('currency');
            }
        }
    }

    public function preference()
    {
        $data['menu'] = 'setting';
        $data['sub_menu'] = 'preference';
        $data['currencyData'] = DB::table('currency')->get();
        $pref                      = DB::table('preference')->where('user_id', auth()->user()->id)->where('category', 'preference')->get();
        $companyData               = AssColumn($a=$pref, $column='id');
        $array_data = array();
        foreach($companyData as $row)
        {
          $array_data[$row->category][$row->field] = $row->value;         
        }
        $data['prefData'] = $array_data;
        
        return view('admin.setting.preference', $data);
    }

  public function savePreference(Request $request)
    {

        $post = $request->all();
        unset($post['_token']);
        
        if($post['date_format'] == 0) {
            $post['date_format_type'] = 'yyyy'.$post['date_sepa'].'mm'.$post['date_sepa'].'dd';
        }elseif ($post['date_format'] == 1) {
            $post['date_format_type'] = 'dd'.$post['date_sepa'].'mm'.$post['date_sepa'].'yyyy';
        }elseif ($post['date_format'] == 2) {
            $post['date_format_type'] = 'mm'.$post['date_sepa'].'dd'.$post['date_sepa'].'yyyy';
        }elseif ($post['date_format'] == 3) {
            $post['date_format_type'] = 'dd'.$post['date_sepa'].'M'.$post['date_sepa'].'yyyy';
        }elseif ($post['date_format'] == 4) {
            $post['date_format_type'] = 'yyyy'.$post['date_sepa'].'M'.$post['date_sepa'].'dd';
        }

        $i=0;
        foreach ($post as $key => $value) {
            $data[$i]['category'] = "preference";
            $data[$i]['field'] = $key;
            $data[$i]['value'] = $value;
            $i++;
        }
         foreach($data as $key => $value){
            $category = $value['category']; 
            $field    = $value['field'];
            $val      = $value['value'];
            $res      = DB::table('preference')->where('user_id', auth()->user()->id)->where(['field' => $field])->first();
            if(count($res)==0){
                $userId = auth()->user()->id;
                DB::insert(DB::raw("INSERT INTO preference(category,field,value,user_id) VALUES ('$category','$field','$val','$userId')"));
            }else{
                DB::table('preference')->where('user_id', auth()->user()->id)->where(['category'=>'preference','field' => $field])->update(array('field'=>$field,'value' => $val));
            }
            
        }
        $pref = DB::table('preference')->where('user_id', auth()->user()->id)->where('category','preference')->get();
        if(!empty($pref)) {
                $prefData = AssColumn($a=$pref, $column='id');
                foreach ($prefData as $value) {
                    $prefer[$value->field] = $value->value;
                }
            Session::put($prefer);
        }       
        \Session::flash('success',trans('message.success.save_success'));
        return redirect()->intended('setting-preference');
    }

    public function backupDB()
    {
        $backup_name = backup_tables(env('DB_HOST'), env('DB_USERNAME'), env('DB_PASSWORD'), env('DB_DATABASE'));
        if($backup_name != 0){
            DB::table('backup')->insert(['user_id'=>auth()->user()->id, 'name' => $backup_name, 'created_at' => date('Y-m-d H:i:s')]);
            \Session::flash('success',trans('message.success.save_success'));
        }
        return redirect()->intended('backup/list');
    }

    public function backupList()
    {
        $data['menu'] = 'setting';
        $data['sub_menu'] = 'general';
        $data['list_menu'] = 'backup';
        $data['path'] = storage_path();
        $data['backupData'] = DB::table('backup')->where('user_id', auth()->user()->id)->orderBy('id', 'desc')->get();
        
        return view('admin.setting.backupList', $data);
    }

    public function emailSetup()
    {
        $data['menu'] = 'setting';
        $data['sub_menu'] = 'general';
        $data['list_menu'] = 'email_setup';
        $data['emailConfigData'] = DB::table('email_config')->where('user_id', auth()->user()->id)->first();
        //d($data['emailConfigData'],1);
        return view('admin.setting.emailConfig',$data);
    }

    public function emailSaveConfig(Request $request)
    {
        $data = $request->all();
        unset($data['_token']);
        
        $emailConfig = DB::table('email_config')->where('user_id', auth()->user()->id)->first();
        if(!empty($emailConfig)) {
            DB::table('email_config')->where('user_id', auth()->user()->id)->update($data);
        }else{
            $data['user_id'] = auth()->user()->id;
            DB::table('email_config')->insert($data);
        }

         \Session::flash('success',trans('message.success.save_success'));
            return redirect()->intended('email/setup');
    }

    public function testEmailConfig()
    {
        
        $from = \Config::get('mail.from');
        

        $data['mailTo'] = $_POST['email'];
        $data['mailfrom'] = $from['address'];
        $data['subject']  = 'Testing Email';
        $data['message']  = 'Hello, <br>This is a test email.<br>Thanks';
        $this->email->sendEmail($data['mailTo'],$data['subject'],$data['message']);
        $return_arr['name'] = $data['mailTo'];

        echo json_encode($return_arr);

    }

    public function paymentTerm()
    {
        $data['menu'] = 'setting';
        $data['sub_menu'] = 'finance';
        $data['list_menu'] = 'payment_term';
        $data['paymentTermData'] = PaymentTerm::where('user_id', auth()->user()->id)->get();
        return view('admin.payment.paymentTerm',$data);
    }

    public function addPaymentTerms(Request $request)
    {
        $data = $request->all();
        unset($data['_token']);

        if($request->defaults == 1) {
            PaymentTerm::where('user_id', auth()->user()->id)->where('defaults', 1)->update(['defaults'=>0]);
        }
        $data['user_id'] = auth()->user()->id;
        PaymentTerm::create($data);

        \Session::flash('success',trans('message.success.save_success'));
        return redirect()->intended('payment/terms');
    }

    public function editPaymentTerms()
    {
        $id = $_POST['id'];

        $termData = PaymentTerm::where('id',$id)->first();

        $return_arr['id'] = $termData->id;
        $return_arr['terms'] = $termData->terms;
        $return_arr['days_before_due'] = $termData->days_before_due;
        $return_arr['defaults'] = $termData->defaults;

        echo json_encode($return_arr);
    }

    public function updatePaymentTerms(Request $request)
    {
        $id = $request->id;
        $data = $request->all();
        unset($data['_token']);
        unset($data['id']);

        if($request->defaults == 1) {
            PaymentTerm::where('user_id', auth()->user()->id)->where('defaults', 1)->update(['defaults'=>0]);
        }
        PaymentTerm::where('id',$id)->update($data);

        \Session::flash('success',trans('message.success.update_success'));
        return redirect()->intended('payment/terms');
    }

    public function deletePaymentTerm($id)
    {
        if (isset($id)) {
            $record = PaymentTerm::where('id', $id)->first();
            if ($record) {
                PaymentTerm::where('id', '=', $id)->delete();
                \Session::flash('success',trans('message.success.delete_success'));
                return redirect()->intended('payment/terms');
            }
        }
    }

    public function paymentMethod()
    {
        $data['menu'] = 'setting';
        $data['sub_menu'] = 'finance';
        $data['list_menu'] = 'payment_method';
        $data['paymentMethodData'] = PaymentMethod::where('user_id', auth()->user()->id)->get();
        return view('admin.payment.paymentMethod',$data);
    }

    public function addPaymentMethod(Request $request)
    {
        $data = $request->all();
        unset($data['_token']);

        if($request->defaults == 1) {
            PaymentMethod::where('user_id', auth()->user()->id)->where('defaults', 1)->update(['defaults'=>0]);
        }
        $data['user_id'] = auth()->user()->id;
        PaymentMethod::create($data);
        \Session::flash('success',trans('message.success.save_success'));
        return redirect()->intended('payment/method');
    }

    public function editPaymentMethod()
    {
        $id = $_POST['id'];

        $methodData = PaymentMethod::where('id',$id)->first();

        $return_arr['id'] = $methodData->id;
        $return_arr['name'] = $methodData->name;
        $return_arr['defaults'] = $methodData->defaults;

        echo json_encode($return_arr);
    }

    public function updatePaymentMethod(Request $request)
    {
        $id = $request->id;
        $data = $request->all();
        unset($data['_token']);
        unset($data['id']);

        if($request->defaults == 1) {
            PaymentMethod::where('user_id', auth()->user()->id)->where('defaults', 1)->update(['defaults'=>0]);
        }
        PaymentMethod::where('id',$id)->update($data);
        \Session::flash('success',trans('message.success.update_success'));
        return redirect()->intended('payment/method');
    }

    public function deletePaymentMethod($id)
    {
        if (isset($id)) {
            $record = PaymentMethod::where('id', $id)->first();
            if ($record) {
                PaymentMethod::where('id', '=', $id)->delete();
                \Session::flash('success',trans('message.success.delete_success'));
                return redirect()->intended('payment/method');
            }
        }
    }

    public function companySetting()
    {
        $data['menu'] = 'setting';
        $data['sub_menu'] = 'company';
        $data['list_menu'] = 'sys_company';
        $data['paymentMethodData'] = DB::table('payment_terms')->get();
        $data['all_state']         = DB::table('states')->get();
        $data['countries'] = DB::table('countries')->get();
        $data['currencyData'] = DB::table('currency')->where('user_id', auth()->user()->id)->get();
        $data['saleTypes'] = DB::table('sales_types')->get();
        $pref                      = DB::table('preference')->where('user_id', auth()->user()->id)->where('category', 'company')->get();
        $companyData               = AssColumn($a=$pref, $column='id');
        $array_data = array();
        foreach($companyData as $row)
        {
          $array_data[$row->category][$row->field] = $row->value;         
        }
        $data['companyData'] = $array_data;
        
        return view('admin.setting.companySetting',$data);
    }

     public function companySettingSave(Request $request){
        $post                  = $request->all();
        $post['company_state'] = $request->company_state_select?$request->company_state_select:$request->company_state;
        unset($post['company_state_select']);
        unset($post['_token']);
                //d($post,1);

        $i = 0;
        $company_logo = $request->file('company_logo');
        $update_logo  = false;

        $filename = $request->company_logo;
        if (isset($company_logo)) {
          $old_img = DB::table('preference')->where('user_id', auth()->user()->id)->where(['category' => 'company','field' =>'company_logo'])->first();
          $file    = $old_img?$old_img->value:'';

          $upload = 'public/uploads/company';        
          if ($file != NULL) {
            $dir = public_path("uploads/company/$file");
            if (file_exists($dir)) {
               unlink($dir);  
            }
          }

          
          $exp = explode('.', $company_logo->getClientOriginalName());
          $ext = $exp[count($exp) - 1];
          $company_id = \Auth::user()->id;
          
          $filename = $company_id . '_' . time() . '.' . $ext; 
          
          $company_logo->move($upload, $filename);
        }
        
        foreach ($post as $key => $value) {
        
            $data[$i]['category'] = 'company'; 
            $data[$i]['field'] = $key; 
            
            if($key =='company_logo')
                $data[$i]['value'] = $filename;
            else
                $data[$i]['value'] = $value; 


            $i++;
        }
        foreach($data as $key => $value){
            $category = $value['category'];
            $field    = $value['field'];
            $val      = $value['value'];
            $res      = DB::table('preference')->where('user_id', auth()->user()->id)->where(['field' => $field])->first();
            if(count($res)==0){
                $userId = auth()->user()->id;
                DB::insert(DB::raw("INSERT INTO preference(category,field,value,user_id) VALUES ('$category','$field','$val','$userId')"));
            }else{
                DB::table('preference')->where('user_id', auth()->user()->id)->where(['category'=>'company','field' => $field])->update(array('field'=>$field,'value' => $val));
            }
        }
        $pref = DB::table('preference')->where('user_id', auth()->user()->id)->where('category','company')->get();
        if(!empty($pref)) {
                $prefData = AssColumn($a=$pref, $column='id');
                foreach ($prefData as $value) {
                    $prefer[$value->field] = $value->value;
                }
            Session::put($prefer);
        } 
        \Session::flash('success',trans('message.success.update_success'));
        return redirect()->intended('company/setting');

    }
    public function destroyBackup($id)
    {
        if(isset($id)) {
            $record = \DB::table('backup')->where('id', $id)->first();
            if($record) {
                DB::table('backup')->where('id', '=', $id)->delete();
                \Session::flash('success',trans('message.success.delete_success'));
                return redirect()->intended('backup/list');
            }
        }
    }

    public function PaymentGateway(Request $request){
        $data['menu'] = 'setting';
        $data['sub_menu'] = 'finance';
        $data['list_menu'] = 'payment_gateway';

        if(!$_POST)
        {
            $data['result'] = DB::table('payment_gateway')->get();
            //dd($data['result']);
            //$data['result'] = PaymentGateway::where('user_id', auth()->user()->id)->get()->toArray();
            return view('admin.setting.paymentGateway', $data);
        }
        else if($request->submit)
        {
            // Payment Gateway Validation Rules
            $rules = array(
                    'username'  => 'required',
                    'password'  => 'required',
                    'signature' => 'required'
                    );

            // Payment Gateway Validation Custom Names
            $niceNames = array(
                        'username'  => 'PayPal Username',
                        'password'  => 'PayPal Password',
                        'signature' => 'PayPal Signature'
                        );

            $validator = Validator::make($request->all(), $rules);
            $validator->setAttributeNames($niceNames); 

            if ($validator->fails()) 
            {
                return back()->withErrors($validator)->withInput(); // Form calling with Errors and Input values
            } else {
                if (env('APP_MODE', '') != 'test') {
                    DB::table('payment_gateway')->where(['name' => 'username', 'site' => 'PayPal'])->update(['value' => $request->username]);

                    DB::table('payment_gateway')->where(['name' => 'password', 'site' => 'PayPal'])->update(['value' => $request->password]);

                    DB::table('payment_gateway')->where(['name' => 'signature', 'site' => 'PayPal'])->update(['value' => $request->signature]);

                    DB::table('payment_gateway')->where(['name' => 'mode', 'site' => 'PayPal'])->update(['value' => $request->mode]);

                    \Session::flash('success', 'Paypal information updated successfully'); // Call flash message function
                }
                return redirect('payment/gateway');
            }
        }
        else
        {
            return redirect('payment/gateway');
        }
    }
}
