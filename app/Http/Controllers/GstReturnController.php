<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Sales;
use App\Http\Requests;
use DB;
use PDF;
use Session;
use Excel;
use App\Http\Start\Helpers;

class GstReturnController extends Controller
{
    public function __construct(Sales $sales){
        $this->sale = $sales;
    }



    public function index()
    {
        $data['menu']       = 'gst-return';
        //$data['sub_menu']   = 'sales/direct-invoice';
        $data['gstReturn']  = $this->sale->getAllGstReturn($from = NULL, $to = NULL);
        //d($data['gstReturn'],1);
        $pref                   = DB::table('preference')->where('user_id', auth()->user()->id)->where('category', 'company')->get();
        $companyData            = AssColumn($a=$pref, $column='id');
         $array_data = array();
            foreach($companyData as $row)
            {
              $array_data[$row->category][$row->field] = $row->value;         
            }
        $data['companyInfo'] = $array_data;

        return view('admin.gstReturn.gst_return_list', $data);
    }

    public function gstFilter(){
        $data['menu']      = 'gst-return';
        //$data['sub_menu']  = 'sales/direct-invoice';

        $fromDate = DB::table('sales_orders')->select('ord_date')->where('deleted', 0)->where('user_id', auth()->user()->id)->where('trans_type',SALESINVOICE)->orderBy('ord_date','asc')->first();
        
        if(isset($_GET['from'])){
            $data['from'] = $from = $_GET['from'];
        }else{
           $data['from'] = $from = isset($fromDate->ord_date) ? formatDate(date("d-m-Y", strtotime($fromDate->ord_date))) : date('d-m-Y'); 
        }
        
        if(isset($_GET['to'])){
            $data['to'] = $to = $_GET['to'];
        }else{
            $data['to'] = $to = formatDate(date('d-m-Y'));
        }

        $data['gstReturn'] = $this->sale->getAllGstReturn($from, $to);
         
        $pref                   = DB::table('preference')->where('user_id', auth()->user()->id)->where('category', 'company')->get();
        $companyData            = AssColumn($a=$pref, $column='id');
         $array_data = array();
            foreach($companyData as $row)
            {
              $array_data[$row->category][$row->field] = $row->value;         
            }
        $data['companyInfo'] = $array_data;

        return view('admin.gstReturn.gst_return_list_filter', $data);

    }
    public function gstReturnCsv(){

         $to                     = isset($_GET['to']) ? DbDateFormat($_GET['to']) : NULL;
         $from                   = isset($_GET['from']) ? DbDateFormat($_GET['from']) : NULL;
          
         $gstReturnList          = $this->sale->getAllGstReturnCsv($from, $to);
         
         $pref                   = DB::table('preference')->where('user_id', auth()->user()->id)->where('category', 'company')->get();
         $companyData            = AssColumn($a=$pref, $column='id');
         $array_data = array();
            foreach($companyData as $row)
            {
              $array_data[$row->category][$row->field] = $row->value;         
            }
          $companyInfo = $array_data;

          if(count($gstReturnList)>0){
            
                foreach ($gstReturnList as $key => $item) {
                    $datas[$key]['Invoice Date']                     = formatDate($item->ord_date);
                    $datas[$key]['Invoice Number']                     = $item->reference;
                    $datas[$key]['State Place of Supply']              = $item->billing_state;
                    $datas[$key]['GST Status']                         = 'NA';
                    $datas[$key]['Is the customer a Composition dealer or UIN registered?']        = '';
                    $datas[$key]['Return Filing Period']               = '';
                    $datas[$key]['Total Transaction Value']            = $item->sale_price_incl_tax;
                    $datas[$key]['Is Reverse Charge Applicable?']      = 'N';
                    $datas[$key]['Original Invoice Date (In case of amendment)']        = '';
                    $datas[$key]['Original Invoice Number (In case of amendment)']      = '';
                    $datas[$key]['Original Customer Billing GSTIN (In case of amendment)']        = '';
                    $datas[$key]['Is this document cancelled?']           = '';
                    $datas[$key]['GSTIN of Ecommerce Marketplace']        = '';
                    $datas[$key]['Is this a Bill of Supply?']             = 'N';
                    $datas[$key]['Has GST/IDT TDS been deducted']         = 'N';
                    $datas[$key]['Type of Export']                        = '';
                    $datas[$key]['Shipping Port Code - Export']           = '';
                    $datas[$key]['Shipping Bill Number - Export']         = '';
                    $datas[$key]['Shipping Bill Date - Export']           = '';
                    $datas[$key]['Customer Billing Name']                 = $item->name;
                    $datas[$key]['Customer Billing GSTIN']                = $item->gstin;
                    $datas[$key]['Customer Billing Address']              = $item->br_address;
                    $datas[$key]['Customer Billing City']                 = $item->billing_city;
                    $datas[$key]['Customer Billing State']                = $item->billing_state;
                    $datas[$key]['Date of Linked Advance Receipt']        = '';
                    $datas[$key]['Voucher Number of Linked Advance Receipt']                  = '';
                    $datas[$key]['Adjustment Amount of the Linked Advance Receipt']           = '';
                    $datas[$key]['Item Quantity']                                             = $item->quantity;
                    $datas[$key]['Item Unit of Measurement']                                  = '';
                    $datas[$key]['Item Rate']                                                 = $item->unit_price;
                    $datas[$key]['Is the item a GOOD (G) or SERVICE (S)']                     = '';
                    $datas[$key]['Item Description']                                          = $item->description;
                    $datas[$key]['HSN or SAC Code']                                           = $item->hsn;
                    if($item->billing_country_id=='IN' && ($item->billing_state==$companyInfo['company']['company_state'] && $companyInfo['company']['company_country_id']=='India')){
                    $datas[$key]['CGST Rate']                                                 = number_format($item->tax_rate/2,2,'.',',');
                    $datas[$key]['CGST Amount']                                               = number_format(($item->sale_price_incl_tax-$item->sale_price_excl_tax)/2,2,'.',',');
                    $datas[$key]['SGST Rate']                                                 = number_format($item->tax_rate/2,2,'.',',');
                    $datas[$key]['SGST Amount']                                               = number_format(($item->sale_price_incl_tax-$item->sale_price_excl_tax)/2,2,'.',',');
                    $datas[$key]['IGST Rate']                                                 = '';
                    $datas[$key]['IGST Amount']                                               = '';
                   }
                    else{
                    $datas[$key]['CGST Rate']                                                 = '';
                    $datas[$key]['CGST Amount']                                               = '';
                    $datas[$key]['SGST Rate']                                                 = '';
                    $datas[$key]['SGST Amount']                                               = '';
                    $datas[$key]['IGST Rate']                                                 = number_format($item->tax_rate,2,'.',',');
                    $datas[$key]['IGST Amount']                                               = number_format(($item->sale_price_incl_tax-$item->sale_price_excl_tax),2,'.',',');
                    }
                    $datas[$key]['CESS Rate']                                                 = '';
                    $datas[$key]['CESS Amount']                                               = '';
                    $datas[$key]['Item Taxable Value']     = Session::get('currency_symbol').number_format($item->sale_price_excl_tax,2,'.',',');
                    $datas[$key]['Total Item Discount Amount']     = number_format($item->discount_amount,2,'.',',');
                    $datas[$key]['Is this a Nil Rated/Exempt/NonGST item?']     = '';
                }
            }else{
                    $datas[0]['Invoice Date']                       = '';
                    $datas[0]['Invoice Number']                     = '';
                    $datas[0]['State Place of Supply']              = '';
                    $datas[0]['GST Status']                         = '';
                    $datas[0]['Is the customer a Composition dealer or UIN registered?']        = '';
                    $datas[0]['Return Filing Period']               = '';
                    $datas[0]['Total Transaction Value']            = '';
                    $datas[0]['Is Reverse Charge Applicable?']      = '';
                    $datas[0]['Original Invoice Date (In case of amendment)']        = '';
                    $datas[0]['Original Invoice Number (In case of amendment)']      = '';
                    $datas[0]['Original Customer Billing GSTIN (In case of amendment)']        = '';
                    $datas[0]['Is this document cancelled?']           = '';
                    $datas[0]['GSTIN of Ecommerce Marketplace']        = '';
                    $datas[0]['Is this a Bill of Supply?']             = '';
                    $datas[0]['Has GST/IDT TDS been deducted']         = '';
                    $datas[0]['Type of Export']                        = '';
                    $datas[0]['Shipping Port Code - Export']           = '';
                    $datas[0]['Shipping Bill Number - Export']         = '';
                    $datas[0]['Shipping Bill Date - Export']           = '';
                    $datas[0]['Customer Billing Name']                 = '';
                    $datas[0]['Customer Billing GSTIN']                = '';
                    $datas[0]['Customer Billing Address']              = '';
                    $datas[0]['Customer Billing City']                 = '';
                    $datas[0]['Customer Billing State']                = '';
                    $datas[0]['Date of Linked Advance Receipt']        = '';
                    $datas[0]['Voucher Number of Linked Advance Receipt']                  = '';
                    $datas[0]['Adjustment Amount of the Linked Advance Receipt']           = '';
                    $datas[0]['Item Quantity']                                             = '';
                    $datas[0]['Item Unit of Measurement']                                  = '';
                    $datas[0]['Item Rate']                                                 = '';
                    $datas[0]['Is the item a GOOD (G) or SERVICE (S)']                     = '';
                    $datas[0]['Item Description']                                          = '';
                    $datas[0]['HSN or SAC Code']                                           = '';
                    $datas[0]['CGST Rate']                                                 = '';
                    $datas[0]['CGST Amount']                                               = '';
                    $datas[0]['SGST Rate']                                                 = '';
                    $datas[0]['SGST Amount']                                               = '';
                    $datas[0]['IGST Rate']                                                 = '';
                    $datas[0]['IGST Amount']                                               = '';
                    $datas[0]['CESS Rate']                                                 = '';
                    $datas[0]['CESS Amount']                                               = '';
                    $datas[0]['Item Taxable Value']                                        = '';
                    $datas[0]['Total Item Discount Amount']                                = '';
                    $datas[0]['Is this a Nil Rated/Exempt/NonGST item?']                   = '';
                    
                
            }
            return Excel::create('gst_return_filter_'.time().'', function($excel) use ($datas) {
            $excel->sheet('mySheet', function($sheet) use ($datas)
            {
                $sheet->fromArray($datas);
            }); 
        })->download(); 

    }

}