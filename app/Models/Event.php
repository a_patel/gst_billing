<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable  = ['user_id', 'name', 'full_day_event', 'start_time', 'end_time'];
}
