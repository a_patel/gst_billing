<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemUnit extends Model
{
    protected $table = 'item_unit';
    protected $fillable = ['abbr','name','inactive','user_id'];
    protected $dates = ['deleted_at'];
    use softDeletes;
}
