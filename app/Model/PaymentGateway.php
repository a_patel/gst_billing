<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentGateway extends Model
{
    protected $table = 'payment_gateway';
    public $timestamps = false;
    protected $fillable = [
        'name', 'value', 'site', 'user_id'
    ];
    use softDeletes;
    protected $dates = ['deleted_at'];
}
