<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Currency extends Model
{
    protected $table = 'currency';
    public $timestamps = false;
    protected $fillable = [
        'name', 'symbol', 'user_id'
    ];
    use softDeletes;
    protected $dates = ['deleted_at'];
}
