<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class IncomeExpenseCategory extends Model
{
    protected $table = 'income_expense_categories';
    public $timestamps = false;
    protected $fillable = [
        'name', 'type', 'user_id'
    ];
    use softDeletes;
    protected $dates = ['deleted_at'];
}
