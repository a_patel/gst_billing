<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentMethod extends Model
{
    protected $table = 'payment_terms';
    public $timestamps = false;
    protected $fillable = [
        'name', 'defaults', 'user_id'
    ];
    use softDeletes;
    protected $dates = ['deleted_at'];
}
