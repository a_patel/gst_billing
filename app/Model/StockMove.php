<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StockMove extends Model
{
    public $timestamps = false;
    protected $fillable = ['stock_id', 'order_no', 'trans_type', 'loc_code', 'tran_date', 'person_id', 'order_reference', 'reference', 'transaction_reference_id', 'note', 'qty', 'price'];
    protected $table= 'stock_moves';
    use softDeletes;
    protected $dates = ['deleted_at'];

}
