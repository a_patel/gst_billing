<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Location extends Model
{
    protected $table = 'location';
    use softDeletes;
    protected $fillable = ['loc_code','user_id','location_name','delivery_address','email','phone','fax','contact','inactive'];
    protected $dates = ['deleted_at'];
}
