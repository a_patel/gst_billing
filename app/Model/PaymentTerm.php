<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentTerm extends Model
{
    protected $table = 'invoice_payment_terms';
    public $timestamps = false;
    protected $fillable = [
        'terms', 'days_before_due', 'defaults', 'user_id'
    ];
    use softDeletes;
    protected $dates = ['deleted_at'];
}
