<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Supplier extends Model
{
    protected $table = 'suppliers';
    protected $primaryKey = 'supplier_id';
    use softDeletes;
    protected $fillable = ['supplier_id','user_id','supp_name','email','address','contact','city','state','zipcode','country','gstin','inactive'];
    protected $dates = ['deleted_at'];
}
