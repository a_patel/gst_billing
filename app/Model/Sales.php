<?php

namespace App\Model;
use DB;
use Illuminate\Database\Eloquent\Model;

class Sales extends Model
{
	protected $table = 'sales_orders';
    protected $fillable = ['user_id', 'trans_type', 'invoice_type', 'debtor_no', 'branch_id', 'person_id', 'reference', 'customer_ref', 'order_reference_id', 'order_reference', 'comments', 'ord_date', 'order_type', 'from_stk_loc', 'payment_id', 'total', 'paid_amount', 'payment_term','deleted'];

    public function getAllSalseOrder($from, $to, $customer, $location)
    { 
        $conditions = array('sales_orders.trans_type'=>SALESINVOICE,'sales_orders.user_id'=>auth()->user()->id,'sales_orders.deleted'=>0);
        $whereBetween = '';
        if($customer){
            $conditions['sales_orders.debtor_no'] = $customer;
        }
        if($location){
            $conditions['sales_orders.from_stk_loc'] = $location;
        }


        if($from !=NULL && $to != NULL){
            $from = DbDateFormat($from);
            $to = DbDateFormat($to);
        $data = $this->leftJoin('debtors_master', 'sales_orders.debtor_no', '=', 'debtors_master.debtor_no')
                    ->select('sales_orders.*', 'debtors_master.name as cus_name')
                    ->where($conditions)
                    ->where('sales_orders.ord_date','>=',$from)
                    ->where('sales_orders.ord_date','<=',$to)
                    ->orderBy('sales_orders.created_at', 'desc')
                    ->get();

        }else{

        $data = $this->leftJoin('debtors_master', 'sales_orders.debtor_no', '=', 'debtors_master.debtor_no')
                    ->select('sales_orders.*', 'debtors_master.name as cus_name','debtors_master.gstin as cus_gstin')
                    ->where($conditions)
                    ->orderBy('sales_orders.created_at', 'desc')
                    ->get();
        }
      
        return $data;
    }

    public function getAllGstReturn($from, $to){
       if($from !=NULL && $to != NULL){
         $from   = DbDateFormat($from);
         $to     = DbDateFormat($to);
         $result = [];
         $data  = DB::select('SELECT order_no from sales_orders where trans_type=202 and deleted = 0 and user_id = "'.auth()->user()->id.'" and ord_date between "'.$from.'" and "'.$to.'" order by sales_orders.created_at DESC');

             for($i=0;$i<count($data);$i++){
                $order_no   = $data[$i]->order_no?$data[$i]->order_no:'';
                $result[$i] = DB::select("SELECT sum(unit_price*quantity)as taxable_amount,sales_orders.*,sales_order_details.*,debtors_master.*,cust_branch.* FROM `sales_orders` left join sales_order_details ON sales_orders.order_no=sales_order_details.order_no left join debtors_master
                                  ON sales_orders.debtor_no=debtors_master.debtor_no 
                                  left join cust_branch
                                  ON sales_orders.debtor_no=cust_branch.debtor_no
                                  where sales_order_details.order_no=$order_no
                                  and sales_orders.deleted = 0
                                  and sales_orders.user_id = ".auth()->user()->id);
              }
       }else{
		  $result = [];
         $data  = DB::select('SELECT order_no from sales_orders where trans_type=202 and deleted = 0 and user_id = "'.auth()->user()->id.'"  order by sales_orders.created_at DESC');
            for($i=0;$i<count($data);$i++){
                $order_no   = $data[$i]->order_no;
                $result[$i] = DB::select("SELECT sum(unit_price*quantity)as taxable_amount,sales_orders.*,debtors_master.name,debtors_master.gstin,cust_branch.* FROM `sales_orders` left join sales_order_details ON sales_orders.order_no=sales_order_details.order_no left join debtors_master
                                  ON sales_orders.debtor_no=debtors_master.debtor_no
                                  left join cust_branch
                                  ON sales_orders.debtor_no=cust_branch.debtor_no
                                  where sales_order_details.order_no=$order_no
                                  and sales_orders.deleted = 0
                                  and sales_orders.user_id = ".auth()->user()->id);

             }
        
        }
        
        return $result;
    }

    public function getAllGstReturnCsv($from, $to){
       if($from !=NULL && $to != NULL){
         $from   = DbDateFormat($from);
         $to     = DbDateFormat($to);
         $data = DB::select(DB::raw("
                SELECT so.invoice_type,so.reference,so.order_reference_id,so.debtor_no,so.ord_date,dm.name,dm.address,dm.gstin,cub.*,info_table.* FROM sales_orders as so
                
                LEFT JOIN (

                SELECT sd.*,(sd.quantity*pd.purchase_rate_incl_tax)as purchase_price_incl_tax,(sd.quantity*pd.purchase_rate_excl_tax)as purchase_price_excl_tax FROM(SELECT sod.*,itt.tax_rate,(sod.unit_price*sod.quantity-(sod.unit_price*sod.quantity*discount_percent)/100) as sale_price_excl_tax,((sod.unit_price*sod.quantity*discount_percent)/100) as discount_amount,((sod.unit_price*sod.quantity-(sod.unit_price*sod.quantity*discount_percent)/100)+((sod.unit_price*sod.quantity-(sod.unit_price*sod.quantity*discount_percent)/100)*itt.tax_rate/100)) as sale_price_incl_tax

                FROM sales_order_details as sod

                LEFT JOIN item_tax_types as itt
                ON itt.id = sod.tax_type_id
                WHERE sod.trans_type = 202)sd
                LEFT JOIN(SELECT pod.item_code,ROUND(SUM(pod.unit_price*pod.quantity_received)/SUM(pod.quantity_received),2) as purchase_rate_excl_tax,ROUND(SUM(pod.unit_price*pod.quantity_received+pod.unit_price*pod.quantity_received*itt.tax_rate/100)/SUM(pod.quantity_received),2) as purchase_rate_incl_tax FROM purch_order_details as pod LEFT JOIN item_tax_types as itt ON itt.id = pod.tax_type_id AND itt.user_id=".auth()->user()->id." AND itt.deleted_at is null GROUP BY pod.item_code)pd
                ON sd.stock_id = pd.item_code

                )info_table
                ON so.order_no = info_table.order_no
                LEFT JOIN debtors_master as dm
                ON dm.debtor_no = so.debtor_no
                LEFT JOIN cust_branch as cub
                ON cub.debtor_no = so.debtor_no
                WHERE so.trans_type=202
                                  AND so.deleted = 0
                                  AND so.user_id = ".auth()->user()->id." AND so.ord_date BETWEEN '$from' AND '$to'"));
     }
       
        return $data; 
    }
    public function getSalseInvoiceByID($order_no)
    {
              $data = DB::table('sales_order_details')
                    ->where(['order_no'=>$order_no])
                    ->leftJoin('item_code', 'sales_order_details.stock_id', '=', 'item_code.stock_id')
                    ->leftJoin('item_tax_types', 'item_tax_types.id','=','sales_order_details.tax_type_id')
                    ->select('sales_order_details.*', 'item_code.id as item_id','item_tax_types.tax_rate')
                    ->get();
          return $data;
    }

    public function getSalseInvoicePdf($id)
    { 
        return $this->where(['sales_orders.order_no'=>$id])
                    ->leftJoin('debtors_master', 'sales_orders.debtor_no', '=', 'debtors_master.debtor_no')
                    ->leftJoin('sales_order_details', 'sales_orders.order_no', '=', 'sales_order_details.order_no')
                    ->select('sales_orders.*', 'debtors_master.*')
                    ->first();
    }

    public function stockValidate($loc,$id)
    {
        $data = DB::table('stock_moves')
                     ->select(DB::raw('sum(qty) as total'))
                     ->where(['stock_id'=>$id, 'loc_code'=>$loc])
                     ->groupBy('loc_code')
                     ->first();
        
        if(count($data)>0){
            if($data->total <= 0){
               $total = 0; 
            }else{
            $total = $data->total;
        }
        }else{
         $total = 0;   
        }
         return $total;
    }

        public function calculateTaxRow($order_no){
            //echo $order_no;exit();
        $tax_rows = DB::table('sales_order_details')
                ->join('item_tax_types', 'item_tax_types.id', '=', 'sales_order_details.tax_type_id')
                ->select(DB::raw('((sales_order_details.quantity*sales_order_details.unit_price-sales_order_details.quantity*sales_order_details.unit_price*sales_order_details.discount_percent/100)*item_tax_types.tax_rate)/100 AS tax_amount,item_tax_types.tax_rate'))
                ->where('order_no', $order_no)
                ->get();

       // d($tax_rows,1);

        $tax_amount = [];
        $tax_rate   =[];
        $i=0;
        foreach ($tax_rows as $key => $value) {
           if(isset($tax_amount[$value->tax_rate])){
                $tax_amount[strval($value->tax_rate)] +=$value->tax_amount;
           }else{
             $tax_amount[strval($value->tax_rate)] =$value->tax_amount;
           }
        }
        return $tax_amount;
    }
    public function getAllSalseInvoiceByUserId($from, $to, $customer, $location, $id)
    { 
        $from = DbDateFormat($from);
        $to = DbDateFormat($to);

        if($customer =='all' && $location =='all'){
        $data = $this->leftJoin('debtors_master', 'sales_orders.debtor_no', '=', 'debtors_master.debtor_no')
                    ->leftJoin('location', 'sales_orders.from_stk_loc', '=', 'location.loc_code')
                    ->select('sales_orders.*', 'debtors_master.name as cus_name','location.location_name as loc_name')
                    ->where(['sales_orders.trans_type'=>SALESINVOICE,'sales_orders.person_id'=>$id])
                    ->whereDate('sales_orders.ord_date','>=', $from)
                    ->whereDate('sales_orders.ord_date','<=', $to)
                    ->orderBy('sales_orders.ord_date', 'desc')
                    ->get();
        }else if($customer !='all' && $location =='all'){
        $data = $this->leftJoin('debtors_master', 'sales_orders.debtor_no', '=', 'debtors_master.debtor_no')
                    ->leftJoin('location', 'sales_orders.from_stk_loc', '=', 'location.loc_code')
                    ->select('sales_orders.*', 'debtors_master.name as cus_name','location.location_name as loc_name')
                    ->where(['sales_orders.trans_type'=>SALESINVOICE,'sales_orders.person_id'=>$id,'sales_orders.debtor_no'=>$customer])
                    ->whereDate('sales_orders.ord_date','>=', $from)
                    ->whereDate('sales_orders.ord_date','<=', $to)
                    ->orderBy('sales_orders.ord_date', 'desc')
                    ->get();
        }else if($customer =='all' && $location !='all'){
        $data = $this->leftJoin('debtors_master', 'sales_orders.debtor_no', '=', 'debtors_master.debtor_no')
                    ->leftJoin('location', 'sales_orders.from_stk_loc', '=', 'location.loc_code')
                    ->select('sales_orders.*', 'debtors_master.name as cus_name','location.location_name as loc_name')
                    ->where(['sales_orders.trans_type'=>SALESINVOICE,'sales_orders.person_id'=>$id,'sales_orders.from_stk_loc'=>$location])
                    ->whereDate('sales_orders.ord_date','>=', $from)
                    ->whereDate('sales_orders.ord_date','<=', $to)
                    ->orderBy('sales_orders.ord_date', 'desc')
                    ->get();
        }else if($customer !='all' && $location !='all'){
        $data = $this->leftJoin('debtors_master', 'sales_orders.debtor_no', '=', 'debtors_master.debtor_no')
                    ->leftJoin('location', 'sales_orders.from_stk_loc', '=', 'location.loc_code')
                    ->select('sales_orders.*', 'debtors_master.name as cus_name','location.location_name as loc_name')
                    ->where(['sales_orders.trans_type'=>SALESINVOICE,'sales_orders.person_id'=>$id,'sales_orders.from_stk_loc'=>$location,'sales_orders.debtor_no'=>$customer])
                    ->whereDate('sales_orders.ord_date','>=', $from)
                    ->whereDate('sales_orders.ord_date','<=', $to)
                    ->orderBy('sales_orders.ord_date', 'desc')
                    ->get();
        }
        return $data;

    }

        public function getSaleYears(){
        $data = DB::select("SELECT DISTINCT YEAR(ord_date) as year FROM sales_orders WHERE trans_type = 202 AND deleted=0 AND user_id=".auth()->user()->id." ORDER BY ord_date DESC");
        return $data;
    }

}
