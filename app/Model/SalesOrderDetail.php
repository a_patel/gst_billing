<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SalesOrderDetail extends Model
{
    protected $fillable  = ['order_no', 'trans_type', 'stock_id', 'tax_type_id', 'description', 'unit_price', 'qty_sent', 'quantity', 'is_inventory', 'hsn', 'discount_percent'];

    use softDeletes;
    protected $dates = ['deleted_at'];

}
