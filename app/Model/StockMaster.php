<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StockMaster extends Model
{
    protected $table = 'stock_master';
    public $timestamps = false;
    protected $fillable = ['stock_id','user_id','tax_type_id','description','category_id','long_description','hsn','inactive','units','deleted_status'];
}
