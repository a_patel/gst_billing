<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tax extends Model
{
    protected $table = 'item_tax_types';
    public $timestamps = false;
    protected $fillable = [
        'name', 'tax_rate', 'defaults', 'user_id'
    ];
    use softDeletes;
    protected $dates = ['deleted_at'];
}
